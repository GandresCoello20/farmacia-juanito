import { AxiosResponse } from 'axios';
import { Respuesta } from '../../interface';
import {
  createNameProduct,
  createNameLaboratorio,
  createNamePrincipioActive,
  elimarPrincioActivo, eliminarLaboratorio, elimarProductName
} from '../propiedades-producto';

let respuesta: Respuesta = { feeback: '', type: 'EXITO' };


////////////////   CREAR 

export const createProductoName = async (name: string) => {
  try {
    const axios: AxiosResponse = await createNameProduct(name);
    
    return HooksResponsCreate(axios);
  } catch (error) {
    return error.message;
  }
};

export const createLaboratorioName = async (name: string) => {
  try {
    const axios: AxiosResponse = await createNameLaboratorio(name);
    
    return HooksResponsCreate(axios);
  } catch (error) {
    return error.message;
  }
};

export const createPrincipioActive = async (name: string) => {
  try {
    const axios: AxiosResponse = await createNamePrincipioActive(name);
    
    return HooksResponsCreate(axios);
  } catch (error) {
    return error.message;
  }
};


////////// ELIMINAR

export const deletePrincipioActive = async (id: string | number) => {
  try {
    const axios: AxiosResponse = await elimarPrincioActivo(id);
    
    return HooksResponsDelete(axios, 'EL PRINCIPIO ACTIVO');
  } catch (error) {
    return error.message;
  }
};

export const deleteLaboratorio = async (id: string | number) => {
  try {
    const axios: AxiosResponse = await eliminarLaboratorio(id);

    return HooksResponsDelete(axios, 'EL LABORATORIO');
  } catch (error) {
    return error.message;
  }
};

export const deleteProductName = async (id: string | number) => {
  try {
    const axios: AxiosResponse = await elimarProductName(id);

    return HooksResponsDelete(axios, 'EL NOMBRE DEL PRODUCTO')
  } catch (error) {
    return error.message;
  }
}


////// HOOKS


const HooksResponsDelete = (axios: AxiosResponse, sms: string) => {
  if (axios.data.feeback) {
    respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
  }else{
    if(axios.data.removed){
      respuesta = { feeback: `${sms} FUE ELIMINADO`, type: 'EXITO' };
    }else {
      respuesta = { feeback: `OCURRIO UN ERROR AL ELIMINAR ${sms}`, type: 'ERROR' };
    }
  }

  return { axios, respuesta }
}

const HooksResponsCreate = (axios: AxiosResponse) => {
  if (axios.data.feeback) {
    respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
  }

  return { axios, respuesta };
}
