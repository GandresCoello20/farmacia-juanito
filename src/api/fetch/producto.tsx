import { AxiosResponse } from 'axios';
import { Respuesta } from '../../interface';
import { createProduct, eliminarProducto } from '../productos';
import { Producto_INT } from '../../interface';

let respuesta: Respuesta = { feeback: '', type: 'EXITO' };

export const createProducto = async (data: Producto_INT) => {
  try {
    const axios: AxiosResponse = await createProduct(data);
    if (axios.data.feeback) {
      respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
    }

    return { axios, respuesta };
  } catch (error) {
    return error.message;
  }
};

export const EliminarProducto = async (id: string | number) => {
  try {
    const axios: AxiosResponse = await eliminarProducto(id);
    if (axios.data.feeback) {
      respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
    }else{
      if(axios.data.removed){
        respuesta = { feeback: 'EL PRODUCTO FUE ELIMINADO', type: 'EXITO' };
      }else {
        respuesta = { feeback: 'OCURRIO UN ERROR AL ELIMINAR EL PRODUCTO', type: 'ERROR' };
      }
    }

    return { axios, respuesta };
  } catch (error) {
    return error.message;
  }
};