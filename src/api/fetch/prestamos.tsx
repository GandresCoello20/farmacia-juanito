import { AxiosResponse } from 'axios';
import { addPrestamo } from '../prestamos';
import { eliminarPrestamo, incrementar_abono_Prestamo, limpiarPrestamo } from '../prestamos';
import { Respuesta } from '../../interface';

let respuesta: Respuesta = { feeback: '', type: 'EXITO' };

export const CreatePrestamos = async (descripcion_prestamo: string, cantidad_prestamo: number) => {
  try {
    const axios: AxiosResponse = await addPrestamo(descripcion_prestamo, cantidad_prestamo);
    if (axios.data.feeback) {
      respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
    }

    return { axios, respuesta };
  } catch (error) {
    return error.message;
  }
};

export const DeletePrestamo = async (id: number | string) => {
  try {
    const axios: AxiosResponse = await eliminarPrestamo(id);
    if (axios.data.feeback) {
      respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
    } else {
      if (axios.data.removed) {
        respuesta = { feeback: `EL PRESTAMO FUE ELIMINADO`, type: 'EXITO' };
      } else {
        respuesta = { feeback: `OCURRIO UN ERROR AL ELIMINAR EL PRESTAMO`, type: 'ERROR' };
      }
    }

    return { axios, respuesta };
  } catch (error) {
    return error.message;
  }
};

export const LimpiarPrestamo = async () => {
  try {
    const axios: AxiosResponse = await limpiarPrestamo();
    if (axios.data.feeback) {
      respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
    } else {
      if (axios.data.removed) {
        respuesta = {
          feeback: `LOS PRESTAMOS DE MESES ANTERIORES FUERON ELIMINADOS`,
          type: 'EXITO',
        };
      } else {
        respuesta = {
          feeback: `OCURRIO UN ERROR AL ELIMINAR LOS PRESTAMOS ANTERIORES`,
          type: 'ERROR',
        };
      }
    }

    return { axios, respuesta };
  } catch (error) {
    return error.message;
  }
};

export const Incrementar_pago_abono_prestamo = async (id_prestamo: string, valor: number) => {
  try {
    const axios: AxiosResponse = await incrementar_abono_Prestamo(id_prestamo, valor);
    if (axios.data.feeback) {
      respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
    } else {
      if (axios.data.update) {
        respuesta = { feeback: `EL INCREMENTO DEL ABONO FUE REGISTRADO`, type: 'EXITO' };
      } else {
        respuesta = {
          feeback: `OCURRIO UN ERROR AL INCREMENTAR EL ABONO`,
          type: 'ERROR',
        };
      }
    }

    return { axios, respuesta };
  } catch (error) {
    return error.message;
  }
};
