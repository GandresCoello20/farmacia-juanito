import { AxiosResponse } from 'axios';
import { Proveedor_INT, Producto_proveedor_INT, Respuesta } from '../../interface';
import {
  crearProveedor,
  addNewProduct,
  eliminarProveedor,
  eliminarProductoProveedor,
  pagarProductProveedor,
} from '../proveedores';

let respuesta: Respuesta = { feeback: '', type: 'EXITO' };

export const CreateProveedor = async (data: Proveedor_INT) => {
  try {
    const axios: AxiosResponse = await crearProveedor(data);
    if (axios.data.feeback) {
      respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
    }

    return { axios, respuesta };
  } catch (error) {
    return error.message;
  }
};

export const DeleteProveedor = async (id: number | string) => {
  try {
    const axios: AxiosResponse = await eliminarProveedor(id);
    if (axios.data.feeback) {
      respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
    } else {
      if (axios.data.removed) {
        respuesta = { feeback: `EL PROVEEDOR FUE ELIMINADO`, type: 'EXITO' };
      } else {
        respuesta = { feeback: `OCURRIO UN ERROR AL ELIMINAR EL PROVEEDOR`, type: 'ERROR' };
      }
    }

    return { axios, respuesta };
  } catch (error) {
    return error.message;
  }
};

/////////////////  producto proveedor

export const CreateNewProductProveedor = async (data: Producto_proveedor_INT) => {
  try {
    const axios: AxiosResponse = await addNewProduct(data);
    if (axios.data.feeback) {
      respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
    }

    return { axios, respuesta };
  } catch (error) {
    return error.message;
  }
};

export const DeleteProductoProveedor = async (id: number | string) => {
  try {
    const axios: AxiosResponse = await eliminarProductoProveedor(id);
    if (axios.data.feeback) {
      respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
    } else {
      if (axios.data.removed) {
        respuesta = { feeback: `EL PRODUCTO DEL PROVEEDOR FUE ELIMINADO`, type: 'EXITO' };
      } else {
        respuesta = {
          feeback: `OCURRIO UN ERROR AL ELIMINAR EL PRODUCTO DEL PROVEEDOR`,
          type: 'ERROR',
        };
      }
    }

    return { axios, respuesta };
  } catch (error) {
    return error.message;
  }
};

export const PagarProductoProveedor = async (id: string) => {
  try {
    const axios: AxiosResponse = await pagarProductProveedor(id);
    if (axios.data.feeback) {
      respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
    } else {
      if (axios.data.update) {
        respuesta = { feeback: `EL PRODUCTO DEL PROVEEDOR FUE PAGADO`, type: 'EXITO' };
      } else {
        respuesta = {
          feeback: `OCURRIO UN ERROR AL PAGAR EL PRODUCTO DEL PROVEEDOR`,
          type: 'ERROR',
        };
      }
    }

    return { axios, respuesta };
  } catch (error) {
    return error.message;
  }
};
