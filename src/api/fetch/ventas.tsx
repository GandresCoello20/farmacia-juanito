import { AxiosResponse } from 'axios';
import { Respuesta, Factura_INT } from '../../interface';
import { createVenta, eliminarFactura } from '../ventas';

let respuesta: Respuesta = { feeback: '', type: 'EXITO' };

export const CreateVenta = async (data: Factura_INT) => {
  try {
    const axios: AxiosResponse = await createVenta(data);
    if (axios.data.feeback) {
      respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
    }

    return { axios, respuesta };
  } catch (error) {
    return error.message;
  }
};

export const DeleteFactura = async (id: number | string) => {
  try {
    const axios: AxiosResponse = await eliminarFactura(id);
    if (axios.data.feeback) {
      respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
    } else {
      if (axios.data.removed) {
        respuesta = { feeback: `LA FACTURA FUE ELIMINADO`, type: 'EXITO' };
      } else {
        respuesta = { feeback: `OCURRIO UN ERROR AL ELIMINAR LA FACTURA`, type: 'ERROR' };
      }
    }

    return { axios, respuesta };
  } catch (error) {
    return error.message;
  }
};
