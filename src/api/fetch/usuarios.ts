import { AxiosResponse } from 'axios';
import { Respuesta, CreateCount } from '../../interface';
import { cleanHistory, create_count, eliminarUser, verificacionCodeAccess } from '../usuarios';

let respuesta: Respuesta = { feeback: '', type: 'EXITO' };

export const LimpiarHistorialSession = async () => {
  try {
    const axios: AxiosResponse = await cleanHistory();
    if (axios.data.feeback) {
      respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
    }

    return { axios, respuesta };
  } catch (error) {
    return error.message;
  }
};

export const CodeAccess = async (autorizacion: string) => {
  try {
    const axios: AxiosResponse = await verificacionCodeAccess(autorizacion);
    if (axios.data.feeback !== 'Acceso concedido') {
      respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
    }

    return { axios, respuesta };
  } catch (error) {
    return error.message;
  }
};

export const CreateCountUser = async (user: CreateCount) => {
  try {
    const axios: AxiosResponse = await create_count(user);
    if (axios.data.feeback) {
      respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
    }

    return { axios, respuesta };
  } catch (error) {
    return error.message;
  }
};

export const DeleteUser = async (id: number | string) => {
  try {
    const axios: AxiosResponse = await eliminarUser(id);
    if (axios.data.feeback) {
      respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
    } else {
      if (axios.data.removed) {
        respuesta = { feeback: `EL USUARIO FUE ELIMINADO`, type: 'EXITO' };
      } else {
        respuesta = { feeback: `OCURRIO UN ERROR AL ELIMINAR EL USUARIO`, type: 'ERROR' };
      }
    }

    return { axios, respuesta };
  } catch (error) {
    return error.message;
  }
};
