import { AxiosResponse } from 'axios';
import { Cliente_INT, Respuesta } from '../../interface';
import { createCliente, eliminarCliente } from '../clientes';

let respuesta: Respuesta = { feeback: '', type: 'EXITO' };

export const createClient = async (data: Cliente_INT) => {
  try {
    const axios: AxiosResponse = await createCliente(data);
    if (axios.data.feeback) {
      respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
    }

    return { axios, respuesta };
  } catch (error) {
    return error.message;
  }
};

export const eliminarClient = async (id: string | number) => {
  try {
    const axios: AxiosResponse = await eliminarCliente(id);
    if (axios.data.feeback) {
      respuesta = { feeback: axios.data.feeback, type: 'ERROR' };
    } else {
      if (axios.data.removed) {
        respuesta = { feeback: 'EL CLIENTE FUE ELIMINADO', type: 'EXITO' };
      } else {
        respuesta = { feeback: 'OCURRIO UN ERROR AL ELIMINAR EL CLIENTE', type: 'ERROR' };
      }
    }

    return { axios, respuesta };
  } catch (error) {
    return error.message;
  }
};
