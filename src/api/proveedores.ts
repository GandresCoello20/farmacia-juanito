import axios from 'axios';
import { DOMAIN, TOKEN } from '../config/domain';
import { Proveedor_INT, Producto_proveedor_INT } from '../interface';

/////////////////  METODO DE PETICION GET

export const obtenerProveedores = async () => {
  return await axios({
    method: 'GET',
    url: `${DOMAIN}/api/proveedor`,
  });
};

export const obtenerProductoProveedor = async (id_proveedor: string) => {
  return await axios({
    method: 'GET',
    url: `${DOMAIN}/api/proveedor/producto/${id_proveedor}`,
  });
};

/*
export const obtenerMontoTotalPP = async (fecha) => {
    return await axios({
        method: "GET",
        url: `${domain()}/api/proveedor/producto/monto_total/${fecha}`,
    });
};
*/
/////////////////  METODO DE PETICION POST

export const crearProveedor = async (data: Proveedor_INT) => {
  return await axios({
    method: 'POST',
    url: `${DOMAIN}/api/proveedor`,
    data,
    headers: { 'access-token': TOKEN },
  });
};

export const addNewProduct = async (data: Producto_proveedor_INT) => {
  return await axios({
    method: 'POST',
    url: `${DOMAIN}/api/proveedor/producto`,
    data,
  });
};

////////////////  METODO DE PETICION DELETE

export const eliminarProveedor = async (id: number | string) => {
  return await axios({
    method: 'DELETE',
    url: `${DOMAIN}/api/proveedor/${id}`,
    headers: { 'access-token': TOKEN },
  });
};

export const eliminarProductoProveedor = async (id: string | number) => {
  return await axios({
    method: 'DELETE',
    url: `${DOMAIN}/api/proveedor/producto/${id}`,
    headers: { 'access-token': TOKEN },
  });
};

/*
///////////////  METODO DE PETICION PUT

export const editarProveedor = async (
    id,
    nombres,
    id_laboratorio,
    correo,
    telefono
    ) => {
    return await axios({
        method: "PUT",
        url: `${domain()}/api/proveedor/${id}`,
        data: { nombres, id_laboratorio, correo, telefono },
        headers: { "access-token": Cookie.get("access_token") },
    });
};

export const editarProductoProveedor = async (
    id_pp,
    descripcion,
    fecha_pago,
    total,
    estado_pp,
    abonado
) => {
    return await axios({
        method: "PUT",
        url: `${domain()}/api/proveedor/producto/${id_pp}`,
        data: { descripcion, fecha_pago, total, estado_pp, abonado },
        headers: { "access-token": Cookie.get("access_token") },
    });
};*/

export const pagarProductProveedor = async (id: string) => {
  return await axios({
    method: 'PUT',
    url: `${DOMAIN}/api/proveedor/producto/estado/pagado/${id}`,
  });
};
