import axios from 'axios';
import { DOMAIN, TOKEN } from '../config/domain';

////////////////  METODO DE PETICION GET

export const obtenerPrestamos = async () => {
  return await axios({
    method: 'GET',
    url: `${DOMAIN}/api/prestamo`,
  });
};

export const obtenerPrestamosPorFecha = async (fecha: string) => {
  return await axios({
    method: 'GET',
    url: `${DOMAIN}/api/prestamo/fecha/${fecha}`,
  });
};

export const obtenerMontoTotalPorFecha = async (fecha: string) => {
  return await axios({
    method: 'GET',
    url: `${DOMAIN}/api/prestamo/monto_total/fecha/${fecha}`,
  });
};

////////////////  METDOD DE PETICION POST

export const addPrestamo = async (descripcion_prestamo: string, cantidad_prestamo: number) => {
  return await axios({
    method: 'POST',
    url: `${DOMAIN}/api/prestamo`,
    data: { descripcion_prestamo, cantidad_prestamo },
    headers: { 'access-token': TOKEN },
  });
};

////////////////  METODO DE PETICION DELETE

export const eliminarPrestamo = async (id: string | number) => {
  return await axios({
    method: 'DELETE',
    url: `${DOMAIN}/api/prestamo/${id}`,
    headers: { 'access-token': TOKEN },
  });
};

export const limpiarPrestamo = async () => {
  return await axios({
    method: 'DELETE',
    url: `${DOMAIN}/api/prestamo/limpiar_prestamos`,
    headers: { 'access-token': TOKEN },
  });
};

////////////////  METODO DE PETICION PUT

export const incrementar_abono_Prestamo = async (id: string, incremento_abono: number) => {
  return await axios({
    method: 'PUT',
    url: `${DOMAIN}/api/prestamo/incrementar_abono/${id}`,
    data: { incremento_abono },
    headers: { 'access-token': TOKEN },
  });
};

/*
export const editarPrestamo = async (
  id,
  descripcion_prestamo,
  cantidad_prestamo
) => {
  return await axios({
    method: "PUT",
    url: `${domain()}/api/prestamo/${id}`,
    data: { descripcion_prestamo, cantidad_prestamo },
    headers: { "access-token": Cookie.get("access_token") },
  });
};
*/
