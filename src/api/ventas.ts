import axios from 'axios';
import { DOMAIN, TOKEN } from '../config/domain';
import { Factura_INT } from '../interface';

////////////////////////  METODO DE PETICION POST

export const createVenta = async (data: Factura_INT) => {
  return await axios({
    method: 'POST',
    url: `${DOMAIN}/api/factura`,
    data: data,
  });
};

////////////////////// METODO DE PETICION GET

export const obtenerVentas = async (fecha_factura: string) => {
  return await axios({
    method: 'GET',
    url: `${DOMAIN}/api/factura/${fecha_factura}`,
  });
};

export const obtenerMontoTotaldeVentaPorFecha = async (fecha: string) => {
  return await axios({
    method: 'GET',
    url: `${DOMAIN}/api/factura/monto_total/${fecha}`,
  });
};

export const obtenerVentasPorMes = async () => {
  return await axios({
    method: 'GET',
    url: `${DOMAIN}/api/factura/ventas_por_mes`,
  });
};

/*
//////////////////// METODO DE PETICION DELETE

export const eliminarVentas = async (id) => {
    return await axios({
        method: "DELETE",
        url: `${domain()}/api/venta/${id}`,
        headers: { "access-token": Cookie.get("access_token") },
    });
};*/

export const eliminarFactura = async (id: number | string) => {
  return await axios({
    method: 'DELETE',
    url: `${DOMAIN}/api/factura/${id}`,
    headers: { 'access-token': TOKEN },
  });
};
