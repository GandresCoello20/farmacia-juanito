import axios from 'axios';
// import Cookie from "js-cookie";
import { DOMAIN, TOKEN } from '../config/domain';

///  PETICION GET

export const obtenerPrincipioActive = async () => {
  return await axios({
    method: 'GET',
    url: `${DOMAIN}/api/producto/principio_activo`,
  });
};

export const obtenerNameProduct = async () => {
  return await axios({
    method: 'GET',
    url: `${DOMAIN}/api/producto/nombre_producto`,
  });
};

export const obtenerNameLaboratorio = async () => {
  return await axios({
    method: 'GET',
    url: `${DOMAIN}/api/producto/nombre_laboratorio`,
  });
};

/// PETICION POST

export const createNameProduct = async (name: string) => {
  return await axios({
    method: 'POST',
    url: `${DOMAIN}/api/producto/nombre_producto`,
    data: {
      name_product: name,
    },
    headers: { 'access-token': TOKEN },
  });
};

export const createNameLaboratorio = async (name: string) => {
  return await axios({
    method: 'POST',
    url: `${DOMAIN}/api/producto/nombre_laboratorio`,
    data: {
      name_laboratorio: name,
    },
    headers: { 'access-token': TOKEN },
  });
};

export const createNamePrincipioActive = async (name: string) => {
  return await axios({
    method: 'POST',
    url: `${DOMAIN}/api/producto/principio_activo`,
    data: {
      name_principio_activo: name,
    },
    headers: { 'access-token': TOKEN },
  });
};

////////  DELETE

export const elimarPrincioActivo = async (id: number | string) => {
  return await axios({
    method: "DELETE",
    url: `${DOMAIN}/api/producto/principio_activo/${id}`,
    headers: { "access-token": TOKEN },
  });
};

export const eliminarLaboratorio = async (id: number | string) => {
  return await axios({
    method: "DELETE",
    url: `${DOMAIN}/api/producto/nombre_laboratorio/${id}`,
    headers: { "access-token": TOKEN },
  });
};

export const elimarProductName = async (id: string | number) => {
  return await axios({
    method: "DELETE",
    url: `${DOMAIN}/api/producto/nombre_producto/${id}`,
    headers: { "access-token": TOKEN },
  });
};
