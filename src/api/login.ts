import axios from 'axios';
import { DOMAIN } from '../config/domain';

//// PETICIONES GET

export const verificarEmail = async (email: string) => {
    return await axios({
        method: "GET",
        url: `${DOMAIN}/api/email/verificar/email/${email}`,
    });
};


//// PETICIONES POST

export const LoginAccess = async (email: string, password: string) => {
    return await axios({
        method: "POST",
        url: `${DOMAIN}/api/login/autenticacion`,
        data: {
            email,
            password,
        },
    });
};

export const LifeToken = async (token: string) => {
    return await axios({
        method: "POST",
        url: `${DOMAIN}/api/login/vida-token`,
        headers: { "access-token": token }
    });
};
