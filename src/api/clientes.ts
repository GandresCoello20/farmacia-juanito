import axios from 'axios';
import { DOMAIN, TOKEN } from '../config/domain';
import { Cliente_INT } from '../interface';

/////////////////  METODO DE PETICION POST
export const createCliente = async (dataClient: Cliente_INT) => {
  return await axios({
    method: 'POST',
    url: `${DOMAIN}/api/cliente`,
    data: dataClient,
  });
};

////////////////  METODO DE PETICI0N GET

export const traerCLiente = async () => {
  return await axios({
    method: 'GET',
    url: `${DOMAIN}/api/cliente`,
  });
};

/////////////// METODO DE PETICION DELETE

export const eliminarCliente = async (id: number | string) => {
  return await axios({
    method: 'DELETE',
    url: `${DOMAIN}/api/cliente/${id}`,
    headers: { 'access-token': TOKEN },
  });
};

/////////////  METODO DE PETICION PUT

export const editarCliente = async (dataClient: Cliente_INT) => {
  return await axios({
    method: 'PUT',
    url: `${DOMAIN}/api/cliente/${dataClient.id_cliente}`,
    data: dataClient,
    headers: { 'access-token': TOKEN },
  });
};
