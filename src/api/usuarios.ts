import axios from 'axios';
import { DOMAIN, TOKEN } from '../config/domain';
import { CreateCount } from '../interface';

///////////////////////  METODO DE PETICION GET

export const verificacionCodeAccess = async (autorizacion: string) => {
  return await axios({
    method: 'GET',
    url: `${DOMAIN}/view/home/verificar/${autorizacion}`,
  });
};

export const sessionHistory = async () => {
  return await axios({
    method: 'GET',
    url: `${DOMAIN}/api/usuario/history-session?limite=5`,
  });
};

export const obtenerUsuarios = async () => {
  return await axios({
    method: 'GET',
    url: `${DOMAIN}/api/usuario`,
  });
};

///////////////////  METODO DE PETICION POST

export const create_count = async (user_register: CreateCount) => {
  return await axios({
    method: 'POST',
    url: `${DOMAIN}/api/usuario`,
    data: user_register,
  });
};

/////////////////  METODO DE PETICION DELETE

export const cleanHistory = async () => {
  return await axios({
    method: 'DELETE',
    url: `${DOMAIN}/api/usuario/history-session`,
    headers: { 'access-token': TOKEN },
  });
};

export const eliminarUser = async (id: number | string) => {
  return await axios({
    method: 'DELETE',
    url: `${DOMAIN}/api/usuario/${id}`,
    headers: { 'access-token': TOKEN },
  });
};
/*
/////////////////  METODO DE PETICION PUT

export const editarUser = async (
    id,
    nombres,
    apellidos,
    email_on,
    tipo_user
    ) => {
    return await axios({
        method: "PUT",
        url: `${domain()}/api/usuario/${id}`,
        data: {
        nombres,
        apellidos,
        email_on,
        tipo_user,
        },
        headers: { "access-token": Cookie.get("access_token") },
    });
};*/
