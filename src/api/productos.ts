import axios from 'axios';
// import Cookie from "js-cookie";
import { DOMAIN, TOKEN } from '../config/domain';
import { Producto_INT } from '../interface';

/////////////////////////  METODO DE PETICION POST

export const createProduct = async (data: Producto_INT) => {
  return await axios({
    method: 'POST',
    url: `${DOMAIN}/api/producto`,
    data,
    headers: { 'access-token': TOKEN },
  });
};

/////////////////////////////////////  METODO DE PETICION GET

export const obtenerProductoCompleto = async () => {
  return await axios({
    method: 'GET',
    url: `${DOMAIN}/api/producto`,
    headers: { 'access-token': TOKEN },
  });
};

export const obtenerProductoCaducados = async () => {
  return await axios({
    method: 'GET',
    url: `${DOMAIN}/api/producto/caducados`,
    headers: { 'access-token': TOKEN },
  });
};

export const reporteProductos = async () => {
  return await axios({
    method: 'GET',
    url: `${DOMAIN}/api/producto/reporte`,
  });
};

//////////////////////////////////// METODO DE PETICION DELETE

export const eliminarProducto = async (id: string | number) => {
  return await axios({
    method: 'DELETE',
    url: `${DOMAIN}/api/producto/${id}`,
    headers: { 'access-token': TOKEN },
  });
};

/////////////////// METODO DE PETICION PUT

export const verificar_posibles_productos_caducados = async () => {
  return await axios({
    method: 'PUT',
    url: `${DOMAIN}/api/producto/verificar_posibles_caducados`,
    headers: { 'access-token': TOKEN },
  });
};

/*
export const editarPrincipioActivo = async (id, principio_activo) => {
  return await axios({
    method: "PUT",
    url: `${domain()}/api/producto/principio_activo/${id}`,
    data: { principio_activo },
    headers: { "access-token": Cookie.get("access_token") },
  });
};

export const editarNombreProducto = async (id, name_product) => {
  return await axios({
    method: "PUT",
    url: `${domain()}/api/producto/nombre_producto/${id}`,
    data: { name_product },
    headers: { "access-token": Cookie.get("access_token") },
  });
};

export const editarLaboratorio = async (id, name_laboratorio) => {
  return await axios({
    method: "PUT",
    url: `${domain()}/api/producto/nombre_laboratorio/${id}`,
    data: { name_laboratorio },
    headers: { "access-token": Cookie.get("access_token") },
  });
};

export const editarProducto = async (
  id,
  producto,
  laboratorio,
  principio_act,
  cantidad,
  presentacion,
  lote,
  sanitario,
  medidas,
  tipo_medidas,
  elaboracion,
  caducidad,
  pvp,
  pvf,
  cantidad_disponible
) => {
  return await axios({
    method: "PUT",
    url: `${domain()}/api/producto/${id}`,
    data: {
      producto,
      laboratorio,
      principio_act,
      cantidad,
      presentacion,
      lote,
      sanitario,
      medidas,
      tipo_medidas,
      elaboracion,
      caducidad,
      pvp,
      pvf,
      cantidad_disponible,
    },
    headers: { "access-token": Cookie.get("access_token") },
  });
};
*/
