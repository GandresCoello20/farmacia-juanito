import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../redux';
import { Usuario_INT } from '../interface';
import { ConfirmDelete } from '../components/confirm/confirmDelete';
import { BtnEdit } from '../components/edit/btn-edit-drawer';
import { DOMAIN } from '../config/domain';
import { Nav } from '../components/header/nav';
import { Row, Col, Alert, Tag, Avatar } from 'antd';
import { TableSkeleton } from '../components/skeleton/tabla-skeleton';

export default function UsuarioPage() {
  const UsuariosReducer = useSelector((state: RootState) => state.UsuarioReducer);

  return (
    <>
      <Nav title='Usuarios' />
      <h2 className='title-page'>Todo los Usuarios</h2>
      <Row justify='center' className='header-table'>
        <Col span={3}>Nombre</Col>
        <Col span={3}>Apellido</Col>
        <Col span={4}>Email</Col>
        <Col span={2}>Verificado</Col>
        <Col span={3}>Tipo de usuario</Col>
        <Col span={2}>Foto</Col>
        <Col span={3}>Opciones</Col>
      </Row>
      {UsuariosReducer.loading && <TableSkeleton />}
      {UsuariosReducer.usuarios.length === 0 ? (
        <Alert message='No existen datos de usuarios para mostrar' type='info' />
      ) : (
        UsuariosReducer.usuarios.map(
          (usuario: Usuario_INT) =>
            UsuariosReducer.myUser[0].id_user !== usuario.id_user && (
              <Row justify='center' className='body-table' key={usuario.id_user}>
                <>
                  <Col span={3}>{usuario.nombres}</Col>
                  <Col span={3}>{usuario.apellidos}</Col>
                  <Col span={4}>{usuario.email}</Col>
                  <Col span={2}>
                    {usuario.email_on === 0 ? (
                      <Tag color='red'>No verificado</Tag>
                    ) : (
                      <Tag color='green'>Verificado</Tag>
                    )}
                  </Col>
                  <Col span={3}>
                    {usuario.tipo_user === 'Administrador' ? (
                      <Tag color='cyan'>{usuario.tipo_user}</Tag>
                    ) : (
                      <Tag color='gold'>{usuario.tipo_user}</Tag>
                    )}
                  </Col>
                  <Col span={2}>
                    <Avatar src={`${DOMAIN}/static/${usuario.foto}`} />
                  </Col>
                  <Col span={3}>
                    <BtnEdit icono={true} formulario='usuarios' />
                    &nbsp; &nbsp;
                    <ConfirmDelete id={usuario.id_user} tabla='usuarios' icono={true} />
                  </Col>
                </>
              </Row>
            ),
        )
      )}
    </>
  );
}
