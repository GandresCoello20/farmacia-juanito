import React, { useState } from 'react';
import { Nav } from '../components/header/nav';
import { ContentCarrito } from '../components/carrito';
import { Row, Col } from 'antd';
import { StepsProducto } from '../components/productos/steps';

export function CarritoPage() {
  const [steps, setSteps] = useState<number>(1);

  const updateSteps = (value: number) => setSteps(value);

  return (
    <>
      <Nav title='Carrito' />
      <Row justify='center'>
        <Col span={18}>
          <StepsProducto current={steps} />
        </Col>
      </Row>
      <h2 className='title-page'>Productos Seleccionados</h2>
      <Row justify='center'>
        <Col span={22}>
          <ContentCarrito updateSteps={updateSteps} />
        </Col>
      </Row>
    </>
  );
}
