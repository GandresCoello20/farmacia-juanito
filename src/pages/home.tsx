import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import { Nav } from '../components/header/nav';
import { Row, Col, Divider } from 'antd';
import { TableVendidosReciente } from '../components/home/table-vendidos-recientes';
import { TablePorCaducar } from '../components/home/table-por-caducar';
import { TableCaducados } from '../components/home/table-caducados';
import './stylesPage/home.scss';

export function HomePage() {
  const history = useHistory<typeof useHistory>();

  useEffect(() => {
    if (window.location.pathname === '/login') {
      history.push('/404');
    }
  }, [history]);

  return (
    <>
      <Nav title='Inicio' />
      <h2 className='title-page'>Vendidos Recientemente</h2>
      <Row justify='center'>
        <Col span={22}>
          <TableVendidosReciente />
        </Col>
      </Row>
      <Divider />
      <h2 className='title-page'>Productos Por Caducar</h2>
      <Row justify='center'>
        <Col span={22}>
          <TablePorCaducar />
        </Col>
      </Row>
      <Divider />
      <h2 className='title-page'>Productos Caducados</h2>
      <Row justify='center'>
        <Col span={22}>
          <TableCaducados />
        </Col>
      </Row>
    </>
  );
}
