import React, { useState, useEffect } from 'react';
import { Row, Col, Result, Button, Badge, Divider } from 'antd';
import moment from 'moment';
import './stylesPage/emitir-factura.scss';

export function EmitirFact() {
  const [isImprimir] = useState<boolean>(false);
  const [productos, setProductos] = useState<[]>([]);
  const [total, setTotal] = useState<number>(0);
  const [desc, setDesc] = useState<number>(0);
  const [efect, setEfect] = useState<number>(0);
  const [camb, setCamb] = useState<number>(0);

  useEffect(() => {
    const urlParams = new URLSearchParams(window.location.search);
    const datos: any = urlParams.get('productos');
    const total: any = urlParams.get('total');
    const desc: any = urlParams.get('descuento');
    const efect: any = urlParams.get('efectivo');
    const camb: any = urlParams.get('cambio');

    setProductos(JSON.parse(datos));
    setTotal(total);
    setDesc(desc);
    setEfect(efect);
    setCamb(camb);
  }, []);

  return (
    <>
      <Row justify='center'>
        <Col span={24}>
          {isImprimir ? (
            <Result
              status='success'
              title='Compra realizada con exito'
              subTitle='En este momento debe imprimirse la fectura.'
              extra={[
                <Button type='primary' key='console'>
                  Cerrar
                </Button>,
                <strong>
                  Se cerrar en: <Badge count={25} /> segundos...
                </strong>,
              ]}
            />
          ) : (
            <>
              <strong style={{ marginLeft: 20 }}>{moment(new Date()).format('LTS')}</strong>
              <Row justify='start' className='header-table'>
                <Col span={2}>Cant</Col>
                <Col span={5}>Producto</Col>
                <Col span={2}>Iva</Col>
                <Col span={2}>Sub</Col>
              </Row>
              {productos.map((item: any) => (
                <Row justify='start' className='body-table' key={item.producto.id_producto}>
                  <Col span={2}>{item.cantidad}</Col>
                  <Col span={5}>{item.producto.product_name}</Col>
                  <Col span={2}>{item.iva ? `${item.iva} %` : '0 %'}</Col>
                  <Col span={2}>$ {item.total}</Col>
                </Row>
              ))}

              <Divider />

              <Row justify='start' className='header-table'>
                <Col span={3}>Total</Col>
                <Col span={3}>Desc</Col>
                <Col span={3}>Efec</Col>
                <Col span={3}>Camb</Col>
              </Row>
              <Row justify='start' className='body-table'>
                <Col span={3}>$ {total}</Col>
                <Col span={3}>{desc} %</Col>
                <Col span={3}>$ {efect}</Col>
                <Col span={3}>$ {camb}</Col>
              </Row>

              <br />

              <p style={{ marginLeft: 20 }}>¡GRACIAS POR SU COMPRA!</p>
              <Divider />

              <Button
                className='oculto-impresion'
                type='primary'
                style={{ marginLeft: 20 }}
                onClick={() => window.print()}>
                Imprimir factura
              </Button>
            </>
          )}
        </Col>
      </Row>
    </>
  );
}
