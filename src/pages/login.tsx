import React, { useEffect, useState } from 'react';
import { Head } from '../components/header/head';
import { Row, Col } from 'antd';
import { useParams } from 'react-router-dom';
import "./stylesPage/login.scss";
import { FormLogin } from '../components/login/iniciar-session/FromLogin';
import { FormRegistroUser } from '../components/login/registroUser/form-registro-user';

interface Params {
    form: string
}

export function LoginPage(){
    const [isRegistro, setIsRegistro] = useState<boolean>(false);
    const params: Params = useParams();

    useEffect( () => {
        if(params.form === "registro-user"){
            setIsRegistro(true);
        }else{
            setIsRegistro(false);
        }
    },[params]);

    return (
        <>
        <Head title='Login' />
            <div className='login'>
                <div className='children-login'>
                    <Row justify='center' className='row-login'>
                        <Col xs={24} md={10}>
                            <div className='login-izquierdo'>
                                <div className='content-login-izquierdo'>
                                    <figure>
                                        <img src='../logo-farmacia-removebg-preview.png' alt='logo-farmacia-juanito' />
                                    </figure>
                                    <h1>Farmacia Juanito</h1>
                                    <p>Tu farmcia de la esquina desde 1992</p>
                                </div>
                            </div>
                        </Col>
                        <Col xs={24} md={10}>
                            <div className='login-derecha'>
                                {isRegistro ? <FormRegistroUser /> : <FormLogin />}
                            </div>
                        </Col>
                    </Row>
                </div>
            </div>
        </>
    );
};