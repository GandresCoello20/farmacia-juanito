import React, { useEffect, useState } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Dispatch } from '../redux';
import { Producto_proveedor_INT, ResponseAxios, Notificacion } from '../interface';
import { AxiosResponse } from 'axios';
import moment from 'moment';
import { PagarProductoProveedor } from '../api/fetch/proveedores';
import { SetNotificacion } from '../redux/modulos/notificaciones';
import { ConfirmDelete } from '../components/confirm/confirmDelete';
import { TableSkeleton } from '../components/skeleton/tabla-skeleton';
import { obtenerProductoProveedor } from '../api/proveedores';
import { Row, Col, message, Alert, Tag, Button } from 'antd';
import { Nav } from '../components/header/nav';

interface Params {
  id_proveedor: string;
  proveedor: string;
}

export default function ProveedorDetallePage() {
  let notificaciones: Notificacion;
  const dispatch: Dispatch = useDispatch();
  const history = useHistory<typeof useHistory>();
  const params: Params = useParams();
  const [isLoading, setIsLoading] = useState<boolean>(true);
  const [Proveedor, setProveedor] = useState<string>('Anonimo');
  const [productosProveedor, setProductosProveedor] = useState<Producto_proveedor_INT[]>([]);

  const validar_status = (estado: string | undefined): string => {
    switch (estado) {
      case 'Pagado':
        return 'body-table alert-success';
      case 'Saldo pendiente':
        return 'body-table alert-warning';
      default:
        return 'body-table alert-danger';
    }
  };

  const pagarProducto = async (id_producto_proveedor: string) => {
    const resPago: ResponseAxios = await PagarProductoProveedor(id_producto_proveedor);

    notificaciones = {
      type: resPago.respuesta.type,
      content: resPago.respuesta.feeback,
      date: new Date(),
    };

    dispatch(SetNotificacion(notificaciones));

    if (resPago.respuesta.type !== 'ERROR') {
      message.success(resPago.respuesta.feeback);
      history.push('/proveedores');
    } else {
      message.error(resPago.respuesta.feeback);
    }
  };

  useEffect(() => {
    setIsLoading(true);
    setProveedor(params.proveedor);

    const getProductosProveedor = async (id_proveedor: string) => {
      try {
        const resPP: AxiosResponse = await obtenerProductoProveedor(id_proveedor);
        setProductosProveedor(resPP.data);
        setIsLoading(false);
      } catch (error) {
        message.error(error.message);
      }
    };

    getProductosProveedor(params.id_proveedor);
  }, [params]);

  return (
    <>
      <Nav title='Productos de Proveedor' />
      <h2 className='title-page'>
        Detalles sobre este proveedor:{' '}
        <span
          style={{
            background: '#cdcdcd',
            padding: 4,
            borderBottom: 4,
            borderBottomStyle: 'solid',
            borderBottomColor: '#696969',
          }}>
          {Proveedor}
        </span>
      </h2>
      <Row justify='center' className='header-table'>
        <Col span={4}>Laboratorio</Col>
        <Col span={4}>Descripcion</Col>
        <Col span={4}>Fecha Ingreso</Col>
        <Col span={2}>Fecha Pago</Col>
        <Col span={2}>Estado</Col>
        <Col span={1}>Total</Col>
        <Col span={2}>Abonado</Col>
        <Col span={4}>Opciones</Col>
      </Row>

      {isLoading && <TableSkeleton />}

      {productosProveedor.map(producto => (
        <Row
          justify='center'
          className={validar_status(producto.estado_pp)}
          key={producto.id_product_proveedor}>
          <Col span={4}>{producto.nombre_laboratorio}</Col>
          <Col span={4}>{producto.descripcion}</Col>
          <Col span={4}>{moment(producto.fecha_ingreso).format('L, LTS')}</Col>
          <Col span={2}>{moment(producto.fecha_pago).format('l')}</Col>
          <Col span={2}>
            {producto.estado_pp === 'Pagado' ? (
              <Tag color='lime'>{producto.estado_pp}</Tag>
            ) : (
              <Tag color='default'>{producto.estado_pp}</Tag>
            )}
          </Col>
          <Col span={1}>
            <Tag color='gold'>$ {producto.total}</Tag>
          </Col>
          <Col span={2}>
            <Tag color='purple'>$ {producto.abonado}</Tag>
          </Col>
          <Col span={4}>
            <Button
              type='primary'
              onClick={() => pagarProducto(producto.id_product_proveedor)}
              disabled={producto.estado_pp === 'Pagado'}>
              Pagado
            </Button>
            &nbsp; &nbsp;
            <ConfirmDelete id={producto.id_product_proveedor} tabla='productoProveedor' />
          </Col>
        </Row>
      ))}

      {productosProveedor.length === 0 && (
        <Alert type='info' message='No existen productos de este proveedor...!!' />
      )}
    </>
  );
}
