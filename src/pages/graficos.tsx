import React from 'react';
import { Nav } from '../components/header/nav';
import { VentasEstadisticas } from '../components/graficos/ventas_por_dia';
import { VentasMeses } from '../components/graficos/ventas_en_mese';
import { Row, Col } from 'antd';

export default function GraficosPage() {
  return (
    <>
      <Nav title='Graficos' />
      <h2 className='title-page'>Graficos</h2>
      <Row justify='center'>
        <Col span={22}>
          <VentasEstadisticas />
        </Col>
        <Col span={22}>
          <h3 className='title-page'>Monto de Ventas por mes</h3>
          <VentasMeses />
        </Col>
      </Row>
    </>
  );
}
