import React, { useState } from 'react';
import { Row, Col, DatePicker, message } from 'antd';
import { Nav } from '../components/header/nav';
import { useDispatch } from 'react-redux';
import moment from 'moment';
import { Dispatch } from '../redux';
import { obtenerVentas } from '../api/ventas';
import { fecha_actual } from '../hooks/fechas';
import { VentasEstadisticas } from '../components/graficos/ventas_por_dia';
import { TableVentas } from '../components/ventas/table-ventas';
import { SearchVentasPropiedades } from '../components/ventas/searchVentas';
import { SetVentas } from '../redux/modulos/ventas';

export function VentasPage() {
  const dispatch: Dispatch = useDispatch();
  const [dateVenta, setDateVenta] = useState<string>(fecha_actual());
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const actionDateVenta = async (date: any, dateString: string) => {
    setIsLoading(true);
    setDateVenta(dateString);

    try {
      const resFactura = await obtenerVentas(dateString);
      dispatch(SetVentas([...resFactura.data]));
    } catch (error) {
      message.error(error.message);
    }

    setIsLoading(false);
  };

  return (
    <>
      <Nav title='Ventas' />
      <h2 className='title-page'>
        Ventas de <u>{moment(dateVenta).format('LL')}</u>
      </h2>
      <Row justify='center'>
        <Col span={5}>
          <SearchVentasPropiedades />
        </Col>
        <Col span={5}>
          <DatePicker
            placeholder='Buscar por fecha'
            defaultValue={moment(dateVenta)}
            onChange={actionDateVenta}
          />
        </Col>
        <Col span={20}>
          <VentasEstadisticas />
        </Col>
        <br />
        <br />
        <Col span={22}>
          <TableVentas loadingDate={isLoading} dateVenta={dateVenta} />
        </Col>
      </Row>
      <br />
      <br />
    </>
  );
}
