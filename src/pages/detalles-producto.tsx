import React from 'react';
import { Row, Col, Alert } from 'antd';
import { useSelector } from 'react-redux';
import { RootState } from '../redux';
import { TableSkeleton } from '../components/skeleton/tabla-skeleton';
import { ConfirmDelete } from '../components/confirm/confirmDelete';
import { BtnEdit } from '../components/edit/btn-edit-drawer';
import { Nav } from '../components/header/nav';
import { Principio_activo, Laboratorio, Producto_name } from '../interface';

export default function DetallesProdutoPage() {
  const PropiedadesProductReducer = useSelector((state: RootState) => state.PropiedadesReducer);

  return (
    <>
      <Nav title='Detalles Productos' />
      <h2 className='title-page'>Propiedades de los productos</h2>
      <Row justify='space-between'>
        <Col span={8}>
          <Row justify='center'>
            <Col span={24} className='header-table'>
              Principio Activo
            </Col>
            {PropiedadesProductReducer.loading_principio_activo && <TableSkeleton />}
            {PropiedadesProductReducer.principio_activo.length === 0 ? (
              <Alert message='No esxiten datos para mostrar' type='info' />
            ) : (
              PropiedadesProductReducer.principio_activo.map((item: Principio_activo) => (
                <>
                  <Col span={18} key={item.id_principio_activo} className='body-table'>
                    {item.principio_activo}
                  </Col>
                  <Col span={3} className='body-table'>
                    <BtnEdit icono={true} formulario='principio-activo' />
                  </Col>
                  <Col span={3} className='body-table'>
                    <ConfirmDelete
                      icono={true}
                      id={item.id_principio_activo}
                      tabla='principio_activo'
                    />
                  </Col>
                </>
              ))
            )}
          </Row>
        </Col>
        <Col span={8}>
          <Row justify='center'>
            <Col span={24} className='header-table'>
              Laboratorio
            </Col>
            {PropiedadesProductReducer.loading_laboratorio && <TableSkeleton />}
            {PropiedadesProductReducer.laboratorio.length === 0 ? (
              <Alert message='No existen datos para mostrar' type='info' />
            ) : (
              PropiedadesProductReducer.laboratorio.map((item: Laboratorio) => (
                <>
                  <Col span={18} key={item.id_name_laboratorio} className='body-table'>
                    {item.nombre_laboratorio}
                  </Col>
                  <Col span={3} className='body-table'>
                    <BtnEdit icono={true} formulario='laboratorio' />
                  </Col>
                  <Col span={3} className='body-table'>
                    <ConfirmDelete
                      icono={true}
                      id={item.id_name_laboratorio}
                      tabla='nombre_laboratorio'
                    />
                  </Col>
                </>
              ))
            )}
          </Row>
        </Col>
        <Col span={8}>
          <Row justify='center'>
            <Col span={24} className='header-table'>
              Producto
            </Col>
            {PropiedadesProductReducer.loading_nombre && <TableSkeleton />}
            {PropiedadesProductReducer.nombre.length === 0 ? (
              <Alert message='No existen datos para mostrar' />
            ) : (
              PropiedadesProductReducer.nombre.map((item: Producto_name) => (
                <>
                  <Col span={18} key={item.id_product_name} className='body-table'>
                    {item.product_name}
                  </Col>
                  <Col span={3} className='body-table'>
                    <BtnEdit icono={true} formulario='product-name' />
                  </Col>
                  <Col span={3} className='body-table'>
                    <ConfirmDelete icono={true} id={item.id_product_name} tabla='nombre_producto' />
                  </Col>
                </>
              ))
            )}
          </Row>
        </Col>
      </Row>
    </>
  );
}
