import React, { useState } from 'react';
import { Nav } from '../components/header/nav';
import moment from 'moment';
import { obtenerPrestamosPorFecha } from '../api/prestamos';
import { TablePrestamos } from '../components/prestamos/table-prestamos';
import { Row, Col, DatePicker, message } from 'antd';
import { Dispatch, RootState } from '../redux';
import { SetPrestamos } from '../redux/modulos/prestamos';
import { useDispatch, useSelector } from 'react-redux';
import { LimpiarPrestamos } from '../components/prestamos/limpiar-prestamos';
import { Prestamo_INT } from '../interface';

export default function Prestamos() {
  const dispatch: Dispatch = useDispatch();
  const [SearchFechaPrestamo, setSearchPrestamo] = useState<string>('');
  const prestamos: Array<Prestamo_INT> = useSelector(
    (state: RootState) => state.PrestamosReducer.Prestamos,
  );

  const searchForVentas = async (date: any, dateString: string) => {
    setSearchPrestamo(dateString);

    try {
      const resPrestamos = await obtenerPrestamosPorFecha(dateString);

      dispatch(SetPrestamos([...resPrestamos.data]));
    } catch (error) {
      message.error(error.message);
    }
  };

  return (
    <>
      <Nav title='Flujo de caja' />
      <h2 className='title-page'>
        Mis Prestamos de:{' '}
        <u>
          {SearchFechaPrestamo
            ? moment(SearchFechaPrestamo).format('LL')
            : `${prestamos.length} registros`}
        </u>
      </h2>
      <Row justify='center'>
        <Col span={4}>
          <DatePicker placeholder='Seleccionar fecha' onChange={searchForVentas} />
        </Col>
        <Col span={4}>
          <LimpiarPrestamos />
        </Col>
        <Col span={22}>
          <br />
          <TablePrestamos searchFecha={SearchFechaPrestamo} />
        </Col>
      </Row>
    </>
  );
}
