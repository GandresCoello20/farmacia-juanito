import React, { useState } from 'react';
import { Row, Col, Button } from 'antd';
import { Nav } from '../components/header/nav';
import { AxiosResponse } from 'axios';
import { message } from 'antd';
import { DOMAIN } from '../config/domain';
import { SearchProduct } from '../components/productos/search-product';
import { reporteProductos } from '../api/productos';
import { TableMisProductos } from '../components/productos/mis-productos';
import { ActionStock } from '../components/stock/action-stock';

export function StockPage() {
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const reporte_productos = async () => {
    setIsLoading(true);
    try {
      const resResporte: AxiosResponse = await reporteProductos();
      if (resResporte.data.feeback) {
        window.open(`${DOMAIN}/static/reporte-productos.csv`);
      }
      setIsLoading(false);
    } catch (error) {
      message.error(error.message);
    }
    setIsLoading(false);
  };

  return (
    <>
      <Nav title='Stock' />
      <h2 className='title-page'>Agregar y actualizar productos en stock</h2>
      <Row justify='center'>
        <Col span={22}>
          <Row justify='space-between'>
            <Col span={15}>
              <ActionStock />
            </Col>
            <Col span={6}>
              <SearchProduct />
            </Col>
            <Col span={2}>
              <Button type='primary' loading={isLoading} onClick={reporte_productos}>
                Generar CSV
              </Button>
            </Col>
          </Row>
        </Col>
        <Col span={22}>
          <TableMisProductos page='stock' />
        </Col>
      </Row>
    </>
  );
}
