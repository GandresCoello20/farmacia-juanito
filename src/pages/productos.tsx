import React from 'react';
import { Row, Col } from 'antd';
import { Nav } from '../components/header/nav';
import { StepsProducto } from '../components/productos/steps';
import { SearchProduct } from '../components/productos/search-product';
import { TableMisProductos } from '../components/productos/mis-productos';

export function ProductosPage() {
  return (
    <>
      <Nav title='Productos' />
      <Row justify='center'>
        <Col span={18}>
          <StepsProducto current={0} />
        </Col>
      </Row>
      <h2 className='title-page'>Todo los productos ingresados</h2>
      <Row justify='center'>
        <Col span={6}>
          <SearchProduct />
        </Col>
        <Col span={22}>
          <TableMisProductos page='productos' />
        </Col>
      </Row>
    </>
  );
}
