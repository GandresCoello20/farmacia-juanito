import React from 'react';
import { Nav } from '../components/header/nav';
import { Row, Col } from 'antd';
import { TableClient } from '../components/clientes/table-clientes';
import { SearchClient } from '../components/clientes/search-client';
import { ModalCreateClient } from '../components/clientes/modalCreateClient';

export default function ClientePage() {
  return (
    <>
      <Nav title='Clientes' />
      <h2 className='title-page'>Todo los clientes</h2>
      <Row justify='center'>
        <Col span={5}>
          <ModalCreateClient />
        </Col>
        <Col span={5}>
          <SearchClient />
        </Col>
        <Col span={22}>
          <TableClient />
        </Col>
      </Row>
    </>
  );
}
