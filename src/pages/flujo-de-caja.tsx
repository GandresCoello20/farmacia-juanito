import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { Nav } from '../components/header/nav';
import { obtenerMontoTotaldeVentaPorFecha } from '../api/ventas';
import { TablePrestamos } from '../components/prestamos/table-prestamos';
import { CardFlujoCaja } from '../components/flujo-de-caja/card-flujo-caja';
import { ModalCreatePrestamo } from '../components/prestamos/modal-create-prestamos';
import { Row, Col, Button, Divider, message } from 'antd';
import { fecha_actual } from '../hooks/fechas';
import { obtenerMontoTotalPorFecha } from '../api/prestamos';

export default function FlujoCaja() {
  const [ingresos, setIngresos] = useState<number>(0);
  const [egresos, setEgresos] = useState<number>(0);
  const [saldo, setSaldo] = useState<number>(0);

  useEffect(() => {
    const obtenerMontos = async () => {
      try {
        const resMontoPrestamo = await obtenerMontoTotalPorFecha(fecha_actual());

        if (resMontoPrestamo.data.total) {
          setEgresos(egresos + resMontoPrestamo.data.total);
        }

        const resMontoVenta = await obtenerMontoTotaldeVentaPorFecha(fecha_actual());

        if (resMontoVenta.data.total) {
          setIngresos(ingresos + resMontoVenta.data.total);
        }
      } catch (error) {
        message.error(error.message);
      }
    };

    obtenerMontos().then(() => setSaldo(ingresos - egresos));
  }, [egresos, ingresos]);
  return (
    <>
      <Nav title='Flujo de caja' />
      <h2 className='title-page'>Ultimos 8 Prestamos</h2>
      <Row justify='center'>
        <Col span={4}>
          <Link to='/prestamos'>
            <Button type='primary'>Mis Prestamos</Button>
          </Link>
        </Col>
        <Col span={4}>
          <ModalCreatePrestamo saldo={saldo} />
        </Col>
        <Col span={22}>
          <br />
          <TablePrestamos limit={8} />
        </Col>
      </Row>
      <Divider />
      <Row justify='center'>
        <Col span={22}>
          <h3 className='title-page'>Detalles</h3>
          <CardFlujoCaja ingresos={ingresos} egresos={egresos} saldo={saldo} />
        </Col>
      </Row>
    </>
  );
}
