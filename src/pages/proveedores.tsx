import React from 'react';
import { Nav } from '../components/header/nav';
import { ModalCreateProveedor } from '../components/proveedores/modal-create-proveedor';
import { TableProveedor } from '../components/proveedores/table-proveedor';
import { SearchProveedor } from '../components/proveedores/search-proveedor';
import { Row, Col } from 'antd';

export default function ProveedorPage() {
  return (
    <>
      <Nav title='Proveedores' />
      <h2 className='title-page'>Todo los proveedores</h2>
      <Row justify='center'>
        <Col span={6}>
          <ModalCreateProveedor />
        </Col>
        <Col span={6}>
          <SearchProveedor />
        </Col>
        <Col span={22}>
          <TableProveedor />
        </Col>
      </Row>
    </>
  );
}
