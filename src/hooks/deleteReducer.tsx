import {
  Principio_activo,
  Laboratorio,
  Producto_name,
  Producto_INT,
  Usuario_INT,
  Cliente_INT,
  Factura_INT,
  Proveedor_INT,
  Prestamo_INT,
} from '../interface';

export const RemoveStorePrincipe = (store: any, id: string | number) => {
  const principioActive: Array<Principio_activo> = store.PropiedadesReducer.principio_activo;
  const index: number = principioActive.findIndex(item => item.id_principio_activo === id);

  principioActive.splice(index, 1);
  return principioActive;
};

export const RemoveStoreLaboratorio = (store: any, id: string | number) => {
  const Lab: Array<Laboratorio> = store.PropiedadesReducer.laboratorio;
  const index: number = Lab.findIndex(item => item.id_name_laboratorio === id);

  Lab.splice(index, 1);
  return Lab;
};

export const RemoveStoreProductName = (store: any, id: string | number) => {
  const product: Array<Producto_name> = store.PropiedadesReducer.nombre;
  const index: number = product.findIndex(item => item.id_product_name === id);

  product.splice(index, 1);
  return product;
};

export const RemoveStoreProduct = (store: any, id: string | number) => {
  const product: Array<Producto_INT> = store.ProductosReducer.productos;
  const index: number = product.findIndex(item => item.id_producto === id);

  product.splice(index, 1);
  return product;
};

export const RemoveStoreUser = (store: any, id: string | number) => {
  const user: Array<Usuario_INT> = store.UsuarioReducer.usuarios;
  const index: number = user.findIndex(item => item.id_user === id);

  user.splice(index, 1);
  return user;
};

export const RemoveStoreClient = (store: any, id: string | number) => {
  const client: Array<Cliente_INT> = store.ClienteReducer.clientes;
  const index: number = client.findIndex(item => item.id_cliente === id);

  client.splice(index, 1);
  return client;
};

export const RemoveStoreFactura = (store: any, id: string | number) => {
  const factura: Array<Factura_INT> = store.VentasReduce.facturas;
  const index: number = factura.findIndex(item => item.id_factura === id);

  factura.splice(index, 1);
  return factura;
};

export const RemoveStoreProveedor = (store: any, id: string | number) => {
  const proveedor: Array<Proveedor_INT> = store.ProveedorReducer.proveedores;
  const index: number = proveedor.findIndex(item => item.id_proveedores === id);

  proveedor.splice(index, 1);
  return proveedor;
};

export const RemoveStorePrestamo = (store: any, id: string | number) => {
  const prestamo: Array<Prestamo_INT> = store.PrestamosReducer.Prestamos;
  const index: number = prestamo.findIndex(item => item.id_prestamo === id);

  prestamo.splice(index, 1);
  return prestamo;
};
