import { ResponseAxios, Notificacion } from '../interface';
import { EliminarProducto } from '../api/fetch/producto';
import {
  deleteLaboratorio,
  deletePrincipioActive,
  deleteProductName,
} from '../api/fetch/propiedades-productos';
import { DeleteUser } from '../api/fetch/usuarios';
import { DeleteFactura } from '../api/fetch/ventas';
import { eliminarClient } from '../api/fetch/clientes';
import { DeletePrestamo } from '../api/fetch/prestamos';
import { DeleteProveedor, DeleteProductoProveedor } from '../api/fetch/proveedores';

let notificacion: Notificacion;

export const eliminarProducto = async (id: string | number) => {
  const res: ResponseAxios = await EliminarProducto(id);

  return generateNotificacion(res);
};

export const eliminarPrincipioActive = async (id: string | number) => {
  const res: ResponseAxios = await deletePrincipioActive(id);

  return generateNotificacion(res);
};

export const eliminarLaboratorio = async (id: number | string) => {
  const res: ResponseAxios = await deleteLaboratorio(id);

  return generateNotificacion(res);
};

export const eliminarProductName = async (id: string | number) => {
  const res: ResponseAxios = await deleteProductName(id);

  return generateNotificacion(res);
};

export const eliminarUsuario = async (id: string | number) => {
  const res: ResponseAxios = await DeleteUser(id);

  return generateNotificacion(res);
};

export const eliminarCliente = async (id: string | number) => {
  const res: ResponseAxios = await eliminarClient(id);

  return generateNotificacion(res);
};

export const eliminarFactura = async (id: string | number) => {
  const res: ResponseAxios = await DeleteFactura(id);

  return generateNotificacion(res);
};

export const eliminarProveedor = async (id: string | number) => {
  const res: ResponseAxios = await DeleteProveedor(id);

  return generateNotificacion(res);
};

export const eliminarProductoProveedor = async (id: string | number) => {
  const res: ResponseAxios = await DeleteProductoProveedor(id);

  return generateNotificacion(res);
};

export const eliminarPrestamo = async (id: string | number) => {
  const res: ResponseAxios = await DeletePrestamo(id);

  return generateNotificacion(res);
};

const generateNotificacion = (res: ResponseAxios) => {
  notificacion = { type: res.respuesta.type, content: res.respuesta.feeback, date: new Date() };
  return { respuesta: res.respuesta, notificacion };
};
