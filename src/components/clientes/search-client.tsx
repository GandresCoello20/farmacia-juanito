/* eslint-disable array-callback-return */
import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Cliente_INT } from '../../interface';
import { RootState, Dispatch } from '../../redux';
import { SetSearchtCliente } from '../../redux/modulos/clientes';
import { Input, Select, Form, Button } from 'antd';
import { SearchOutlined } from '@ant-design/icons';

export function SearchClient() {
  const dispatch: Dispatch = useDispatch();
  const { Option } = Select;
  const [form] = Form.useForm();
  const ClienteReducer = useSelector((state: RootState) => state.ClienteReducer);
  const [propiedadesCliente, setPropiedadesClient] = useState<string>('Nombres');
  const [writeSearch, setWriteSearch] = useState<string>('');

  const buscar_por_propiedades = (search: string, propiedad: string) => {
    return ClienteReducer.clientes.filter((cliente: Cliente_INT) => {
      switch (propiedad) {
        case 'Nombres':
          return cliente.nombres?.indexOf(search) !== -1;
        case 'Apellidos':
          return cliente.apellidos?.indexOf(search) !== -1;
        case 'Identificacion':
          return cliente.identificacion.toString().indexOf(search) !== -1;
        case 'Correo':
          return cliente.correo.indexOf(search) !== -1;
      }
    });
  };

  const InputSearchClient = (event: React.ChangeEvent<HTMLInputElement>) =>
    setWriteSearch(event.target.value);

  const send = () => {
    let result: Array<Cliente_INT>;
    result = buscar_por_propiedades(writeSearch, propiedadesCliente);
    dispatch(SetSearchtCliente([...result]));
  };

  return (
    <>
      <Form form={form} onFinish={send} name='dynamic_rule'>
        <Input.Group compact>
          <Select defaultValue='Nombres' onChange={(value: string) => setPropiedadesClient(value)}>
            <Option value='Nombres'>Nombres</Option>
            <Option value='Apellidos'>Apellidos</Option>
            <Option value='Identificacion'>Identificacion</Option>
            <Option value='Correo'>Correo</Option>
          </Select>
          <Input
            type='search'
            style={{ width: '50%' }}
            onChange={InputSearchClient}
            placeholder='Buscar Cliente...'
          />
          <Button htmlType='submit'>
            <SearchOutlined />
          </Button>
        </Input.Group>
      </Form>
      <br />
    </>
  );
}
