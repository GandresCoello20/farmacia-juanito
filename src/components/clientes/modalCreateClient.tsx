import React, { useState } from 'react';
import { createClient } from '../../api/fetch/clientes';
import { Dispatch, RootState } from '../../redux';
import { useDispatch, useStore } from 'react-redux';
import { ResponseAxios, Notificacion } from '../../interface';
import { SetNotificacion } from '../../redux/modulos/notificaciones';
import { SetCliente } from '../../redux/modulos/clientes';
import { Button, Modal, Form, Input, message } from 'antd';

export function ModalCreateClient() {
  const dispatch: Dispatch = useDispatch();
  const store: RootState = useStore().getState();
  let notificacion: Notificacion;
  const [form] = Form.useForm();
  const { TextArea } = Input;
  const [visible, setVisible] = useState<boolean>(false);

  const send = async (data: any) => {
    const resClien: ResponseAxios = await createClient(data);

    if (resClien.respuesta.type === 'ERROR') {
      message.error(resClien.respuesta.feeback);
      notificacion = {
        type: resClien.respuesta.type,
        content: resClien.respuesta.feeback,
        date: new Date(),
      };
    } else {
      notificacion = {
        type: resClien.respuesta.type,
        content: resClien.respuesta.feeback,
        date: new Date(),
      };
      dispatch(SetCliente([...store.ClienteReducer.clientes, ...resClien.axios.data]));
      message.success(`SE REGISTRO EL NUEVO CLIENTE: ${String(data.nombre).toUpperCase()}`);
      form.resetFields();
    }
    dispatch(SetNotificacion(notificacion));
  };

  return (
    <>
      <Button type='ghost' onClick={() => setVisible(true)}>
        Agregar nuevo cliente
      </Button>
      <Modal
        title='Agregar nuevo cliente'
        visible={visible}
        onOk={() => setVisible(false)}
        onCancel={() => setVisible(false)}>
        <Form form={form} onFinish={send}>
          <Form.Item
            name='nombre'
            label='Nombres'
            rules={[
              {
                required: true,
                message: 'Necesitas ingresar por lo menos 1 nombre',
              },
            ]}>
            <Input placeholder='Ingrese los nombres' />
          </Form.Item>
          <Form.Item
            name='apellido'
            label='Apellidos'
            rules={[
              {
                required: true,
                message: 'Necesitas ingresar por lo menos 1 apellido',
              },
            ]}>
            <Input placeholder='Ingrese los apellidos' />
          </Form.Item>
          <Form.Item
            name='identificacion'
            label='Cdi / Ruc'
            rules={[
              {
                required: true,
                message: 'Necesitas ingresar el numero de identidad',
              },
            ]}>
            <Input type='number' placeholder='# de Identificacion' />
          </Form.Item>
          <Form.Item name='correo' label='Correo'>
            <Input type='email' placeholder='Ingrese la direccion de correo' />
          </Form.Item>
          <Form.Item name='direccion'>
            <TextArea rows={3} placeholder='Direccion del cliente...' />
          </Form.Item>

          <Button htmlType='submit' block type='primary'>
            Guardar
          </Button>
        </Form>
      </Modal>
    </>
  );
}
