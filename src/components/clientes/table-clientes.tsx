import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux';
import { Row, Col } from 'antd';
import { Cliente_INT } from '../../interface';
import { ConfirmDelete } from '../confirm/confirmDelete';
import { TableSkeleton } from '../skeleton/tabla-skeleton';
import { BtnEdit } from '../edit/btn-edit-drawer';

export function TableClient() {
  const [thisClient, setThisClien] = useState<Cliente_INT[]>([]);
  const ClienteReducer = useSelector((state: RootState) => state.ClienteReducer);

  useEffect(() => {
    if (ClienteReducer.searchClientes.length > 0) {
      setThisClien(ClienteReducer.searchClientes);
    } else {
      setThisClien(ClienteReducer.clientes);
    }
  }, [ClienteReducer]);

  return (
    <>
      <Row justify='center' className='header-table'>
        <Col span={4}>Nombres</Col>
        <Col span={4}>Apellidos</Col>
        <Col span={3}>Cdi / Ruc</Col>
        <Col span={4}>Correo</Col>
        <Col span={5}>Direccion</Col>
        <Col span={3}>Opciones</Col>
      </Row>
      {ClienteReducer.loading && <TableSkeleton />}
      {thisClient.map((cliente: Cliente_INT) => (
        <Row justify='center' className='body-table' key={cliente.id_cliente}>
          <Col span={4}>{cliente.nombres}</Col>
          <Col span={4}>{cliente.apellidos}</Col>
          <Col span={3}>{cliente.identificacion}</Col>
          <Col span={4}>{cliente.correo}</Col>
          <Col span={5}>{cliente.direccion}</Col>
          <Col span={3}>
            <BtnEdit icono={true} formulario='cliente' />
            &nbsp; &nbsp;
            <ConfirmDelete icono={true} id={cliente.id_cliente} tabla='cliente' />
          </Col>
        </Row>
      ))}
    </>
  );
}
