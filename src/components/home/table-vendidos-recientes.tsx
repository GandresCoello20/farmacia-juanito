import React from 'react';
import { TableVentas } from '../../components/ventas/table-ventas';
import { fecha_actual } from '../../hooks/fechas';

export function TableVendidosReciente() {
  return (
    <>
      <TableVentas limit={8} dateVenta={fecha_actual()} />
    </>
  );
}
