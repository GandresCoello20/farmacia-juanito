import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux';
import { Producto_INT } from '../../interface';
import { TableSkeleton } from '../skeleton/tabla-skeleton';
import { Row, Col, Alert } from 'antd';
import { DetallesProductos } from '../productos/detalles-productos';

export function TableCaducados() {
  const ProductReducer = useSelector((state: RootState) => state.ProductosReducer);

  return (
    <>
      <Row justify='center' className='header-table'>
        <Col span={4}>Nombre</Col>
        <Col span={4}>Laboratorio</Col>
        <Col span={2}>Cant-Disp</Col>
        <Col span={2}>Medidas</Col>
        <Col span={3}>Presentacion</Col>
        <Col span={2}>Lote</Col>
        <Col span={1}>PVF</Col>
        <Col span={1}>PVP</Col>
        <Col span={2}>Opciones</Col>
      </Row>
      {ProductReducer.loadingCaducado && <TableSkeleton />}
      {ProductReducer.productosCaducados.slice(0, 8).map((producto: Producto_INT) => (
        <Row justify='center' className='body-table alert-danger' key={producto.id_producto}>
          <>
            <Col span={4}>{producto.product_name}</Col>
            <Col span={4}>{producto.nombre_laboratorio}</Col>
            <Col span={2}>
              {producto.cantidad} / {producto.cantidad_disponible}
            </Col>
            <Col span={2}>
              {producto.medida} {producto.tipo_medida}
            </Col>
            <Col span={3}>{producto.presentacion}</Col>
            <Col span={2}>{producto.lote}</Col>
            <Col span={1}>{producto.pvf}</Col>
            <Col span={1}>{producto.pvp}</Col>
            <Col span={2}>
              <DetallesProductos producto={producto} />
            </Col>
          </>
        </Row>
      ))}
      {ProductReducer.productosCaducados.length === 0 && (
        <Alert type='info' message={`No existen productos caducados`} />
      )}
    </>
  );
}
