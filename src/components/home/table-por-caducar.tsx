import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux';
import moment from 'moment';
import { Producto_INT } from '../../interface';
import { TableSkeleton } from '../skeleton/tabla-skeleton';
import { Row, Col, Alert } from 'antd';
import { DetallesProductos } from '../productos/detalles-productos';
import { fecha_actual } from '../../hooks/fechas';

export function TablePorCaducar() {
  const [PorCaducar, setPorCaducar] = useState<Producto_INT[]>([]);
  const ProductReducer = useSelector((state: RootState) => state.ProductosReducer);

  useEffect( () => {
    const arr: Array<Producto_INT> = [];
    // eslint-disable-next-line array-callback-return
    ProductReducer.productos.map( (producto: Producto_INT) => {
      if(moment(producto.fecha_caducidad).diff(fecha_actual(), 'days') < 60){
        return arr.push(producto);
      }
    });

    setPorCaducar(arr);
  }, [ProductReducer]);

  return (
    <>
      <Row justify='center' className='header-table'>
        <Col span={4}>Nombre</Col>
        <Col span={4}>Laboratorio</Col>
        <Col span={2}>Cant-Disp</Col>
        <Col span={2}>Medidas</Col>
        <Col span={3}>Presentacion</Col>
        <Col span={2}>Lote</Col>
        <Col span={1}>PVF</Col>
        <Col span={1}>PVP</Col>
        <Col span={2}>Opciones</Col>
      </Row>
      {ProductReducer.loadingCaducado && <TableSkeleton />}
      {PorCaducar.map((producto: Producto_INT) => (
        <Row justify='center' className='body-table alert-warning' key={producto.id_producto}>
          <>
            <Col span={4}>{producto.product_name}</Col>
            <Col span={4}>{producto.nombre_laboratorio}</Col>
            <Col span={2}>
              {producto.cantidad} / {producto.cantidad_disponible}
            </Col>
            <Col span={2}>
              {producto.medida} {producto.tipo_medida}
            </Col>
            <Col span={3}>{producto.presentacion}</Col>
            <Col span={2}>{producto.lote}</Col>
            <Col span={1}>{producto.pvf}</Col>
            <Col span={1}>{producto.pvp}</Col>
            <Col span={2}>
              <DetallesProductos producto={producto} />
            </Col>
          </>
        </Row>
      ))}
      {PorCaducar.length === 0 && (
        <Alert
          type='info'
          message={`No existen productos caducados entre ( HOY ) y ( 60 dias ) hacia adelante`}
        />
      )}
    </>
  );
}
