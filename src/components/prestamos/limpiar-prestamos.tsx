import React from 'react';
import { useDispatch } from 'react-redux';
import { LimpiarPrestamo } from '../../api/fetch/prestamos';
import { Notificacion, ResponseAxios } from '../../interface';
import { SetNotificacion } from '../../redux/modulos/notificaciones';
import { SetPrestamos } from '../../redux/modulos/prestamos';
import { Dispatch } from '../../redux';
import { Tooltip, Button, message } from 'antd';

export function LimpiarPrestamos() {
  const dispatch: Dispatch = useDispatch();
  let notificaciones: Notificacion;

  const clear_prestamos = async () => {
    const resLimpio: ResponseAxios = await LimpiarPrestamo();

    if (resLimpio.respuesta.type === 'ERROR') {
      message.error(resLimpio.respuesta.feeback);
    } else {
      message.success(resLimpio.respuesta.feeback);
      dispatch(SetPrestamos([...resLimpio.axios.data.prestamos]));
    }

    notificaciones = {
      type: resLimpio.respuesta.type,
      content: resLimpio.respuesta.feeback,
      date: new Date(),
    };

    dispatch(SetNotificacion(notificaciones));
  };

  return (
    <>
      <Tooltip placement='right' title='Eliminara los prestamos pagados de los meses anteriores.'>
        <Button danger block onClick={clear_prestamos}>
          Limpiar prestamos
        </Button>
      </Tooltip>
    </>
  );
}
