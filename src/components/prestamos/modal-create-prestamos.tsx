import React, { useState } from 'react';
import { CreatePrestamos } from '../../api/fetch/prestamos';
import { ResponseAxios, Notificacion } from '../../interface';
import { SetNotificacion } from '../../redux/modulos/notificaciones';
import { Modal, Button, Form, Input, message, Alert, Tag, Divider } from 'antd';

interface Props {
  saldo: number;
}

export function ModalCreatePrestamo({ saldo }: Props) {
  let notificaciones: Notificacion;
  const [form] = Form.useForm();
  const { TextArea } = Input;

  const [isVisible, setIsVisible] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const send = async (data: any) => {
    const { descripcion, cantidad } = data;
    setIsLoading(true);

    if (Number(cantidad) < 0) {
      message.error('No se permiten numeros negativos.');
      setIsLoading(false);
      return false;
    }

    if (Number(cantidad) > saldo) {
      message.error('El valor supera al valor en caja.');
      setIsLoading(false);
      return false;
    }

    const resPrestamo: ResponseAxios = await CreatePrestamos(descripcion, Number(cantidad));

    notificaciones = {
      type: resPrestamo.respuesta.type,
      content: '',
      date: new Date(),
    };

    if (resPrestamo.respuesta.type === 'ERROR') {
      message.error(resPrestamo.respuesta.feeback);
      notificaciones.content = resPrestamo.respuesta.feeback;
    } else {
      message.success('SE REGISTRO EL PRESTAMO');
      notificaciones.content = 'SE REGISTRO EL PRESTAMO';
      form.resetFields();
      setIsVisible(false);
    }

    SetNotificacion(notificaciones);
    setIsLoading(false);
  };

  return (
    <>
      <Button type='ghost' onClick={() => setIsVisible(true)}>
        Tomar un prestamo
      </Button>
      <Modal title='Crear prestamo' visible={isVisible} onCancel={() => setIsVisible(false)}>
        <div style={{ textAlign: 'center', fontSize: 26 }}>
          En caja:{' '}
          <Tag color='success' style={{ fontSize: 26, padding: 8 }}>
            $ {saldo}
          </Tag>
        </div>

        <Divider />

        <Form form={form} onFinish={send} name='dynamic_rule'>
          <Form.Item
            label='Descripcion'
            name='descripcion'
            rules={[
              {
                required: true,
                message: 'Especifique para que es el prestamo',
              },
            ]}>
            <TextArea placeholder='Especificaciones del prestamo' cols={3} />
          </Form.Item>
          <Form.Item
            label='Cantidad'
            name='cantidad'
            rules={[
              {
                required: true,
                message: 'Especifique una monto para el prestamo',
              },
            ]}>
            <Input type='number' min={0} placeholder='Monto del prestamo $' />
          </Form.Item>

          <Form.Item>
            <Button htmlType='submit' block loading={isLoading} type='primary'>
              Guardar
            </Button>
          </Form.Item>
        </Form>

        <Alert type='warning' message='Se desminuye el saldo actual en caja.' />
      </Modal>
    </>
  );
}
