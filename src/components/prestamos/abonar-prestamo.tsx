import React, { useState } from 'react';
import { ResponseAxios, Notificacion, Prestamo_INT } from '../../interface';
import { useDispatch, useSelector } from 'react-redux';
import { Dispatch, RootState } from '../../redux';
import { SetNotificacion } from '../../redux/modulos/notificaciones';
import { SetPrestamos } from '../../redux/modulos/prestamos';
import { Incrementar_pago_abono_prestamo } from '../../api/fetch/prestamos';
import { Modal, Button, Form, Input, message, Tag, Divider } from 'antd';

interface Props {
  estado_prestamo: string;
  prestamo: number;
  abono_prestamo: number;
  id_prestamo: string;
}

interface Validate {
  status: string | any;
  help: string;
}

export function AbonarPrestamo({ estado_prestamo, prestamo, abono_prestamo, id_prestamo }: Props) {
  const [form] = Form.useForm();
  const dispatch: Dispatch = useDispatch();
  let notificaciones: Notificacion;

  const PrestamosReducer: Array<Prestamo_INT> = useSelector(
    (state: RootState) => state.PrestamosReducer.Prestamos,
  );

  const [isVisible, setIsVisible] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [isValidate, setIsValidate] = useState<Validate>({
    status: '',
    help: '',
  });

  const send = async (data: any) => {
    const { abono } = data;
    setIsLoading(true);

    if (Number(abono) < 0) {
      message.error('NO SE ACEPTAN NUMEROS NEGATIVOS');
      setIsLoading(false);
      return false;
    }

    if (Number(abono) < abono_prestamo) {
      message.error('PARA ABONAR DEBES DE ESPECIFICAR UN VALOR MAS ALTO QUE EL ACTUAL.');
      setIsLoading(false);
      return false;
    }

    if (Number(abono) > prestamo) {
      message.error(`Este valor de abono sobre pasa el limite de pago ($ ${prestamo})`);
      setIsLoading(false);
      return false;
    }

    const resAbonar: ResponseAxios = await Incrementar_pago_abono_prestamo(
      id_prestamo,
      Number(abono),
    );

    if (resAbonar.respuesta.type === 'ERROR') {
      message.error(resAbonar.respuesta.feeback);
    } else {
      message.success(resAbonar.respuesta.feeback);
      dispatch(SetPrestamos([...PrestamosReducer, ...resAbonar.axios.data.prestamo]));
      form.resetFields();
    }

    notificaciones = {
      type: resAbonar.respuesta.type,
      content: resAbonar.respuesta.feeback,
      date: new Date(),
    };

    dispatch(SetNotificacion(notificaciones));

    setIsLoading(false);
  };

  const validar_abono = (e: React.ChangeEvent<HTMLInputElement>): void => {
    const newAbono = Number(e.target.value);

    if (newAbono > prestamo) {
      setIsValidate({
        status: 'error',
        help: `Este valor de abono sobre pasa el limite de pago ($ ${prestamo})`,
      });
    } else {
      setIsValidate({
        status: 'success',
        help: 'Valor de abono aceptado.',
      });
    }

    if (newAbono <= abono_prestamo) {
      setIsValidate({
        status: 'error',
        help: 'Necesitas especificar un abono mayor que el actual.',
      });
    }
  };

  return (
    <>
      <Button
        type='primary'
        disabled={estado_prestamo === 'Pagado'}
        onClick={() => setIsVisible(true)}>
        Abonar
      </Button>
      <Modal title='Abonar Prestamo' visible={isVisible} onCancel={() => setIsVisible(false)}>
        <div style={{ textAlign: 'center', fontSize: 25 }}>
          Prestamo:{' '}
          <Tag color='volcano' style={{ fontSize: 25, padding: 10 }}>
            $ {prestamo}
          </Tag>
        </div>

        <Divider />

        <Form form={form} onFinish={send} name='dynamic_rule'>
          <Form.Item
            label='Abono'
            name='abono'
            validateStatus={isValidate.status}
            help={isValidate.help}
            rules={[
              {
                required: true,
                message: 'Especifique el abono del prestamo',
              },
            ]}>
            <Input
              placeholder='Especifique el abono del prestamo'
              defaultValue={abono_prestamo}
              onChange={validar_abono}
              min={abono_prestamo}
            />
          </Form.Item>

          <Form.Item>
            <Button htmlType='submit' block loading={isLoading} type='primary'>
              Agregar Abono
            </Button>
          </Form.Item>
        </Form>
      </Modal>
    </>
  );
}
