import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { TableSkeleton } from '../skeleton/tabla-skeleton';
import { ConfirmDelete } from '../confirm/confirmDelete';
import moment from 'moment';
import { AbonarPrestamo } from './abonar-prestamo';
import { RootState } from '../../redux';
import { Prestamo_INT } from '../../interface';
import { Row, Col, Tag, Alert } from 'antd';
import { fecha_actual } from '../../hooks/fechas';

interface Props {
  searchFecha?: string;
  limit?: number;
}

export function TablePrestamos({ searchFecha, limit }: Props) {
  const PrestamoReducer = useSelector((state: RootState) => state.PrestamosReducer);
  const [thisPrestamos, setThisPrestamos] = useState<Prestamo_INT[]>([]);

  useEffect(() => {
    setThisPrestamos(PrestamoReducer.Prestamos);

    if (limit) {
      thisPrestamos.slice(0, limit);
    }
  }, [PrestamoReducer, limit, thisPrestamos]);

  const validar_status = (estado: string | undefined): string => {
    switch (estado) {
      case 'Pagado':
        return 'body-table alert-success';
      case 'Saldo pendiente':
        return 'body-table alert-warning';
      default:
        return 'body-table alert-danger';
    }
  };

  return (
    <>
      <Row justify='center' className='header-table'>
        <Col span={4}>Descripcion</Col>
        <Col span={3}>Fecha</Col>
        <Col span={2}>Monto</Col>
        <Col span={3}>Estado</Col>
        <Col span={2}>Abono</Col>
        <Col span={4}>Opciones</Col>
      </Row>

      {PrestamoReducer.loading && <TableSkeleton />}

      {PrestamoReducer.Prestamos.map((prestamo: Prestamo_INT) => (
        <Row
          justify='center'
          className={validar_status(prestamo.estado_prestamo)}
          key={prestamo.id_prestamo}>
          <Col span={4}>{prestamo.descripcion_prestamo}</Col>
          <Col span={3}>
            <Tag color='volcano'>{moment(prestamo.fecha_prestamo).format('l, LTS')}</Tag>
          </Col>
          <Col span={2}>
            <Tag color='gold'>$ {prestamo.cantidad_prestamo}</Tag>
          </Col>
          <Col span={3}>
            <Tag color={prestamo.estado_prestamo === 'Pagado' ? 'green' : 'volcano'}>
              {prestamo.estado_prestamo}
            </Tag>
          </Col>
          <Col span={2}>
            <Tag color='pink'>$ {prestamo.abono_prestamo}</Tag>
          </Col>
          <Col span={4}>
            <AbonarPrestamo
              estado_prestamo={prestamo.estado_prestamo}
              prestamo={prestamo.cantidad_prestamo}
              abono_prestamo={prestamo.abono_prestamo}
              id_prestamo={prestamo.id_prestamo}
            />
            &nbsp; &nbsp;
            <ConfirmDelete id={prestamo.id_prestamo} tabla='prestamo' />
          </Col>
        </Row>
      ))}

      {PrestamoReducer.Prestamos.length === 0 && (
        <Alert
          type='info'
          message={`No existen prestamos para esta fecha: ${
            searchFecha ? searchFecha : fecha_actual()
          }`}
        />
      )}
    </>
  );
}
