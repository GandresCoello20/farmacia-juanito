import React, { useState } from 'react';
import { ProfileOutlined } from '@ant-design/icons';
import { Drawer, Button, Row, Col } from 'antd';
import { Producto_INT } from '../../interface/index';

interface Props {
    producto: Producto_INT;
    icono?: boolean;
}

export function DetallesProductos({ producto, icono }: Props){
    const [visible, setVisible] = useState<boolean>(false);
    return(
        <>
            <Button type="primary" onClick={() => setVisible(true)}>
                {icono ? <ProfileOutlined /> : 'Detalles'}
            </Button>
            <Drawer
                title="Detalles del productos"
                placement="right"
                width={300}
                closable={false}
                onClose={() => setVisible(false)}
                visible={visible}
            >
                <h2 style={{ textAlign: 'center' }}>Detalles del producto</h2>
                <Row justify='center' className="header-table" key={producto.id_producto}>
                    <Col span={22}>Nombre</Col>
                    <Col className='body-table'>{producto.product_name}</Col>
                    <Col span={22}>Laboratorio</Col>
                    <Col className='body-table'>{producto.nombre_laboratorio}</Col>
                    <Col span={22}>Principio Activo</Col>
                    <Col className='body-table'>{producto.principio_activo}</Col>
                    <Col span={22}>Cantidad y disponibles</Col>
                    <Col className='body-table'>{producto.cantidad} {producto.cantidad_disponible}</Col>
                    <Col span={22}>Medidas</Col>
                    <Col className='body-table'>{producto.medida} {producto.tipo_medida}</Col>
                    <Col span={22}>Presentacion</Col>
                    <Col className='body-table'>{producto.presentacion}</Col>
                    <Col span={22}>Registro sanitario</Col>
                    <Col className='body-table'>{producto.registro_sanitario}</Col>
                    <Col span={22}>Lote</Col>
                    <Col className='body-table'>{producto.lote}</Col>
                    <Col span={22}>PVF</Col>
                    <Col className='body-table'>$ {producto.pvf}</Col>
                    <Col span={22}>PVP</Col>
                    <Col className='body-table'>$ {producto.pvp}</Col>
                    <Col span={22}>Fecha elaboracion</Col>
                    <Col className='body-table'>{producto.fecha_elaboracion}</Col>
                    <Col span={22}>Fecha caducidad</Col>
                    <Col className='body-table'>{producto.fecha_caducidad}</Col>
                </Row>
            </Drawer>
        </>
    );
}