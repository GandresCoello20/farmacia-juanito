import React from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux';
import { Steps } from 'antd';

interface Props {
    current: number;
}

export function StepsProducto({ current }: Props){

    const carrito = useSelector((state: RootState) => state.CarritoReducer.carrito);
    const { Step } = Steps;
    const estado = carrito.length === 0 && current === 1 ? 'error' : 'process';

    return(
        <>
            <Steps current={current} status={estado}>
                <Step title="Productos" description="Selecciona los productos a comprar" />
                <Step title="Carrito" description="Especifica el formato y cantidad" />
                <Step title="Pago" description="Detalles de la compra" />
            </Steps>
        </>
    );
}
