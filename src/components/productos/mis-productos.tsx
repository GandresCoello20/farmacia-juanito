import React, { useEffect, useState } from 'react';
import { Row, Col, Alert } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { Producto_INT, Notificacion, Carrito_INT } from '../../interface/index';
import { RootState, Dispatch } from '../../redux';
import { DetallesProductos } from './detalles-productos';
import { ConfirmDelete } from '../confirm/confirmDelete';
import { BtnEdit } from '../edit/btn-edit-drawer';
import { TableSkeleton } from '../skeleton/tabla-skeleton';
import { SetCarrito } from '../../redux/modulos/carrito';
import { SetNotificacion } from '../../redux/modulos/notificaciones';
import './styles/productos-table.scss';
import { dosDecimales } from '../../hooks/decimales';

interface Props {
  page: string;
}

export function TableMisProductos({ page }: Props) {
  let notificacion: Notificacion;
  const [thisProduct, setThisProduct] = useState<Producto_INT[]>([]);
  const dispatch: Dispatch = useDispatch();

  const ProductosReducer = useSelector((state: RootState) => state.ProductosReducer);
  const CarritoReducer = useSelector((state: RootState) => state.CarritoReducer.carrito);

  const btn_carrito = (producto: Producto_INT, event: any) => {
    let element = event.target;
    let fila = element.parentElement.parentElement;

    if (element.innerText !== 'Quitar del carrito') {
      element.innerText = 'Quitar del carrito';
      element.style.backgroundColor = 'red';
      fila.classList.remove('alert-success');
      fila.classList.remove('alert-danger');
      fila.classList.remove('alert-warning');
      fila.classList.add('fila_seleccionada');
      notificacion = {
        type: 'EXITO',
        content: `SE AGREGO "${producto.product_name?.toUpperCase()}" AL CARRITO`,
        date: new Date(),
      };

      const carrito: any = {
        producto: {},
        formato: 'paquete',
        precio_unidad: 0,
        cantidad: 1,
        iva: false,
        total: 0,
      };

      carrito.producto = producto;
      carrito.precio_unidad = Number(dosDecimales(producto.pvp / producto.cantidad));
      carrito.total = dosDecimales(producto.pvp * carrito.cantidad);

      dispatch(SetCarrito([...CarritoReducer, ...[carrito]]));
    } else {
      element.innerText = 'Agregar del carrito';
      element.style.backgroundColor = '#15912f';
      fila.classList.remove('fila_seleccionada');
      notificacion = {
        type: 'EXITO',
        content: `SE QUITO DEL "${producto.product_name?.toUpperCase()}" CARRITO`,
        date: new Date(),
      };

      const index: number = CarritoReducer.findIndex(
        (item: Carrito_INT) => item.producto.id_producto === producto.id_producto,
      );
      CarritoReducer.splice(index, 1);
      dispatch(SetCarrito([...CarritoReducer]));
    }
    dispatch(SetNotificacion(notificacion));
  };

  useEffect(() => {
    if (ProductosReducer.searchProductos.length > 0) {
      setThisProduct(ProductosReducer.searchProductos);
    } else {
      setThisProduct(ProductosReducer.productos);
    }
  }, [ProductosReducer]);

  const validar_status = (estado: string | undefined) => {
    switch (estado) {
      case 'Disponible':
        return 'body-table alert-success';
      case 'Aun disponible':
        return 'body-table alert-warning';
      default:
        return 'body-table alert-danger';
    }
  };

  return (
    <>
      <Row justify='center' className='header-table'>
        <Col span={4}>Nombre</Col>
        <Col span={4}>Laboratorio</Col>
        <Col span={2}>Cant-Disp</Col>
        <Col span={2}>Medidas</Col>
        <Col span={3}>Presentacion</Col>
        <Col span={2}>Lote</Col>
        <Col span={1}>PVF</Col>
        <Col span={1}>PVP</Col>
        <Col span={5}>Opciones</Col>
      </Row>
      {ProductosReducer.loading && <TableSkeleton />}
      {ProductosReducer.productos.length === 0 && (
        <Alert message='No existen datos de productos para mostrar' type='info' />
      )}
      {thisProduct.map((producto: Producto_INT) => (
        <Row
          justify='center'
          className={validar_status(producto.estado)}
          key={producto.id_producto}>
          <>
            <Col span={4}>{producto.product_name}</Col>
            <Col span={4}>{producto.nombre_laboratorio}</Col>
            <Col span={2}>
              {producto.cantidad} / {producto.cantidad_disponible}
            </Col>
            <Col span={2}>
              {producto.medida} {producto.tipo_medida}
            </Col>
            <Col span={3}>{producto.presentacion}</Col>
            <Col span={2}>{producto.lote}</Col>
            <Col span={1}>{producto.pvf}</Col>
            <Col span={1}>{producto.pvp}</Col>
            {page === 'productos' ? (
              <Col span={5}>
                <DetallesProductos producto={producto} />
                &nbsp; &nbsp;
                <button
                  className='btn-verde'
                  disabled={producto.cantidad_disponible <= 0}
                  onClick={e => btn_carrito(producto, e)}>
                  Añadir al carrito
                </button>
              </Col>
            ) : (
              <Col span={5}>
                <DetallesProductos producto={producto} icono={true} />
                &nbsp; &nbsp;
                <BtnEdit icono={true} formulario='productos' />
                &nbsp; &nbsp;
                <ConfirmDelete icono={true} id={producto.id_producto} tabla='productos' />
              </Col>
            )}
          </>
        </Row>
      ))}
    </>
  );
}
