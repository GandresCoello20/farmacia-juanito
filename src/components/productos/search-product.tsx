/* eslint-disable array-callback-return */
import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { Producto_INT } from '../../interface';
import { RootState, Dispatch } from '../../redux';
import { SetSearchProduct } from '../../redux/modulos/productos';
import { Input, Select, Form, Button } from 'antd';
import { SearchOutlined } from '@ant-design/icons';

export function SearchProduct() {
  const dispatch: Dispatch = useDispatch();
  const { Option } = Select;
  const [form] = Form.useForm();
  const ProductosReducer = useSelector((state: RootState) => state.ProductosReducer);
  const [productPropiedades, setProductoPropiedades] = useState<string>('Nombre');
  const [writeSearch, setWriteSearch] = useState<string>('');

  const buscar_por_propiedades = (search: string, propiedad: string) => {
    return ProductosReducer.productos.filter((producto: Producto_INT) => {
      switch (propiedad) {
        case 'Nombre':
          return producto.product_name?.indexOf(search) !== -1;
        case 'Laboratorio':
          return producto.nombre_laboratorio?.indexOf(search) !== -1;
        case 'Principio-Active':
          return producto.principio_activo?.indexOf(search) !== -1;
        case 'Lote':
          return producto.lote.indexOf(search) !== -1;
        case 'Registro-Sanitario':
          return producto.registro_sanitario.indexOf(search) !== -1;
        case 'Cantidad':
          return producto.cantidad.toString().indexOf(search) !== -1;
        case 'Disponible-Cantidad':
          return producto.cantidad_disponible.toString().indexOf(search) !== -1;
        case 'ELaboracion':
          return producto.fecha_elaboracion.indexOf(search) !== -1;
        case 'Caducidad':
          return producto.fecha_caducidad.indexOf(search) !== -1;
        case 'Tipo-medidas':
          return producto.tipo_medida?.indexOf(search) !== -1;
        case 'Medidas':
          return producto.medida?.toString().indexOf(search) !== -1;
        case 'PVP':
          return producto.pvp.toString().indexOf(search) !== -1;
        case 'PVF':
          return producto.pvf.toString().indexOf(search) !== -1;
      }
    });
  };

  const InputSearchProducts = (event: React.ChangeEvent<HTMLInputElement>) =>
    setWriteSearch(event.target.value);

  const send = () => {
    let result: Array<Producto_INT>;
    result = buscar_por_propiedades(writeSearch, productPropiedades);
    dispatch(SetSearchProduct([...result]));
  };

  return (
    <>
      <Form form={form} onFinish={send} name='dynamic_rule'>
        <Input.Group compact>
          <Select defaultValue='Nombre' onChange={(value: string) => setProductoPropiedades(value)}>
            <Option value='Nombre'>Nombre</Option>
            <Option value='Laboratorio'>Laboratorio</Option>
            <Option value='Principio-Active'>Principio-Active</Option>
            <Option value='Lote'>Lote</Option>
            <Option value='Registro-Sanitario'>Registro-Sanitario</Option>
            <Option value='Cantidad'>Cantidad</Option>
            <Option value='Disponible-Cantidad'>Disponible-cantidad</Option>
            <Option value='ELaboracion'>Elaboracion</Option>
            <Option value='Caducidad'>Caducidad</Option>
            <Option value='Tipo-medidas'>Tipo-medidas</Option>
            <Option value='Medidas'>Medidas</Option>
            <Option value='PVP'>PVP</Option>
            <Option value='PVF'>PVF</Option>
          </Select>
          <Input
            type='search'
            style={{ width: '50%' }}
            onChange={InputSearchProducts}
            placeholder='Buscar Producto...'
          />
          <Button htmlType='submit'>
            <SearchOutlined />
          </Button>
        </Input.Group>
      </Form>
      <br />
    </>
  );
}
