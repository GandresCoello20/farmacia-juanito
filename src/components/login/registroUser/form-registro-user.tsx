import React, { useState } from 'react';
import { Row, Col, Form, Input, Button, Alert, message } from 'antd';
import { Link, useHistory } from 'react-router-dom';
import { ResponseAxios, CreateCount } from '../../../interface';
import { CodeAccess, CreateCountUser } from '../../../api/fetch/usuarios';
import { LeftCircleOutlined } from '@ant-design/icons';

export function FormRegistroUser() {
  const [form] = Form.useForm();

  const [isLoading, setIsLoading] = useState<boolean>(false);
  const history = useHistory<typeof useHistory>();

  const send = async (data: any) => {
    const { nombres, apellidos, email, password, confir_password, clave_autorizacion } = data;
    setIsLoading(true);

    if (password === confir_password) {
      if (String(password).length >= 7) {
        const userRegister: CreateCount = { nombres, apellidos, email, password, tipo: 'none' };

        const resAccess: ResponseAxios = await CodeAccess(clave_autorizacion);
        if (resAccess.respuesta.type === 'ERROR') {
          message.error(resAccess.respuesta.feeback);
        } else {
          userRegister.tipo = resAccess.axios.data.info[0].tipo;
          const resCount: ResponseAxios = await CreateCountUser(userRegister);

          if (resCount.respuesta.type === 'ERROR') {
            message.error(resCount.respuesta.feeback);
          } else {
            message.success('LA CUENTA FUE CREADA, ESPERA QUE UN ADMINISTRADOR TE ADMITA');
            form.resetFields();
            history.push('/login/iniciar-session');
          }
        }
      } else {
        message.error('SE REQUIRE 7 O MAS CARACTERES EN EL PASSWORD');
      }
    } else {
      message.error('LAS CONTRASEÑAS NO COINCIDEN, RESIVE Y VUELVA HA INTENTARLO');
    }

    setIsLoading(false);
  };

  return (
    <>
      <div className='content-login-derecha'>
        <Link to='/login/iniciar-Sesion' className='arrow-back'>
          <LeftCircleOutlined style={{ fontSize: 30 }} />
        </Link>

        <h3>Crear cuenta</h3>
        <Form form={form} onFinish={send} name='dynamic_rule'>
          <Row justify='space-between'>
            <Col span={11}>
              <Form.Item
                name='nombres'
                rules={[
                  {
                    required: true,
                    message: 'Falta escribir tu nombre',
                  },
                ]}>
                <Input placeholder='Ingrese sus nombres' />
              </Form.Item>
            </Col>
            <Col span={11}>
              <Form.Item
                name='apellidos'
                rules={[
                  {
                    required: true,
                    message: 'Falta escribir tu apellido',
                  },
                ]}>
                <Input placeholder='Ingrese sus apellidos' />
              </Form.Item>
            </Col>
          </Row>
          <Form.Item
            name='email'
            label='Email:'
            rules={[
              {
                required: true,
                message: 'Falta escribir tu emaill',
              },
            ]}>
            <Input type='email' placeholder='Correo o nombre de usuario' />
          </Form.Item>
          <Form.Item
            name='password'
            label='Contraseña'
            rules={[
              {
                required: true,
                message: 'Falta su contraseña',
              },
            ]}>
            <Input type='password' placeholder='Contraseña 7 caracteres o mas' />
          </Form.Item>
          <Form.Item
            name='confir_password'
            label='Confirmar Contraseña'
            rules={[
              {
                required: true,
                message: 'Vuelve a escribir tu contraseña',
              },
            ]}>
            <Input type='password' placeholder='Cofirmar contraseña' />
          </Form.Item>

          <Form.Item
            name='clave_autorizacion'
            label='Clave de Autorizacion'
            rules={[
              {
                required: true,
                message:
                  'Necesitas la clave de autorizacion, si no tienes una pidele a una ( administrador )',
              },
            ]}>
            <Input type='password' placeholder='Cofirmar contraseña' />
          </Form.Item>

          <Form.Item>
            <Button htmlType='submit' loading={isLoading} type='primary'>
              Registrarme
            </Button>
          </Form.Item>
        </Form>
        <Alert message='¿Ya tienes una cuenta? puedes acceder aqui...!' type='info' showIcon />
        <br />
        <Link to='/login/iniciar-Session'>Iniciar Session</Link>
      </div>
    </>
  );
}
