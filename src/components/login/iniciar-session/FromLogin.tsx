import React, { useState } from 'react';
import Cookie from 'js-cookie';
import { useDispatch } from 'react-redux';
import { Form, Input, Button, Checkbox, Alert, message } from 'antd';
import { MailExists, AccessLogin, TokenLife } from '../../../api/fetch/login';
import { ResponseAxios } from '../../../interface';
import { useHistory } from 'react-router-dom';
import { Dispatch } from '../../../redux';
import { getMyUser } from '../../../redux/modulos/usuario';
import { Link } from 'react-router-dom';

export function FormLogin() {
  const dispatch: Dispatch = useDispatch();
  const [form] = Form.useForm();
  const [checkNick, setCheckNick] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const history = useHistory<typeof useHistory>();

  const send = async (data: any) => {
    setIsLoading(true);
    const { email, password } = data;

    const exists: ResponseAxios = await MailExists(email);

    if (exists.axios.data.length === 0) {
      message.error(`NO EXISTE CUENTA RELACIONADA EN EL EMAIL: "${email}" `);
    } else if (exists.axios.data[0].email_on === 0) {
      message.error(
        `EL EMAIL ${email} NO ESTA VERIFICADO, ESPERA HASTA QUE UN ADMINISTRADOR LO ADMITA`,
      );
    } else {
      const login: ResponseAxios = await AccessLogin(email, password);
      if (login.respuesta.type === 'ERROR') {
        message.error(login.respuesta.feeback);
      } else {
        const resMyUser: ResponseAxios = await TokenLife(login.axios.data.token);
        if (resMyUser.respuesta.type === 'ERROR') {
          message.error(resMyUser.respuesta.feeback);
        } else {
          if (checkNick) {
            Cookie.set('f-j-email-f-j', email, { expires: 7 });
            Cookie.set('f-j-password-f-j', password, { expires: 7 });
          }
          Cookie.set('access-token', login.axios.data.token);
          dispatch(getMyUser(resMyUser.axios.data.myUser));
          history.push('/stock');
        }
      }
    }

    setIsLoading(false);
  };

  return (
    <>
      <div className='content-login-derecha'>
        <h3>Iniciar Session</h3>
        <Form form={form} onFinish={send} name='dynamic_rule'>
          <Form.Item
            name='email'
            rules={[
              {
                required: true,
                message: 'Falta escribir su correo o nombre de usuario',
              },
            ]}>
            <Input
              type='email'
              defaultValue={Cookie.get('f-j-email-f-j')}
              placeholder='Correo o nombre de usuario'
            />
          </Form.Item>
          <Form.Item
            name='password'
            rules={[
              {
                required: true,
                message: 'Falta su contraseña',
              },
            ]}>
            <Input
              type='password'
              defaultValue={Cookie.get('f-j-password-f-j')}
              placeholder='Contraseña'
            />
          </Form.Item>
          <Form.Item>
            <Checkbox checked={checkNick} onChange={e => setCheckNick(e.target.checked)}>
              Recordar mis credenciales.
              {checkNick && (
                <p style={{ color: 'red' }}>
                  Farmacia juanito le recordara sus credenciales haciendo uso de ({' '}
                  <strong>Cookies por 7 dias</strong> ), si existe más de una cuenta que utiliza
                  este mismo equipo, <strong>RECORDAR PUEDE SER PELIGROSO</strong>.
                </p>
              )}
            </Checkbox>
          </Form.Item>
          <Form.Item>
            <Button htmlType='submit' loading={isLoading} type='primary'>
              Entrar
            </Button>
          </Form.Item>
        </Form>
        <Alert message='¿No tienes una cuenta? puedes crear una aqui..!' type='info' showIcon />
        <br />
        <Link to='/login/registro-user'>Registrarme</Link>
      </div>
    </>
  );
}
