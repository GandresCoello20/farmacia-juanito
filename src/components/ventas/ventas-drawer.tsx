import React, { useState } from 'react';
import { Button, Drawer, Row, Col, Divider, Alert } from 'antd';

interface Props {
  carrito: any[];
}

export function VentasDrawerDetalles({ carrito }: Props) {
  const [visible, setVisible] = useState<boolean>(false);

  return (
    <>
      <Button type='primary' onClick={() => setVisible(true)}>
        Detalles
      </Button>
      <Drawer
        title='Detalles de la factura'
        placement='right'
        closable={false}
        onClose={() => setVisible(false)}
        visible={visible}>
        {carrito.map(carrito => (
          <>
            <Row justify='center' className='header-table' key={carrito.id_factura}>
              <Col span={22}>Nombre</Col>
              <Col className='body-table'>{carrito.product_name}</Col>
              <Col span={22}>Laboratorio</Col>
              <Col className='body-table'>{carrito.nombre_laboratorio}</Col>
              <Col span={22}>Presentacion</Col>
              <Col className='body-table'>{carrito.presentacion}</Col>
              <Col span={22}>Medidas</Col>
              <Col className='body-table'>
                {carrito.medida} {carrito.tipo_medida}
              </Col>
              <Col span={22}>Lote</Col>
              <Col className='body-table'>{carrito.lote}</Col>
              <Col span={22}>PVP</Col>
              <Col className='body-table'>$ {carrito.pvp}</Col>
              <Col span={22}>Estado</Col>
              <Col className='body-table'>{carrito.estado}</Col>
              <Col span={22}>Formato</Col>
              <Col className='body-table'>{carrito.formato}</Col>
              <Col span={22}>Cantidad</Col>
              <Col className='body-table'>{carrito.cantidad}</Col>
              <Col span={22}>Total</Col>
              <Col className='body-table'>{carrito.item_total}</Col>
            </Row>
            <Divider />
          </>
        ))}
        {carrito.length === 0 && <Alert type='warning' message='No hay ventas para mostrar..' />}
      </Drawer>
    </>
  );
}
