import React, { useState } from 'react';
import { Form, Input, Select, Button } from 'antd';
import { useSelector, useDispatch } from 'react-redux';
import { SearchOutlined } from '@ant-design/icons';
import { RootState, Dispatch } from '../../redux';
import { Factura_INT } from '../../interface';
import { SearchVentas } from '../../redux/modulos/ventas';

export function SearchVentasPropiedades() {
  const dispatch: Dispatch = useDispatch();
  const [form] = Form.useForm();
  const { Option } = Select;

  const ventasReducer = useSelector((state: RootState) => state.VentasReduce);

  const [ventasPropiedades, setVentasPropiedades] = useState<string>('Cliente');
  const [WriteSearch, setWriteSearch] = useState<string>('');

  const buscar_por_propiedades = (search: string, propiedad: string) => {
    // eslint-disable-next-line array-callback-return
    return ventasReducer.facturas.filter((factura: Factura_INT) => {
      switch (propiedad) {
        case 'Cliente':
          return (
            factura.nombres?.indexOf(search) !== -1 || factura.apellidos?.indexOf(search) !== -1
          );
        case 'Identificacion':
          return factura.identificacion.toString().indexOf(search) !== -1;
        case 'Correo':
          return factura.correo?.indexOf(search) !== -1;
      }
    });
  };

  const InputSearchVentas = (event: React.ChangeEvent<HTMLInputElement>) =>
    setWriteSearch(event.target.value);

  const send = () => {
    let result: Array<Factura_INT>;
    result = buscar_por_propiedades(WriteSearch, ventasPropiedades);
    dispatch(SearchVentas([...result]));
  };

  return (
    <>
      <Form form={form} onFinish={send} name='dynamic_rule'>
        <Input.Group compact>
          <Select defaultValue='Cliente' onChange={(value: string) => setVentasPropiedades(value)}>
            <Option value='Cliente'>Cliente</Option>
            <Option value='Identificacion'>Identificacion</Option>
            <Option value='Correo'>Correo</Option>
          </Select>
          <Input
            type='search'
            style={{ width: '50%' }}
            onChange={InputSearchVentas}
            placeholder='Buscar Ventas...'
          />
          <Button htmlType='submit'>
            <SearchOutlined />
          </Button>
        </Input.Group>
      </Form>
    </>
  );
}
