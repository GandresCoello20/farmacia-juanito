import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux';
import { ConfirmDelete } from '../../components/confirm/confirmDelete';
import { TableSkeleton } from '../skeleton/tabla-skeleton';
import { Row, Col, Tag, Alert } from 'antd';
import { Factura_INT } from '../../interface';
import moment from 'moment';
import { VentasDrawerDetalles } from './ventas-drawer';

interface Props {
  dateVenta: string;
  loadingDate?: boolean;
  limit?: number;
}

export function TableVentas({ dateVenta, loadingDate, limit }: Props) {
  const [thisFactura, setThisFactura] = useState<Factura_INT[]>([]);
  const VentasReducer = useSelector((state: RootState) => state.VentasReduce);

  useEffect(() => {
    if (VentasReducer.search_facturas.length > 0) {
      setThisFactura(VentasReducer.search_facturas);
    } else {
      setThisFactura(VentasReducer.facturas);
    }

    if (limit) {
      thisFactura.slice(0, limit);
    }
  }, [VentasReducer, limit, thisFactura]);

  return (
    <>
      <Row justify='center' className='header-table'>
        <Col span={3}>Cliente</Col>
        <Col span={3}>Cedula</Col>
        <Col span={3}>Fecha</Col>
        <Col span={2}>Total</Col>
        <Col span={2}>Desc</Col>
        <Col span={2}>Efect</Col>
        <Col span={2}>Camb</Col>
        <Col span={4}>Opciones</Col>
      </Row>
      {(VentasReducer.loading || loadingDate) && <TableSkeleton />}
      {thisFactura.map((factura: Factura_INT) => (
        <Row justify='center' className='body-table' key={factura.id_factura}>
          <Col span={3}>
            {factura.nombres === 'consumidor_final' ? (
              <Tag color='magenta'>{factura.nombres}</Tag>
            ) : (
              `${factura.nombres} ${factura.apellidos}`
            )}
          </Col>
          <Col span={3}>
            {factura.identificacion === 0 ? (
              <Tag color='magenta'>No especificado</Tag>
            ) : (
              factura.identificacion
            )}
          </Col>
          <Col span={3}>{factura.fecha_factura}</Col>
          <Col span={2}>
            <Tag color='red'>$ {factura.total}</Tag>
          </Col>
          <Col span={2}>
            <Tag color='blue'>{factura.descuento} %</Tag>
          </Col>
          <Col span={2}>
            <Tag color='green'>$ {factura.efectivo}</Tag>
          </Col>
          <Col span={2}>
            <Tag color='volcano'>$ {factura.cambio}</Tag>
          </Col>
          <Col span={4}>
            <VentasDrawerDetalles carrito={factura.carrito} />
            &nbsp; &nbsp;
            <ConfirmDelete id={factura.id_factura} tabla='factura' />
          </Col>
        </Row>
      ))}
      {thisFactura.length === 0 && (
        <Alert
          type='info'
          message={`No existen datos de ${moment(dateVenta).format('LL')} para mostrar`}
        />
      )}
    </>
  );
}
