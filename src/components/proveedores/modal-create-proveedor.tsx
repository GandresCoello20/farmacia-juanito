import React, { useState } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { RootState, Dispatch } from '../../redux';
import { SetNotificacion } from '../../redux/modulos/notificaciones';
import { SetProveedores } from '../../redux/modulos/proveedores';
import { CreateProveedor } from '../../api/fetch/proveedores';
import { Laboratorio, ResponseAxios, Notificacion, Proveedor_INT } from '../../interface';
import { Button, Modal, Form, Input, Select, message } from 'antd';

export function ModalCreateProveedor() {
  let notificaciones: Notificacion;
  const dispatch: Dispatch = useDispatch();
  const [form] = Form.useForm();
  const { Option } = Select;
  const [visible, setVisible] = useState<boolean>(false);

  const LaboratorioReducer: Array<Laboratorio> = useSelector(
    (state: RootState) => state.PropiedadesReducer.laboratorio,
  );
  const Proveedores: Array<Proveedor_INT> = useSelector(
    (state: RootState) => state.ProveedorReducer.proveedores,
  );

  const send = async (data: any) => {
    const resProveedor: ResponseAxios = await CreateProveedor(data);

    if (resProveedor.respuesta.type === 'ERROR') {
      message.error(resProveedor.respuesta.feeback);
      notificaciones = {
        type: resProveedor.respuesta.type,
        content: resProveedor.respuesta.feeback,
        date: new Date(),
      };
    } else {
      notificaciones = {
        type: resProveedor.respuesta.type,
        content: 'SE CREO EL NUEVO PROVEEDOR',
        date: new Date(),
      };
      dispatch(SetProveedores([...Proveedores, ...resProveedor.axios.data]));
      message.success('SE CREO EL NUEVO PROVEEDOR');
    }

    dispatch(SetNotificacion(notificaciones));
  };

  return (
    <>
      <Button type='ghost' onClick={() => setVisible(true)}>
        Agregar nuevo Proveedor
      </Button>
      <Modal
        title='Agregar nuevo cliente'
        visible={visible}
        onOk={() => setVisible(false)}
        onCancel={() => setVisible(false)}>
        <Form form={form} onFinish={send}>
          <Form.Item
            name='nombres'
            label='Nombres'
            rules={[
              {
                required: true,
                message: 'Necesitas ingresar el nombre del proveedor',
              },
            ]}>
            <Input placeholder='Ingrese los nombres de proveedor' />
          </Form.Item>
          <Form.Item
            name='id_laboratorio'
            label='Laboratorio'
            rules={[
              {
                required: true,
                message: 'Selecciona el laboratorio',
              },
            ]}>
            <Select defaultValue='Laboratorio'>
              {LaboratorioReducer.map(laboratorio => (
                <Option
                  value={laboratorio.id_name_laboratorio}
                  key={laboratorio.id_name_laboratorio}>
                  {laboratorio.nombre_laboratorio}
                </Option>
              ))}
            </Select>
          </Form.Item>
          <Form.Item
            name='correo'
            label='Email'
            rules={[
              {
                required: true,
                message: 'Necesitas ingresar un email',
              },
            ]}>
            <Input type='email' placeholder='Correo electronico' />
          </Form.Item>
          <Form.Item
            name='telefono'
            label='Telefono'
            rules={[
              {
                required: true,
                message: 'Necesitas un numero de telefono',
              },
            ]}>
            <Input type='number' placeholder='Ingrese numero de celular' />
          </Form.Item>

          <Button htmlType='submit' block type='primary'>
            Guardar
          </Button>
        </Form>
      </Modal>
    </>
  );
}
