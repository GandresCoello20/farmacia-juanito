import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Dispatch } from '../../redux';
import { CreateNewProductProveedor } from '../../api/fetch/proveedores';
import { SetNotificacion } from '../../redux/modulos/notificaciones';
import { Producto_proveedor_INT, ResponseAxios, Notificacion } from '../../interface';
import { Modal, Button, Input, Form, DatePicker, message } from 'antd';

interface Props {
  id_proveedor: string;
}

export function ModalNewProduct({ id_proveedor }: Props) {
  let notificaciones: Notificacion;
  const dispatch: Dispatch = useDispatch();
  const [form] = Form.useForm();
  const { TextArea } = Input;

  const [visible, setVisible] = useState<boolean>(false);
  const [IsLogin, setIsLogin] = useState<boolean>(false);
  const [fechaPgo, setFechaPago] = useState<string>('');

  const send = async (data: any) => {
    setIsLogin(true);
    const { descripcion, total, abono } = data;
    let estado_pp: string;

    if (Number(abono) > Number(total)) {
      message.error('El abono no puede ser mayor al total, revise y vuelva a intetarlo');
      setIsLogin(false);
      return false;
    }

    if (Number(abono) === Number(total)) {
      estado_pp = 'Pagado';
    } else if (Number(abono) === 0) {
      estado_pp = 'Ingresado';
    } else if (Number(abono) < Number(total)) {
      estado_pp = 'Salfo pendiente';
    } else {
      estado_pp = 'No especificado';
    }

    const newProducto: Producto_proveedor_INT = {
      id_product_proveedor: '',
      id_proveedor,
      descripcion,
      total,
      abonado: abono,
      estado_pp,
      fecha_pago: fechaPgo,
    };

    const resNewProducto: ResponseAxios = await CreateNewProductProveedor(newProducto);

    if (resNewProducto.respuesta.type === 'ERROR') {
      message.error('ERROR EN CREAR EL PRODUCTO DEL PROVEEDOR');
      notificaciones = {
        type: 'ERROR',
        content: 'ERROR EN CREAR EL PRODUCTO DEL PROVEEDOR',
        date: new Date(),
      };
    } else {
      message.success('SE CREO EL NUEVO PRODUCTO DEL PROVEEDOR');
      notificaciones = {
        type: 'EXITO',
        content: 'SE CREO EL NUEVO PRODUCTO DEL PROVEEDOR',
        date: new Date(),
      };

      form.resetFields();
      setVisible(false);
      setFechaPago('');
    }

    setIsLogin(false);
    dispatch(SetNotificacion(notificaciones));
  };

  return (
    <>
      <Button type='dashed' onClick={() => setVisible(true)}>
        Nuevo producto
      </Button>
      <Modal
        title='Agregar producto del poveedor'
        visible={visible}
        onOk={() => setVisible(false)}
        onCancel={() => setVisible(false)}>
        <Form form={form} onFinish={send}>
          <Form.Item
            name='descripcion'
            label='Descripcion'
            rules={[
              {
                required: true,
                message: 'Necesitas especificar el producto recibido',
              },
            ]}>
            <TextArea rows={3} placeholder='Especificacion del producto recibido' />
          </Form.Item>
          <Form.Item
            label='Fecha pago'
            rules={[
              {
                required: true,
                message: 'Selecciona la fecha de pago',
              },
            ]}>
            <DatePicker
              placeholder='Fecha de pago'
              onChange={(date: any, dateString: string) => setFechaPago(dateString)}
            />
          </Form.Item>
          <Form.Item
            name='total'
            label='Total de pago'
            rules={[
              {
                required: true,
                message: 'Necesitas ingresar el total de pago',
              },
            ]}>
            <Input type='number' defaultValue={0} min={0} placeholder='Total de pago' />
          </Form.Item>
          <Form.Item
            name='abono'
            label='Abono'
            rules={[
              {
                required: true,
                message: 'Necesitas un valor de abono',
              },
            ]}>
            <Input type='number' defaultValue={0} min={0} placeholder='Valor de abono' />
          </Form.Item>

          <Button htmlType='submit' block type='primary' loading={IsLogin}>
            Guardar
          </Button>
        </Form>
      </Modal>
    </>
  );
}
