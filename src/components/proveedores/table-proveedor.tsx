import React, { useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';
import { RootState } from '../../redux';
import { Proveedor_INT } from '../../interface';
import { ConfirmDelete } from '../confirm/confirmDelete';
import { BtnEdit } from '../edit/btn-edit-drawer';
import { ModalNewProduct } from './modal-new-product';
import { TableSkeleton } from '../skeleton/tabla-skeleton';
import { Row, Col, Alert, Button } from 'antd';

export function TableProveedor() {
  const ProveedorReducer = useSelector((state: RootState) => state.ProveedorReducer);
  const [thisProveedor, setThisProveedor] = useState<Proveedor_INT[]>([]);

  useEffect(() => {
    if (ProveedorReducer.searchProveedor.length > 0) {
      setThisProveedor(ProveedorReducer.searchProveedor);
    } else {
      setThisProveedor(ProveedorReducer.proveedores);
    }
  }, [ProveedorReducer]);

  return (
    <>
      <Row justify='center' className='header-table'>
        <Col span={4}>Proveedor</Col>
        <Col span={4}>Laboratorio</Col>
        <Col span={3}>Correo</Col>
        <Col span={3}>Telefono</Col>
        <Col span={7}>Opciones</Col>
      </Row>
      {ProveedorReducer.loading && <TableSkeleton />}
      {thisProveedor.map((proveedor: Proveedor_INT) => (
        <Row justify='center' className='body-table' key={proveedor.id_proveedores}>
          <Col span={4}>{proveedor.nombres}</Col>
          <Col span={4}>{proveedor.nombre_laboratorio}</Col>
          <Col span={3}>{proveedor.correo}</Col>
          <Col span={3}>{proveedor.telefono}</Col>
          <Col span={7}>
            <ModalNewProduct id_proveedor={proveedor.id_proveedores} />
            &nbsp; &nbsp;
            <Link
              to={`/proveedores/detalles/${proveedor.id_proveedores}/${(
                proveedor.nombres +
                ' - ' +
                proveedor.correo
              ).toLocaleUpperCase()}`}>
              <Button type='dashed'>Productos</Button>
            </Link>
            &nbsp; &nbsp;
            <BtnEdit formulario='proveedores' icono={true} />
            &nbsp; &nbsp;
            <ConfirmDelete icono={true} id={proveedor.id_proveedores} tabla='proveedor' />
          </Col>
        </Row>
      ))}
      {thisProveedor.length === 0 && (
        <Alert type='info' message='NO hay datos de proveedores que mostrar...' />
      )}
    </>
  );
}
