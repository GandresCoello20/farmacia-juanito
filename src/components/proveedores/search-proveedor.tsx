import React, { useState } from 'react';
import { Dispatch, RootState } from '../../redux';
import { useDispatch, useSelector } from 'react-redux';
import { Form, Input, Select, Button } from 'antd';
import { Proveedor_INT } from '../../interface';
import { SearchOutlined } from '@ant-design/icons';
import { SetsearchProveedor } from '../../redux/modulos/proveedores';

export function SearchProveedor() {
  const dispatch: Dispatch = useDispatch();
  const [form] = Form.useForm();
  const { Option } = Select;

  const ProveedorReducer = useSelector((state: RootState) => state.ProveedorReducer);

  const [propiedadesProveedor, setPropiedadesProveedor] = useState<string>('Proveedor');
  const [writeSearch, setWriteSearch] = useState<string>('');

  const buscar_por_propiedades = (search: string, propiedad: string) => {
    // eslint-disable-next-line array-callback-return
    return ProveedorReducer.proveedores.filter((proveedor: Proveedor_INT) => {
      switch (propiedad) {
        case 'Proveedor':
          return proveedor.nombres.indexOf(search) !== -1;
        case 'Laboratorio':
          return proveedor.nombre_laboratorio.indexOf(search) !== -1;
        case 'Correo':
          return proveedor.correo.indexOf(search) !== -1;
      }
    });
  };

  const send = () => {
    let result: Array<Proveedor_INT>;
    result = buscar_por_propiedades(writeSearch, propiedadesProveedor);
    dispatch(SetsearchProveedor([...result]));
  };

  const InputSearchProveedor = (event: React.ChangeEvent<HTMLInputElement>) =>
    setWriteSearch(event.target.value);

  return (
    <>
      <Form form={form} onFinish={send} name='dynamic_rule'>
        <Input.Group compact>
          <Select
            defaultValue='Proveedor'
            onChange={(value: string) => setPropiedadesProveedor(value)}>
            <Option value='Proveedor'>Proveedor</Option>
            <Option value='Laboratorio'>Laboratorio</Option>
            <Option value='Correo'>Correo</Option>
          </Select>
          <Input
            type='search'
            style={{ width: '50%' }}
            onChange={InputSearchProveedor}
            placeholder='Buscar Proveedor...'
          />
          <Button htmlType='submit'>
            <SearchOutlined />
          </Button>
        </Input.Group>
      </Form>
      <br />
    </>
  );
}
