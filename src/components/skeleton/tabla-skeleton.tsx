import React from 'react';
import { Skeleton } from 'antd';

export function TableSkeleton(){
    return(
        <>
            {[0,1,2,3,4].map(item => (
                <Skeleton key={item} paragraph={{ rows: 4 }} />
            ))}
        </>
    );
}