import React, { useState, useEffect } from 'react';
import { Producto_INT, Carrito_INT } from '../../interface';
import { useSelector, useDispatch } from 'react-redux';
import { RootState, Dispatch } from '../../redux';
import { CarritoDrawer } from './carrito-drawer';
import { SetCarrito, ClearCarrito } from '../../redux/modulos/carrito';
import { DeleteOutlined } from '@ant-design/icons';
import { Row, Col, Select, Tag, Alert, Button, Input, Checkbox, message } from 'antd';
import { Link } from 'react-router-dom';
import { dosDecimales } from '../../hooks/decimales';

interface Props {
  updateSteps: any;
}

export function ContentCarrito({ updateSteps }: Props) {
  const dispatch: Dispatch = useDispatch();
  const [subTotal, setSubTotal] = useState<number>(0);
  const [iva] = useState<number>(12);
  const [total, setTotal] = useState<number>(0);
  const CarritoReducer = useSelector((state: RootState) => state.CarritoReducer);
  const existProduc = CarritoReducer.carrito.length === 0 ? true : false;

  const { Option } = Select;

  // eslint-disable-next-line react-hooks/exhaustive-deps
  const calcular_sub_iva_total = (
    isIva?: boolean,
    isDescuento?: boolean,
    isFormato?: boolean,
    isCantidad?: boolean,
  ) => {
    let SubTotal: number = 0;
    let Total: number = 0;

    // eslint-disable-next-line array-callback-return
    CarritoReducer.carrito.map((carrito: Carrito_INT) => {
      if (carrito.formato === 'paquete') {
        const precio_X_paquete = carrito.producto.pvp * carrito.cantidad;
        SubTotal = SubTotal + precio_X_paquete;
        carrito.total = Number(dosDecimales(precio_X_paquete));
      } else {
        const precio_X_unidad = carrito.precio_unidad * carrito.cantidad;
        SubTotal = SubTotal + precio_X_unidad;
        carrito.total = Number(dosDecimales(precio_X_unidad));
      }

      if (isIva || isDescuento || isFormato || isCantidad) {
        console.log('dispathc');
        dispatch(SetCarrito([...CarritoReducer.carrito]));
      } else {
        Total = subTotal;
      }
    });

    setSubTotal(dosDecimales(SubTotal));
    setTotal(dosDecimales(Total));
  };

  useEffect(() => {
    calcular_sub_iva_total();
  }, [calcular_sub_iva_total]);

  const formato = (select: any, id_producto: string) => {
    // eslint-disable-next-line array-callback-return
    CarritoReducer.carrito.map((carrito: Carrito_INT) => {
      if (carrito.producto.id_producto === id_producto) {
        carrito.formato = select.value;
        //carrito.cantidad = 0;
      }
    });
    dispatch(SetCarrito([...CarritoReducer.carrito]));
    calcular_sub_iva_total(false, false, true);
  };

  const add_iva = (checked: boolean, id_producto: string) => {
    let Total: number = 0;
    // eslint-disable-next-line array-callback-return
    CarritoReducer.carrito.map((carrito: Carrito_INT) => {
      if (carrito.producto.id_producto === id_producto) {
        carrito.iva = checked;
        const calIva = (subTotal * iva) / 100;

        if (checked) {
          console.log('se aplico el iva');
          carrito.total = Number(dosDecimales(carrito.total + calIva));
          Total = total + calIva;
        } else {
          console.log('se quito el iva');
          carrito.total = Number(dosDecimales(carrito.total - calIva));
          Total = total - calIva;
        }
        setTotal(Total);
      }
    });
    dispatch(SetCarrito([...CarritoReducer.carrito]));
  };

  const cantidad = (e: React.ChangeEvent<HTMLInputElement>, id_producto: string) => {
    let value: number = Number(e.target.value);
    let element: any = e.target;
    if (value.toString().indexOf('-') !== -1) {
      message.error('NO SE ACEPTAN NUMEROS NEGATIVOS');
      return false;
    }
    // eslint-disable-next-line array-callback-return
    CarritoReducer.carrito.map((carrito: Carrito_INT) => {
      let limite: number = 0;
      if (carrito.producto.id_producto === id_producto) {
        if (carrito.formato === 'paquete') {
          limite = carrito.producto.cantidad_disponible / carrito.producto.cantidad;
          if (value > limite) {
            message.error('NO HAY PAQUETES SUFICIENTES');
            carrito.cantidad = Number(limite.toFixed(0));
            element.style.backgroundColor = 'red';
          } else {
            element.style.backgroundColor = 'white';
            carrito.cantidad = value;
          }
        } else {
          if (value > carrito.producto.cantidad_disponible) {
            message.error('NO HAY UNIDADES SUFICIENTES');
            carrito.cantidad = carrito.producto.cantidad_disponible;
            element.style.backgroundColor = 'red';
          } else {
            element.style.backgroundColor = 'white';
            carrito.cantidad = value;
          }
        }
      }
    });

    dispatch(SetCarrito([...CarritoReducer.carrito]));
    calcular_sub_iva_total(false, false, false, true);
  };

  const quitar_del_carrito = (producto: Producto_INT) => {
    const CarritoStore = CarritoReducer.carrito;
    const index: number = CarritoStore.findIndex(
      (item: Producto_INT) => item.id_producto === producto.id_producto,
    );
    CarritoStore.splice(index, 1);
    dispatch(SetCarrito([...CarritoStore]));
  };

  const limpiar_carrito = () => dispatch(ClearCarrito());

  return (
    <>
      <Row justify='center' className='header-table'>
        <Col span={4}>Nombre</Col>
        <Col span={4}>Laboratorio</Col>
        <Col span={2}>Cant-Disp</Col>
        <Col span={2}>Medidas</Col>
        <Col span={2}>Presentacion</Col>
        <Col span={1}>PVP</Col>
        <Col span={3}>Formato</Col>
        <Col span={1}>$ Uni</Col>
        <Col span={2}>Cantidad</Col>
        <Col span={1}>¿ Iva ?</Col>
        <Col span={1}>Total</Col>
        <Col span={1}>Opcion</Col>
      </Row>
      {CarritoReducer.carrito.length === 0 ? (
        <Alert
          type='info'
          message={
            <>
              No hay productos en carrito, ve a la <Link to='/productos'>seccion de productos</Link>{' '}
              y selecciona algunos.
            </>
          }
        />
      ) : (
        CarritoReducer.carrito.map((carrito: Carrito_INT) => (
          <Row justify='center' className='body-table' key={carrito.producto.id_producto}>
            <Col span={4}>{carrito.producto.product_name}</Col>
            <Col span={4}>{carrito.producto.nombre_laboratorio}</Col>
            <Col span={2}>
              {carrito.producto.cantidad} / {carrito.producto.cantidad_disponible}
            </Col>
            <Col span={2}>
              {carrito.producto.medida} {carrito.producto.tipo_medida}
            </Col>
            <Col span={2}>{carrito.producto.presentacion}</Col>
            <Col span={1}>$ {carrito.producto.pvp}</Col>
            <Col span={3}>
              <Select
                labelInValue
                placeholder='paquete'
                style={{ width: 120 }}
                onChange={select => formato(select, carrito.producto.id_producto)}>
                <Option value='unidad'>Unidad</Option>
                <Option value='paquete'>Paquete</Option>
              </Select>
            </Col>
            <Col span={1}>
              <Tag color='purple'>$ {carrito.precio_unidad}</Tag>
            </Col>
            <Col span={2}>
              <Input
                type='number'
                placeholder='Cantidad'
                min={1}
                defaultValue={carrito.cantidad}
                style={{ width: 60 }}
                onChange={e => cantidad(e, carrito.producto.id_producto)}
              />
            </Col>
            <Col span={1}>
              <Checkbox
                onChange={e => add_iva(e.target.checked, carrito.producto.id_producto)}
                defaultChecked={carrito.iva}
              />
            </Col>
            <Col span={1}>
              <Tag color='green'>$ {carrito.total}</Tag>
            </Col>
            <Col span={1}>
              <Button danger onClick={() => quitar_del_carrito(carrito.producto)}>
                <DeleteOutlined />
              </Button>
            </Col>
          </Row>
        ))
      )}

      <br />
      <br />
      <br />

      <Row
        justify='space-around'
        style={{ border: 2, borderStyle: 'solid', borderColor: '#cdcdcd', padding: 10 }}>
        <Col span={3}>
          <Tag color='orange'>
            <span style={{ fontSize: 20 }}>Subtotal: $ {subTotal}</span>
          </Tag>
        </Col>
        <Col span={3}>
          <Tag color='default'>
            <span style={{ fontSize: 20 }}>Iva: {iva} %</span>
          </Tag>
        </Col>
        <Col span={3}>
          <Tag color='green'>
            <span style={{ fontSize: 20 }}>Total: $ {total}</span>
          </Tag>
        </Col>
        <Col span={3}>
          <CarritoDrawer
            updateSteps={updateSteps}
            existProduc={existProduc}
            total={total}
            iva={iva}
          />
        </Col>
        <Col span={3}>
          <Button danger disabled={existProduc} onClick={limpiar_carrito}>
            Limpiar el carrito
          </Button>
        </Col>
      </Row>
    </>
  );
}
