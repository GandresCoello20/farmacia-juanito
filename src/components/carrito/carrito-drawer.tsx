import React, { useState, useEffect } from 'react';
import moment from 'moment';
import { useSelector, useDispatch } from 'react-redux';
import { RootState, Dispatch } from '../../redux';
import { useHistory } from 'react-router-dom';
import { ClearCarrito } from '../../redux/modulos/carrito';
import { DollarCircleOutlined } from '@ant-design/icons';
import {
  Button,
  Drawer,
  Row,
  Col,
  Divider,
  Select,
  Form,
  Input,
  Tag,
  Statistic,
  message,
} from 'antd';
import { fecha_actual } from '../../hooks/fechas';
import { ModalCreateClient } from '../clientes/modalCreateClient';
import {
  Carrito_INT,
  Cliente_INT,
  Factura_INT,
  Notificacion,
  ResponseAxios,
} from '../../interface';
import { dosDecimales } from '../../hooks/decimales';
import { SetVentas } from '../../redux/modulos/ventas';
import { CreateVenta } from '../../api/fetch/ventas';
import { SetNotificacion } from '../../redux/modulos/notificaciones';

interface Props {
  updateSteps: any;
  existProduc: boolean;
  total: number;
  iva?: number;
}

export function CarritoDrawer({ updateSteps, existProduc, iva, total }: Props) {
  const dispatch: Dispatch = useDispatch();
  const history = useHistory<typeof useHistory>();
  const Clientes = useSelector((state: RootState) => state.ClienteReducer.clientes);
  const Carrito = useSelector((state: RootState) => state.CarritoReducer.carrito);
  const Facturas = useSelector((state: RootState) => state.VentasReduce.facturas);

  const [form] = Form.useForm();
  let notificacion: Notificacion;
  const { Option } = Select;
  const { TextArea } = Input;
  const [visible, setVisible] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [efectivo, setEfectivo] = useState<number>(0);
  const [vuelto, setVuelto] = useState<number>(0);
  const [descuento, setDescuento] = useState<number>(0);
  const [totalPago, setTotalPago] = useState<number>(0);

  useEffect(() => {
    setTotalPago(total);
  }, [total]);

  const send = async (data: any) => {
    const { select_client, descripcion_client, efectivo } = data;
    setIsLoading(true);

    // eslint-disable-next-line array-callback-return
    Carrito.map((carrito: Carrito_INT) => {
      if (carrito.cantidad === 0 || carrito.total === 0) {
        message.error('SE ENCONTRO 0 EN CANTIDAD O EN TOTAL, REVISE Y VUELVA HA INTENTARLO');
        return false;
      }
    });

    const compra: Factura_INT | any = {
      id_cliente: select_client === undefined ? '' : select_client.value,
      descripcion: descripcion_client === undefined ? '' : descripcion_client,
      descuento: descuento,
      total: totalPago,
      efectivo: efectivo === undefined ? 0 : efectivo,
      cambio: vuelto,
      carrito: Carrito,
    };

    const resFactura: ResponseAxios = await CreateVenta(compra);
    console.log(resFactura);

    if (resFactura.respuesta.type === 'ERROR') {
      message.error(resFactura.respuesta.feeback);
      notificacion = {
        type: resFactura.respuesta.type,
        content: resFactura.respuesta.feeback,
        date: new Date(),
      };
    } else {
      dispatch(SetVentas([...Facturas, ...resFactura.axios.data]));
      notificacion = {
        type: 'EXITO',
        content: 'SE CREO EL NOMBRE DE LABORATORIO',
        date: new Date(),
      };
      message.success('SE REGISTRO LA COMPRA...!!');
      form.resetFields();
    }

    dispatch(SetNotificacion(notificacion));
    dispatch(ClearCarrito());
    history.push('/productos');
    setIsLoading(false);
  };

  const calcular_vuelto = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = Number(event.target.value);
    setEfectivo(value);

    if (value === 0) setVuelto(value);

    if (value < totalPago) {
      message.error('El valor digitado no alcanza para cancelar el total de la compra');
      setVuelto(0);
    } else {
      setVuelto(Number(dosDecimales(value - totalPago)));
    }
  };

  const add_descuento = (e: React.ChangeEvent<HTMLInputElement>) => {
    const value = Number(e.target.value);

    if (value < 0) {
      message.error('NO SE ACEPTAN NUMEROS NEGATIVOS');
      return false;
    }

    setDescuento(value);
    const calDesc = (total * value) / 100;
    setTotalPago(Number(dosDecimales(total - calDesc)));

    if (vuelto > 0) {
      message.info('POSIBLEMENTE TENDRA QUE COLVER A DIGITAR EL VALOR DEL EFECTIVO');
    }
  };

  const emitir_factura = () => {
    const compra = {
      descuento: descuento,
      total: totalPago,
      efectivo: efectivo,
      cambio: vuelto,
      productos: Carrito,
    };

    window.open(
      `/emitir-factura?productos=${JSON.stringify(compra.productos)}&descuento=${
        compra.descuento
      }&total=${compra.total}&efectivo=${compra.efectivo}&cambio=${compra.cambio}`,
      'Factura',
      'width=530, height=540',
    );
  };

  return (
    <>
      <Button
        disabled={existProduc}
        type='primary'
        onClick={() => {
          setVisible(true);
          updateSteps(2);
        }}>
        Continuar
      </Button>
      <Drawer
        title='Detalles para la compra'
        placement='left'
        closable={false}
        onClose={() => {
          setVisible(false);
          setDescuento(0);
          updateSteps(1);
        }}
        visible={visible}>
        <>
          <Form form={form} onFinish={send}>
            <Row justify='space-around'>
              <Col span={24} style={{ height: 130, position: 'relative', top: -25 }}>
                <img
                  src='logo-farmacia-removebg-preview.png'
                  width={220}
                  height={170}
                  alt='logo-farmacia-juanito'
                />
              </Col>
              <strong>{moment(fecha_actual()).format('LL')}</strong>
              <Divider />
              <ModalCreateClient />
              <Divider />
              <Col span={24}>
                <Form.Item name='select_client'>
                  <Select placeholder='Seleccionar Cliente' labelInValue>
                    {Clientes.map((cliente: Cliente_INT) => (
                      <Option key={cliente.id_cliente} value={cliente.id_cliente}>
                        {cliente.nombres} {cliente.apellidos} {cliente.identificacion}
                      </Option>
                    ))}
                  </Select>
                </Form.Item>
              </Col>
              <Col span={24}>
                <Form.Item name='descripcion_client'>
                  <TextArea rows={3} bordered placeholder='Descripcion de la compra' />
                </Form.Item>
              </Col>
              <Col span={24}>
                <label>Desc:</label>
                &nbsp; &nbsp;
                <Input
                  type='number'
                  disabled={existProduc}
                  placeholder='Descuento %'
                  onChange={add_descuento}
                  min={1}
                  max={101}
                  style={{ width: 120 }}
                />
                <Divider />
              </Col>
              {descuento !== undefined && (
                <Col span={12}>
                  <span>
                    Desc: <Tag color='processing'>{descuento} %</Tag>
                  </span>
                </Col>
              )}
              {iva !== undefined && (
                <Col span={12}>
                  <span>
                    Iva: <Tag color='processing'>{iva} %</Tag>
                  </span>
                </Col>
              )}
              <Divider />
              <Col span={10}>
                <Form.Item name='efectivo'>
                  <Input type='number' placeholder='Efectivo' onChange={calcular_vuelto} />
                </Form.Item>
              </Col>
              <Col span={13}>
                <span>
                  Vuelto: <Tag color='orange'>$ {vuelto}</Tag>
                </span>
              </Col>
              <Divider />
              <Col span={24} style={{ textAlign: 'center' }}>
                <Statistic
                  title='Total a pagar'
                  value={totalPago}
                  prefix={<DollarCircleOutlined />}
                />
              </Col>
              <Divider />
              <Col span={24}>
                <Button danger block onClick={emitir_factura}>
                  Emitir factura
                </Button>
                <br />
                <Button htmlType='submit' loading={isLoading} type='primary' block>
                  Confirmar compra
                </Button>
              </Col>
            </Row>
          </Form>
        </>
      </Drawer>
    </>
  );
}
