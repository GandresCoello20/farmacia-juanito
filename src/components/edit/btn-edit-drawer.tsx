import React, { useState } from 'react';
import { Button, Drawer } from 'antd';
import { EditOutlined } from '@ant-design/icons';

interface Props {
    icono?: boolean;
    formulario: string;
}

export function BtnEdit({ icono, formulario }: Props) {

    const [visible, setVisible] = useState<boolean>(false);

    return(
        <>
            <Button type="primary" onClick={() => setVisible(true)}>
                {icono ? <EditOutlined /> : 'Editar'}
            </Button>
            <Drawer
                title="ACTUALIZACIONES"
                placement="right"
                closable={false}
                onClose={ () => setVisible(false)}
                visible={visible}
            >
                {formulario !== '' && (
                    <>
                        <p>Some contents...</p>
                        <p>Some contents...</p>
                        <p>Some contents...</p>
                    </>
                )}
            </Drawer>
        </>
    );
}