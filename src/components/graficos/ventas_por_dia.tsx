import React, { useState, useEffect } from 'react';
import { useSelector } from 'react-redux';
import { RootState } from '../../redux';
import { Line } from 'react-chartjs-2';
import moment from 'moment';
import { Factura_INT } from '../../interface';

export function VentasEstadisticas() {
  const [cantidadVenta, setCantidadVenta] = useState<number[]>([]);
  const [monto, setMonto] = useState<number[]>([]);
  const [label, setLabel] = useState<string[]>([]);

  const Ventas = useSelector((state: RootState) => state.VentasReduce.facturas);

  useEffect(() => {
    const count: Array<number> | any = [];
    const monto: Array<number> | any = [];
    const label: Array<string> | any = [];

    Ventas.map((ventas: Factura_INT) => count.push(ventas.carrito.length));
    Ventas.map((ventas: Factura_INT) => monto.push(ventas.total));
    Ventas.map((ventas: Factura_INT) => label.push(moment(ventas.fecha_factura).format('LTS')));

    setCantidadVenta(count);
    setLabel(label);
    setMonto(monto);
  }, [Ventas]);

  return (
    <>
      <Line
        data={{
          labels: label,
          datasets: [
            {
              label: 'Productos vendidos',
              data: cantidadVenta,
              backgroundColor: [
                `rgba(255, 206, 86, 0.2)`,
                `rgba(255, 99, 132, 0.2)`,
                `rgba(54, 162, 235, 0.2)`,
                `rgba(75, 192, 192, 0.2)`,
                `rgba(153, 102, 255, 0.2)`,
                `rgba(255, 159, 64, 0.2)`,
              ],
              borderColor: [
                `rgba(255, 99, 132, 1)`,
                `rgba(54, 162, 235, 1)`,
                `rgba(255, 206, 86, 1)`,
                `rgba(75, 192, 192, 1)`,
                `rgba(153, 102, 255, 1)`,
                `rgba(255, 159, 64, 1)`,
              ],
              borderWidth: 2,
            },
            {
              label: 'Monto obtenido',
              data: monto,
              backgroundColor: [
                'rgba(75, 192, 192, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(255, 99, 132, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(255, 159, 64, 0.2)',
              ],
              borderColor: [
                'rgba(75, 192, 192, 1)',
                'rgba(255, 99, 132, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(255, 159, 64, 1)',
              ],
              borderWidth: 2,
            },
          ],
        }}
      />
    </>
  );
}
