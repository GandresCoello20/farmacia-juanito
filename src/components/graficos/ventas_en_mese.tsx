import React, { useState, useEffect } from 'react';
import { obtenerVentasPorMes } from '../../api/ventas';
import { Doughnut } from 'react-chartjs-2';
import { message, Spin } from 'antd';

export function VentasMeses() {
  const [meses, setMeses] = useState<Array<any>>([]);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  useEffect(() => {
    setIsLoading(true);
    const VentasPorMes = async () => {
      let mes = [];
      try {
        const resVentaMeses = await obtenerVentasPorMes();

        for (let i = 0; i < resVentaMeses.data.length; i++) {
          if (resVentaMeses.data[i].length !== 0) {
            let total: number = 0;
            for (let j = 0; j < resVentaMeses.data[i].length; j++) {
              total = total + resVentaMeses.data[i][j].total;
            }
            mes.push(total);
          } else {
            mes.push(0);
          }
        }
        setIsLoading(false);
        setMeses(mes);
      } catch (error) {
        message.error(error.message);
        setIsLoading(false);
      }
    };

    VentasPorMes();
  }, []);

  return (
    <>
      {isLoading && (
        <div style={{ textAlign: 'center', padding: 10 }}>
          <Spin size='large' />
        </div>
      )}
      <Doughnut
        data={{
          labels: [
            'Enero',
            'Febrero',
            'Marzo',
            'Abril',
            'Mayo',
            'Junio',
            'Julio',
            'Agosto',
            'Septiembre',
            'Octubre',
            'Noviembre',
            'Diciembre',
          ],
          datasets: [
            {
              label: 'Productos vendidos',
              data: [
                meses[0],
                meses[1],
                meses[2],
                meses[3],
                meses[4],
                meses[5],
                meses[6],
                meses[7],
                meses[8],
                meses[9],
                meses[10],
                meses[11],
              ],
              backgroundColor: [
                `rgba(255, 99, 132, 0.2)`,
                `rgba(75, 192, 192, 0.2)`,
                `rgba(153, 102, 255, 0.2)`,
                `rgba(54, 162, 235, 0.2)`,
                `rgba(255, 206, 86, 0.2)`,
                `rgba(255, 159, 64, 0.2)`,
                `rgba(25, 199, 132, 0.2)`,
                `rgba(175, 142, 192, 0.2)`,
                `rgba(113, 152, 255, 0.2)`,
                `rgba(90, 162, 235, 0.2)`,
                `rgba(55, 236, 86, 0.2)`,
                `rgba(215, 59, 64, 0.2)`,
              ],
              borderColor: [
                `rgba(255, 99, 132, 1)`,
                `rgba(75, 192, 192, 1)`,
                `rgba(153, 102, 255, 1)`,
                `rgba(54, 162, 235, 1)`,
                `rgba(255, 206, 86, 1)`,
                `rgba(255, 159, 64, 1)`,
                `rgba(25, 199, 132, 1)`,
                `rgba(175, 142, 192, 1)`,
                `rgba(113, 152, 255, 1)`,
                `rgba(90, 162, 235, 1)`,
                `rgba(55, 236, 86, 1)`,
                `rgba(215, 59, 64, 1)`,
              ],
              borderWidth: 2,
            },
          ],
        }}
      />
    </>
  );
}
