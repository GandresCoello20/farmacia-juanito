import React, { useState } from 'react';
import { Drawer, Button } from 'antd';
import { AlertDynamic } from './alert-dinamico';
import { ItemNotificacionActividad } from './item-notificacion-actividad';

export function NotificacionesActivi(){
    const [visible, setVisible] = useState<boolean>(false);

    const onClose = () => setVisible(false);

    return(
        <>
            <div onClick={() => setVisible(true)}>
                <AlertDynamic />
            </div>
            <Drawer
                title="Notificacion de actividades"
                width={620}
                onClose={onClose}
                visible={visible}
                bodyStyle={{ paddingBottom: 80 }}
                footer={
                    <div style={{ textAlign: 'right' }}>
                        <Button onClick={onClose} style={{ marginRight: 8 }}>
                            Salir
                        </Button>
                    </div>
                }
            >
                <ItemNotificacionActividad />
            </Drawer>
        </>
    );
}