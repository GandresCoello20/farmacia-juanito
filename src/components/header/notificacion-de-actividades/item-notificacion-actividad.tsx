import React from 'react';
import { Alert } from 'antd';
import { Notificacion } from '../../../interface';
import moment from 'moment';
import { useSelector } from 'react-redux';
import { RootState } from '../../../redux';

export function ItemNotificacionActividad(){

    const NotificacionReducer = useSelector((state: RootState) => state.NotificacionReducer);

    return(
        <>
            {NotificacionReducer.notificaciones.length === 0 ? <Alert type='info' message='Por el momento no hay notificaciones que mostrar' /> : NotificacionReducer.notificaciones.reverse().map((notificacion: Notificacion) => (
                <Alert
                    message={notificacion.type + ' a las ' + moment(notificacion.date).format('LTS')}
                    description={notificacion.content}
                    type={notificacion.type === 'ERROR' ? 'error' : 'success'}
                    style={{ margin: 5 }}
                    showIcon
                />
            ))}
        </>
    );
}