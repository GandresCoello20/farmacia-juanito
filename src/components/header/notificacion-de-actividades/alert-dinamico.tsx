import React from 'react';
import { Alert } from 'antd';
import { Notificacion } from '../../../interface';
import { useSelector } from 'react-redux';
import { RootState } from '../../../redux';
import TextLoop from 'react-text-loop';


export function AlertDynamic(){

    const NotificacionReducer = useSelector((state: RootState) => state.NotificacionReducer);

    return(
        <>
            {NotificacionReducer.notificaciones.length !== 0 ? (
                <Alert
                    banner
                    message={
                    <TextLoop mask>
                        {NotificacionReducer.notificaciones.map( (item: Notificacion) => (
                            <div key={item.date.toString()}>{item.content}</div>
                        ))}
                    </TextLoop>
                    }
                />
            ) : <Alert
                    banner
                    message={
                    <TextLoop mask>
                        <div>No hay datos para mostrar</div>
                        <div>No hay datos para mostrar</div>
                    </TextLoop>
                    }
                /> }
        </>
    );
}