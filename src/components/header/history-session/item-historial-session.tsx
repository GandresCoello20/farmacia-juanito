import React from 'react';
import { History_session_INT } from '../../../interface';
import { DOMAIN } from '../../../config/domain';
import { RootState } from '../../../redux';
import moment from 'moment';
import { useSelector } from 'react-redux';
import { List, Avatar, Tag } from 'antd';

export function ItemHistorySession() {
  const historyReducer = useSelector((state: RootState) => state.UsuarioReducer.historyUser);

  return (
    <>
      <List
        itemLayout='horizontal'
        dataSource={historyReducer}
        renderItem={(item: History_session_INT) => (
          <List.Item>
            <List.Item.Meta
              avatar={<Avatar src={`${DOMAIN}/static/${item.foto}`} />}
              title={
                <>
                  <strong>
                    {item.nombres} {item.apellidos}
                  </strong>{' '}
                  &nbsp; &nbsp; &nbsp; <span>Email:</span> <strong>{item.email}</strong>
                </>
              }
              description={
                <>
                  <span>Tipo:</span> <Tag color='red'>{item.tipo_user}</Tag> &nbsp; &nbsp;{' '}
                  <span>Ingreso:</span>{' '}
                  <Tag color='volcano'>{moment(item.fecha_session).format('l, LTS')}</Tag>
                </>
              }
            />
          </List.Item>
        )}
      />
    </>
  );
}
