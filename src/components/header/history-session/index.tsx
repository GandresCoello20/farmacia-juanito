import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { useSelector, useDispatch } from 'react-redux';
import { RootState, Dispatch } from '../../../redux';
import { LimpiarHistorialSession } from '../../../api/fetch/usuarios';
import { ProfileOutlined } from '@ant-design/icons';
import { Button, Drawer, Row, Col, Divider, message } from 'antd';
import { ItemHistorySession } from './item-historial-session';
import { SetHistoryUser } from '../../../redux/modulos/usuario';
import { SetNotificacion } from '../../../redux/modulos/notificaciones';
import { ResponseAxios, Notificacion } from '../../../interface';


export function HistorialSession() {
    let notificacion: Notificacion;
    const dispatch: Dispatch = useDispatch();
    const [visible, setVisible] = useState<boolean>(false);

    const historyReducer = useSelector((state: RootState) => state.UsuarioReducer.historyUser);

    const showDrawer = () => setVisible(true);

    const onClose = () => setVisible(false);

    const limpiarHistorial = async () => {
        const responseAxios: ResponseAxios = await LimpiarHistorialSession();

        if(responseAxios.respuesta.type === 'ERROR'){
            message.error(responseAxios.respuesta.feeback);
        }else{
            dispatch(SetHistoryUser(responseAxios.axios.data));
            message.success('SE LIMPIO EL HISTORIAL DE SESSION');
        }
        
        notificacion = { type: responseAxios.respuesta.type, content: responseAxios.respuesta.feeback, date: new Date() }

        dispatch(SetNotificacion(notificacion));
    }

    return (
        <>
            <Button type="primary" onClick={showDrawer}>
                <ProfileOutlined /> Historial Session
            </Button>
            <Drawer
                title="Historial de sessiones ( ultimos 6 ingresos )"
                width={620}
                onClose={onClose}
                visible={visible}
                bodyStyle={{ paddingBottom: 80 }}
                footer={
                    <div style={{ textAlign: 'right' }}>
                        <Button onClick={onClose} style={{ marginRight: 8 }}>
                            Salir
                        </Button>
                    </div>
                }
            >
                <Row justify='center'>
                    <Col span={6}>
                        <Button danger disabled={historyReducer.length <= 1} onClick={limpiarHistorial}>Limpiar Historial</Button>
                    </Col>
                    <Col span={6}>
                        <Link to='/usuarios'>
                            <Button type='primary' >Ver los Usuarios</Button>
                        </Link>
                    </Col>
                </Row>
                <Divider />
                <ItemHistorySession />
            </Drawer>
        </>
    )
}