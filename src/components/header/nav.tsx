import React, { useState } from 'react';
import { Menu, Row, Col, Button, Badge } from 'antd';
import { Link, useHistory } from 'react-router-dom';
import {
  HomeOutlined,
  TagsOutlined,
  ShoppingCartOutlined,
  CloseCircleOutlined,
  ReconciliationOutlined,
  SolutionOutlined,
  TeamOutlined,
  BarChartOutlined,
  ShopOutlined,
  TableOutlined,
} from '@ant-design/icons';
import { HistorialSession } from './history-session';
import Cookie from 'js-cookie';
import { RootState } from '../../redux';
import { useSelector } from 'react-redux';
import { Head } from './head';
import { NotificacionesActivi } from './notificacion-de-actividades/';

interface Props {
  title: string;
}

export function Nav({ title }: Props) {
  const history = useHistory<typeof useHistory>();
  const [current, setCurrent] = useState<string>('home');

  const itemSelect = (e: any) => setCurrent(e.key);

  const CarritoReducer = useSelector((state: RootState) => state.CarritoReducer);

  const cerrarSession = () => {
    Cookie.remove('access-token');
    history.push('/login/iniciar-session');
  };

  return (
    <>
      <Head title={title} />
      <Menu onClick={itemSelect} selectedKeys={[current]} mode='horizontal'>
        <Menu.Item key='home' icon={<HomeOutlined />}>
          <Link to='/'>Inicio</Link>
        </Menu.Item>
        <Menu.Item key='product' icon={<TagsOutlined />}>
          <Link to='/productos'>Mis productos</Link>
        </Menu.Item>
        <Menu.Item key='stock' icon={<TableOutlined />}>
          <Link to='/stock'>Agregar Stock</Link>
        </Menu.Item>
        <Menu.Item key='ventas' icon={<ShopOutlined />}>
          <Link to='/ventas'>Ventas</Link>
        </Menu.Item>
        <Menu.Item key='graficos' icon={<BarChartOutlined />}>
          <Link to='/graficos'>Graficos</Link>
        </Menu.Item>
        <Menu.Item key='clientes' icon={<TeamOutlined />}>
          <Link to='/clientes'>Clientes</Link>
        </Menu.Item>
        <Menu.Item key='proveedores' icon={<SolutionOutlined />}>
          <Link to='/proveedores'>Proveedores</Link>
        </Menu.Item>
        <Menu.Item key='caja' icon={<ReconciliationOutlined />}>
          <Link to='/flujo-de-caja'>Flujo de caja</Link>
        </Menu.Item>
        <Menu.Item key='history-session'>
          <HistorialSession />
        </Menu.Item>
        <Menu.Item
          key='exit'
          onClick={cerrarSession}
          style={{ color: 'red' }}
          icon={<CloseCircleOutlined />}>
          Cerrar Session
        </Menu.Item>
      </Menu>
      <Row justify='space-between' style={{ padding: 20 }}>
        <Col span={5}>
          <NotificacionesActivi />
        </Col>
        <Col span={3}>
          <Link to='/carrito'>
            <Button danger={CarritoReducer.carrito.length === 0} size='middle'>
              <ShoppingCartOutlined style={{ fontSize: 25 }} />
              <Badge
                count={CarritoReducer.carrito.length}
                style={{ position: 'relative', top: -7 }}
              />
            </Button>
          </Link>
        </Col>
      </Row>
    </>
  );
}
