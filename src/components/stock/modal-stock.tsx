import React, { useState, useEffect } from 'react';
import { Modal, Button } from 'antd';
import { InputProduct } from './add-input/producto';
import { InputLaboratorio } from './add-input/laboratorio';
import { InputPincipioActive } from './add-input/principio-activo';
import { InputStock } from './add-input/stock';


interface Props {
    title: string;
    visible: boolean;
    form: string;
    width?: number;
}

export function ModalActionStock({ title, visible, form, width }: Props){

    const [visibleModal, setVisibleModal] = useState<boolean>(false);

    useEffect( () => {
        setVisibleModal(visible);
    },[visible]);

    // const showModal = () => setVisible(true);

    const hideModal = () => setVisibleModal(false);

    return(
        <>
            <Button onClick={() => setVisibleModal(true)}>
                {title}
            </Button>
            <Modal
                width={width}
                title={title}
                visible={visibleModal}
                onOk={hideModal}
                onCancel={hideModal}
                >
                {form === 'form-producto' && <InputProduct />}
                {form === 'form-laboratorio' && <InputLaboratorio />}
                {form === 'form-principe-active' && <InputPincipioActive />}
                {form === 'fomr-stock' && <InputStock />}
            </Modal>
        </>
    );
}