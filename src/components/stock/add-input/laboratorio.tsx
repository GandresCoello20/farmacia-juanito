import React, { useState } from 'react';
import { Dispatch } from '../../../redux';
import { ResponseAxios, Notificacion } from '../../../interface';
import { createLaboratorioName } from '../../../api/fetch/propiedades-productos';
import { useDispatch, useStore } from 'react-redux';
import { SetLaboratorioName } from '../../../redux/modulos/propiedades-producto';
import { SetNotificacion } from '../../../redux/modulos/notificaciones';
import { DetallesProductos } from './btn-detalles-productos';
import { Input, Form, Button, message, Row, Col } from 'antd';

export function InputLaboratorio() {
  const dispatch: Dispatch = useDispatch();
  const store = useStore().getState().PropiedadesReducer;
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [form] = Form.useForm();

  const send = async (data: any) => {
    let notificacion: Notificacion;
    const { nombre_laboratorio } = data;

    if (!nombre_laboratorio) {
      message.error('SE ENCONTRARON CAMPOS VACIOS');
      return false;
    }

    setIsLoading(true);

    const responseAxios: ResponseAxios = await createLaboratorioName(nombre_laboratorio);

    if(responseAxios.axios === undefined){
      console.log('error aqui');
      return false;
    }

    if (responseAxios.respuesta.type === 'ERROR') {
      message.error(`${responseAxios.respuesta.feeback}`);
      notificacion = { type: responseAxios.respuesta.type, content: responseAxios.respuesta.feeback, date: new Date() }
    } else {
      dispatch(SetLaboratorioName([...store.laboratorio, ...responseAxios.axios.data]));
      notificacion = { type: 'EXITO', content: 'SE CREO EL NOMBRE DE LABORATORIO', date: new Date() }
      message.success('SE GUARDO EL NOMBRE DEL LABORATORIO');
      form.resetFields();
    }

    dispatch(SetNotificacion(notificacion));
    setIsLoading(false);
  };

  return (
    <>
      <Form form={form} onFinish={send}>
        <Form.Item
          name='nombre_laboratorio'
          rules={[
            {
              required: true,
              message: 'Especifica el nombre del laboratorio',
            },
          ]}>
          <Input placeholder='Nombre del laboratorio' allowClear onChange={() => true} />
        </Form.Item>
        <Row justify='space-between'>
          <Col span={12}>
            <Form.Item>
              <Button htmlType='submit' loading={isLoading} type='primary'>
                Guardar nombre del laboratorio
              </Button>
            </Form.Item>
          </Col>
          <Col span={12}>
            <DetallesProductos detalles='Nombres Laboratorios' />
          </Col>
        </Row>
      </Form>
    </>
  );
}
