import React from 'react';
import { Button } from 'antd';
import { Link } from 'react-router-dom';

interface Props {
    detalles: string;
}

export function DetallesProductos({ detalles }: Props){
    return(
        <>
            <Link to='/detalles-producto'>
                <Button type='link'>Ver {detalles}</Button>
            </Link>
        </>
    );
}