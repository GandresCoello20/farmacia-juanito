import React, { useState } from 'react';
import { Input, Form, Button, message, Row, Col } from 'antd';
import { ResponseAxios, Notificacion } from '../../../interface';
import { useDispatch, useStore } from 'react-redux';
import { Dispatch } from '../../../redux';
import { createPrincipioActive } from '../../../api/fetch/propiedades-productos';
import { DetallesProductos } from './btn-detalles-productos';
import { SetPrincipioActivo } from '../../../redux/modulos/propiedades-producto';
import { SetNotificacion } from '../../../redux/modulos/notificaciones';

export function InputPincipioActive() {
  let notificacion: Notificacion;
  const dispatch: Dispatch = useDispatch();
  const store = useStore().getState().PropiedadesReducer;
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [form] = Form.useForm();

  const send = async (data: any) => {
    const { principio_activo } = data;

    if (!principio_activo) {
      message.error('SE ENCONTRARON CAMPOS VACIOS');
      return false;
    }

    setIsLoading(true);

    const responseAxios: ResponseAxios = await createPrincipioActive(principio_activo);

    if (responseAxios.respuesta.type === 'ERROR') {
      message.error(`${responseAxios.respuesta.feeback}`);
      notificacion = { type: responseAxios.respuesta.type, content: responseAxios.respuesta.feeback, date: new Date()}
    } else {
      dispatch(SetPrincipioActivo([...store.principio_activo, ...responseAxios.axios.data]));
      notificacion = { type: 'EXITO', content: 'SE CREO EL PRINCIPIO ACTIVO', date: new Date() }
      message.success('SE GUARDO EL PRINCIPIO ACTIVO');
      form.resetFields();
    }
    dispatch(SetNotificacion(notificacion));
    setIsLoading(false);
  };

  return (
    <>
      <Form form={form} onFinish={send}>
        <Form.Item
          name='principio_activo'
          rules={[
            {
              required: true,
              message: 'Especifica el principio-activo',
            },
          ]}>
          <Input placeholder='Principio activo...' allowClear onChange={() => true} />
        </Form.Item>
        <Row justify='space-between'>
          <Col span={12}>
            <Form.Item>
              <Button htmlType='submit' loading={isLoading} type='primary'>
                Guardar Principio activo
              </Button>
            </Form.Item>
          </Col>
          <Col span={12}>
            <DetallesProductos detalles='Principios activos' />
          </Col>
        </Row>
      </Form>
    </>
  );
}
