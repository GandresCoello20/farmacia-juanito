import React, { useState } from 'react';
import { Input, Form, Button, message, Row, Col } from 'antd';
import { useDispatch, useStore } from 'react-redux';
import { Dispatch, RootState } from '../../../redux';
import { ResponseAxios, Notificacion } from '../../../interface';
import { DetallesProductos } from './btn-detalles-productos';
import { createProductoName } from '../../../api/fetch/propiedades-productos';
import { SetNombreProductos } from '../../../redux/modulos/propiedades-producto';
import { SetNotificacion } from '../../../redux/modulos/notificaciones';

export function InputProduct() {
  let notificacion: Notificacion;
  const dispatch: Dispatch = useDispatch();
  const store: RootState = useStore().getState();
  const [isLoading, setIsLoading] = useState<boolean>(false);
  const [form] = Form.useForm();

  const send = async (data: any) => {
    const { nombre_product } = data;

    if (!nombre_product) {
      message.error('SE ENCONTRARON CAMPOS VACIOS');
      return false;
    }

    setIsLoading(true);

    const responseAxios: ResponseAxios = await createProductoName(nombre_product);

    if (responseAxios.axios === undefined) {
      console.log(responseAxios);
      console.log('esto es un error, pero aun no se como notificarlo, pendiente.....');
    }

    if (responseAxios.respuesta.type === 'ERROR') {
      message.error(`${responseAxios.respuesta.feeback}`);
      notificacion = { type: responseAxios.respuesta.type, content: responseAxios.respuesta.feeback, date: new Date() }
    } else {
      dispatch(SetNombreProductos([ ...store.PropiedadesReducer.nombre, ...responseAxios.axios.data]));
      notificacion = { type: 'EXITO', content: 'SE CREO EL NOMBRE DEL PRODUCTO', date: new Date() };
      message.success('SE CREO EL NOMBRE DEL PRODUCTO');
      form.resetFields();
    }
    dispatch(SetNotificacion(notificacion));
    setIsLoading(false);
  };
  return (
    <>
      <Form form={form} onFinish={send}>
        <Form.Item
          name='nombre_product'
          rules={[
            {
              required: true,
              message: 'Especifica el nombre del producto',
            },
          ]}>
          <Input placeholder='Nombre del producto' allowClear onChange={() => true} />
        </Form.Item>
        <Row justify='space-between'>
          <Col span={12}>
            <Form.Item>
              <Button htmlType='submit' loading={isLoading} type='primary'>
                Guardar nombre del producto
              </Button>
            </Form.Item>
          </Col>
          <Col span={12}>
            <DetallesProductos detalles='Nombre del producto' />
          </Col>
        </Row>
      </Form>
    </>
  );
}
