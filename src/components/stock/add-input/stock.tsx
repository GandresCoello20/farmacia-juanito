import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Dispatch, RootState } from '../../../redux';
import {
  ResponseAxios,
  Producto_INT,
  Producto_name,
  Laboratorio,
  Principio_activo,
} from '../../../interface';
import { createProducto } from '../../../api/fetch/producto';
import { SetProductos } from '../../../redux/modulos/productos';
import { Input, Form, Button, message, Row, Col, Select, DatePicker } from 'antd';

export function InputStock() {
  const dispatch: Dispatch = useDispatch();
  const { Option } = Select;
  const [form] = Form.useForm();

  const [elaboracion, setElaboracion] = useState<string>('');
  const [caducidad, setCaducidad] = useState<string>('');
  const [selectPrincipe, setSelectPrincipe] = useState<boolean>(false);
  const [isLoading, setIsLoading] = useState<boolean>(false);

  const PropiedadesProductosReducer = useSelector((state: RootState) => state.PropiedadesReducer);

  const presentacion = (propiedades: any) => {
    if (propiedades.value === 'Insumos medicos' || propiedades.value === 'Cosmeticos') {
      setSelectPrincipe(true);
    } else {
      setSelectPrincipe(false);
    }
  };

  const send = async (data: any) => {
    setIsLoading(true);
    const {
      nombre_product,
      nombre_laboratorio,
      presentacion,
      principio_activo,
      cantidad,
      cantidad_disponible,
      pvp,
      pvf,
      lote,
      registro_sanitario,
      tipo_de_medidas,
      medidas,
    } = data;

    const datos: Producto_INT | any = {
      id_name_product: Number(nombre_product.value),
      id_name_laboratorio: Number(nombre_laboratorio.value),
      cantidad: Number(cantidad),
      presentacion: String(presentacion.value),
      lote: String(lote),
      registro_sanitario: String(registro_sanitario),
      dosis: Number(medidas),
      tipo_dosis: String(tipo_de_medidas.value),
      fecha_elaboracion: elaboracion,
      fecha_caducidad: caducidad,
      pvp: Number(pvp),
      pvf: Number(pvf),
      id_principio_activo: Number(principio_activo.value),
      cantidad_disponible: Number(cantidad_disponible),
      veces_ingreso: 1,
    };

    const responseAxios: ResponseAxios = await createProducto(datos);

    if (responseAxios.axios === undefined) {
      console.log(responseAxios);
      console.log('esto es un error, pero aun no se como notificarlo, pendiente.....');
    }

    if (responseAxios.respuesta.type === 'ERROR') {
      message.error(`${responseAxios.respuesta.feeback}`);
    } else {
      dispatch(SetProductos(responseAxios.axios.data));
      message.success('SE CREO EL PRODUCTO CON TODAS SUS PROPIEDADES');
      form.resetFields();
    }
    setIsLoading(false);
  };
  return (
    <>
      <Form form={form} onFinish={send}>
        <Row justify='space-between'>
          <Col span={3}>
            <Form.Item
              label='Name product'
              name='nombre_product'
              rules={[
                {
                  required: true,
                  message: 'Especifica el nombre del producto',
                },
              ]}>
              <Select
                labelInValue
                placeholder='Productos'
                style={{ width: 120 }}
                onChange={() => true}>
                {PropiedadesProductosReducer.nombre.map((nombre: Producto_name) => (
                  <Option value={nombre.id_product_name}>{nombre.product_name}</Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item
              label='Laboratorio'
              name='nombre_laboratorio'
              rules={[
                {
                  required: true,
                  message: 'Especifica el nombre de laboratorio',
                },
              ]}>
              <Select
                labelInValue
                placeholder='laboratorio'
                style={{ width: 120 }}
                onChange={() => true}>
                {PropiedadesProductosReducer.laboratorio.map((laboratorio: Laboratorio) => (
                  <Option value={laboratorio.id_name_laboratorio}>
                    {laboratorio.nombre_laboratorio}
                  </Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item
              label='Presentacion'
              name='presentacion'
              rules={[
                {
                  required: true,
                  message: 'Especifica la presentacion',
                },
              ]}>
              <Select
                labelInValue
                placeholder='Productos'
                style={{ width: 120 }}
                onChange={presentacion}>
                <Option value='Tabletas'>Tabletas</Option>
                <Option value='Suero'>Suero</Option>
                <Option value='Jarabe'>Jarabe</Option>
                <Option value='Gotas'>Gotas</Option>
                <Option value='Capsula'>Capsula</Option>
                <Option value='Cosmeticos'>Cosmeticos</Option>
                <Option value='Ovulos'>Ovulos</Option>
                <Option value='Cremas'>Cremas</Option>
                <Option value='Sobres'>Sobres</Option>
                <Option value='Ampollas'>Ampollas</Option>
                <Option value='Insumos medicos'>Insumos medicos</Option>
                <Option value='Comprimidos recubiertos'>Comprimidos recubiertos</Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item
              label='Princi Activo'
              name='principio_activo'
              rules={[
                {
                  required: true,
                  message: 'Selecciona el principio activo',
                },
              ]}>
              <Select
                labelInValue
                placeholder='principio activo'
                disabled={selectPrincipe}
                style={{ width: 120 }}
                onChange={() => true}>
                {PropiedadesProductosReducer.principio_activo.map((active: Principio_activo) => (
                  <Option value={active.id_principio_activo}>{active.principio_activo}</Option>
                ))}
              </Select>
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item
              label='Cantidad'
              name='cantidad'
              rules={[
                {
                  required: true,
                  message: 'Especifica la cantidad de unidades por caja',
                },
              ]}>
              <Input placeholder='Cantidad' type='number' />
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item
              label='Disponible Cant'
              name='cantidad_disponible'
              rules={[
                {
                  required: true,
                  message: 'Especifica la cantidad de unidades disponibles',
                },
              ]}>
              <Input type='number' placeholder='Disponible' />
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item
              label='PVP prod'
              name='pvp'
              rules={[
                {
                  required: true,
                  message: 'Especifica el precio de venta al publico',
                },
              ]}>
              <Input type='number' placeholder='PVP' />
            </Form.Item>
          </Col>
          <Col span={2}>
            <Form.Item
              label='PVF prod'
              name='pvf'
              rules={[
                {
                  required: true,
                  message: 'Especifica el precio de farmacia',
                },
              ]}>
              <Input type='number' placeholder='PVF' />
            </Form.Item>
          </Col>
        </Row>

        <Row justify='space-between'>
          <Col span={3}>
            <Form.Item
              label='Lote del producto'
              name='lote'
              rules={[
                {
                  required: true,
                  message: 'Especifica el lote',
                },
              ]}>
              <Input type='number' placeholder='Lote' />
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item
              label='Registro Sanitario'
              name='registro_sanitario'
              rules={[
                {
                  required: true,
                  message: 'Especifica el registro sanitario',
                },
              ]}>
              <Input type='text' placeholder='Regitro Sanitario' />
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item
              label='Tipo de medidas'
              name='tipo_de_medidas'
              rules={[
                {
                  required: true,
                  message: 'Especifica el tipo de medidas',
                },
              ]}>
              <Select
                labelInValue
                placeholder='Tipo de medidas'
                style={{ width: 120 }}
                onChange={() => true}>
                <Option value='Miligramos'>Miligramos</Option>
                <Option value='Gramos'>Gramos</Option>
                <Option value='Litros'>Litros</Option>
                <Option value='Pulgadas'>Pulgadas</Option>
                <Option value='Centimetros'>Centimetros</Option>
                <Option value='Mililitros'>Mililitros</Option>
              </Select>
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item
              label='Cantidad de medida'
              name='medidas'
              rules={[
                {
                  required: true,
                  message: 'Especifica la cantidad de medidas',
                },
              ]}>
              <Input type='number' placeholder='Cantidad de medidas' />
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item
              label='Fecha de Elaboracion'
              name='fecha_de_elaboracion'
              rules={[
                {
                  required: true,
                  message: 'Especifica la fecha de elaboracion',
                },
              ]}>
              <DatePicker
                placeholder='Elaboracion'
                onChange={(date: any, dateString: string) => setElaboracion(dateString)}
              />
            </Form.Item>
          </Col>
          <Col span={3}>
            <Form.Item
              label='Fecha de caducidad'
              name='fecha_de_caducidad'
              rules={[
                {
                  required: true,
                  message: 'Especifica la fecha de caducidad',
                },
              ]}>
              <DatePicker
                placeholder='Caducidad'
                disabled={elaboracion === ''}
                onChange={(date: any, dateString: string) => setCaducidad(dateString)}
              />
            </Form.Item>
          </Col>
        </Row>

        <Form.Item>
          <Button htmlType='submit' loading={isLoading} type='primary'>
            Guardar
          </Button>
        </Form.Item>
      </Form>
    </>
  );
}
