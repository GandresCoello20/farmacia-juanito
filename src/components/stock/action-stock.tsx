import React from 'react';
import { Row, Col } from 'antd';
import { ModalActionStock } from './modal-stock';

export function ActionStock(){
    return(
        <>
            <Row justify='space-between'>
                <Col>
                    <ModalActionStock title='Add Producto' visible={false} form='form-producto' />
                </Col>
                <Col>
                    <ModalActionStock title='Add Laboratorio' visible={false} form='form-laboratorio' />
                </Col>
                <Col>
                    <ModalActionStock title='Add Principio activo' visible={false} form='form-principe-active' />
                </Col>
                <Col>
                    <ModalActionStock title='Add Stock' visible={false} form='fomr-stock' width={1100} />
                </Col>
            </Row>
        </>
    );
}