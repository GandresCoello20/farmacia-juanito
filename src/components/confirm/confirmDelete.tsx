import React from 'react';
import { Dispatch, RootState } from '../../redux';
import { useHistory } from 'react-router-dom';
import { useDispatch, useStore } from 'react-redux';
import { DeleteOutlined } from '@ant-design/icons';
import { Popconfirm, Button, message } from 'antd';
import {
  Principio_activo,
  Laboratorio,
  Producto_name,
  Producto_INT,
  Usuario_INT,
  Cliente_INT,
  Factura_INT,
  Proveedor_INT,
  Prestamo_INT,
} from '../../interface';
import {
  eliminarProducto,
  eliminarPrincipioActive,
  eliminarLaboratorio,
  eliminarProductName,
  eliminarUsuario,
  eliminarCliente,
  eliminarFactura,
  eliminarProveedor,
  eliminarProductoProveedor,
  eliminarPrestamo,
} from '../../hooks/delete';
import { SetProductos } from '../../redux/modulos/productos';
import {
  SetPrincipioActivo,
  SetLaboratorioName,
  SetNombreProductos,
} from '../../redux/modulos/propiedades-producto';
import { SetCliente } from '../../redux/modulos/clientes';
import { SetNotificacion } from '../../redux/modulos/notificaciones';
import { SetVentas } from '../../redux/modulos/ventas';
import { SetProveedores } from '../../redux/modulos/proveedores';
import { SetPrestamos } from '../../redux/modulos/prestamos';
import {
  RemoveStoreLaboratorio,
  RemoveStorePrincipe,
  RemoveStoreProduct,
  RemoveStoreProductName,
  RemoveStoreUser,
  RemoveStoreClient,
  RemoveStoreFactura,
  RemoveStoreProveedor,
  RemoveStorePrestamo,
} from '../../hooks/deleteReducer';

interface Props {
  id: string | number;
  tabla: string;
  icono?: boolean;
}

export function ConfirmDelete({ id, tabla, icono }: Props) {
  const dispatch: Dispatch = useDispatch();
  const store: RootState = useStore().getState();
  const history = useHistory<typeof useHistory>();

  const messageAction = (type: string | undefined, sms: string | undefined) => {
    if (type === 'ERROR') {
      message.error(sms);
    } else {
      message.success(sms);
    }
  };

  const CasePrincipeActive = async () => {
    const resPrincipio = await eliminarPrincipioActive(id);
    messageAction(resPrincipio.respuesta.type, resPrincipio.respuesta.feeback);
    if (resPrincipio.respuesta.type !== 'ERROR') {
      const principioActive: Array<Principio_activo> = RemoveStorePrincipe(store, id);
      dispatch(SetPrincipioActivo([...principioActive]));
    }
    dispatch(SetNotificacion(resPrincipio.notificacion));
  };

  const CaseLaboratorio = async () => {
    const resLaboratorio = await eliminarLaboratorio(id);
    messageAction(resLaboratorio.respuesta.type, resLaboratorio.respuesta.feeback);
    if (resLaboratorio.respuesta.type !== 'ERROR') {
      const Lab: Array<Laboratorio> = RemoveStoreLaboratorio(store, id);
      dispatch(SetLaboratorioName([...Lab]));
    }
    dispatch(SetNotificacion(resLaboratorio.notificacion));
  };

  const CaseProductName = async () => {
    const ProductName = await eliminarProductName(id);
    messageAction(ProductName.respuesta.type, ProductName.respuesta.feeback);
    if (ProductName.respuesta.type !== 'ERROR') {
      const Product: Array<Producto_name> = RemoveStoreProductName(store, id);
      dispatch(SetNombreProductos([...Product]));
    }
    dispatch(SetNotificacion(ProductName.notificacion));
  };

  const CaseProduct = async () => {
    const resProduct = await eliminarProducto(id);
    messageAction(resProduct.respuesta.type, resProduct.respuesta.feeback);
    const Product: Array<Producto_INT> = RemoveStoreProduct(store, id);
    if (resProduct.respuesta.type !== 'ERROR') {
      dispatch(SetProductos([...Product]));
    }
    dispatch(SetNotificacion(resProduct.notificacion));
  };

  const CaseUsuario = async () => {
    const resUser = await eliminarUsuario(id);
    messageAction(resUser.respuesta.type, resUser.respuesta.feeback);
    const User: Array<Usuario_INT> = RemoveStoreUser(store, id);
    if (resUser.respuesta.type !== 'ERROR') {
      dispatch(SetProductos([...User]));
    }
    dispatch(SetNotificacion(resUser.notificacion));
  };

  const CaseCliente = async () => {
    const resClient = await eliminarCliente(id);
    messageAction(resClient.respuesta.type, resClient.respuesta.feeback);
    const Client: Array<Cliente_INT> = RemoveStoreClient(store, id);
    if (resClient.respuesta.type !== 'ERROR') {
      dispatch(SetCliente([...Client]));
    }
    dispatch(SetNotificacion(resClient.notificacion));
  };

  const CaseFactura = async () => {
    const resFact = await eliminarFactura(id);
    messageAction(resFact.respuesta.type, resFact.respuesta.feeback);
    const factura: Array<Factura_INT> = RemoveStoreFactura(store, id);
    if (resFact.respuesta.type !== 'ERROR') {
      dispatch(SetVentas([...factura]));
    }
    dispatch(SetNotificacion(resFact.notificacion));
  };

  const CaseProveedor = async () => {
    const resProveedor = await eliminarProveedor(id);
    messageAction(resProveedor.respuesta.type, resProveedor.respuesta.feeback);
    const proveedor: Array<Proveedor_INT> = RemoveStoreProveedor(store, id);
    if (resProveedor.respuesta.type !== 'ERROR') {
      dispatch(SetProveedores([...proveedor]));
    }
    dispatch(SetNotificacion(resProveedor.notificacion));
  };

  const CaseProductosProveedor = async () => {
    const resProductProveedor = await eliminarProductoProveedor(id);
    messageAction(resProductProveedor.respuesta.type, resProductProveedor.respuesta.feeback);
    if (resProductProveedor.respuesta.type !== 'ERROR') {
      history.push('/proveedores');
    }
    dispatch(SetNotificacion(resProductProveedor.notificacion));
  };

  const CasePrestamo = async () => {
    const resPrestamo = await eliminarPrestamo(id);
    messageAction(resPrestamo.respuesta.type, resPrestamo.respuesta.feeback);
    const prestamo: Array<Prestamo_INT> = RemoveStorePrestamo(store, id);
    if (resPrestamo.respuesta.type !== 'ERROR') {
      dispatch(SetPrestamos([...prestamo]));
    }
    dispatch(SetNotificacion(resPrestamo.notificacion));
  };

  const eliminarRegistro = async (id: string | number, tabla: string) => {
    switch (tabla) {
      case 'productos':
        CaseProduct();
        break;
      case 'cliente':
        CaseCliente();
        break;
      case 'producto_factura':
        console.log(id + ' producto_factura');
        break;
      case 'factura':
        CaseFactura();
        break;
      case 'principio_activo':
        CasePrincipeActive();
        break;
      case 'nombre_producto':
        CaseProductName();
        break;
      case 'nombre_laboratorio':
        CaseLaboratorio();
        break;
      case 'usuarios':
        CaseUsuario();
        break;
      case 'proveedor':
        CaseProveedor();
        break;
      case 'productoProveedor':
        CaseProductosProveedor();
        break;
      case 'prestamo':
        CasePrestamo();
        break;
      default:
        console.info('no se encontro la tabla');
    }
  };

  return (
    <>
      <Popconfirm
        title='¿Estas seguro que quieres eliminar este elemento?'
        okText='SI'
        onCancel={() => console.log('se cancelo')}
        onConfirm={() => eliminarRegistro(id, tabla)}
        cancelText='No'>
        {icono ? (
          <Button danger>
            <DeleteOutlined />
          </Button>
        ) : (
          <Button danger>Eliminar</Button>
        )}
      </Popconfirm>
    </>
  );
}
