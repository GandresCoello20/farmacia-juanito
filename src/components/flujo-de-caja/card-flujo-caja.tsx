import React from 'react';
import { Row, Col, Card, Tag } from 'antd';

interface Props {
  ingresos: number;
  egresos: number;
  saldo: number;
}

export function CardFlujoCaja({ ingresos, egresos, saldo }: Props) {
  return (
    <>
      <div style={{ backgroundColor: '#cdcdcd', padding: 10 }}>
        <Row justify='center' gutter={16}>
          <Col span={5}>
            <Card
              title={
                <>
                  <strong>Ingresos:</strong> &nbsp; &nbsp;
                  <Tag style={{ fontSize: 20, padding: 6 }} color='gold'>
                    $ {ingresos}
                  </Tag>
                </>
              }>
              <p>
                Se obtiene de las ventas en productos y pagos o abonos de los prestamos en caja.
              </p>
            </Card>
          </Col>
          <Col span={5}>
            <Card
              title={
                <>
                  <strong>Egresos:</strong> &nbsp; &nbsp;
                  <Tag style={{ fontSize: 20, padding: 6 }} color='red'>
                    $ {egresos}
                  </Tag>
                </>
              }>
              <p>Se obtiene del pago de los productos del proveedor y prestamos en caja.</p>
            </Card>
          </Col>
          <Col span={5}>
            <Card
              title={
                <>
                  <strong>Saldos: </strong> &nbsp; &nbsp;
                  <Tag style={{ fontSize: 20, padding: 6 }} color='lime'>
                    $ {saldo}
                  </Tag>
                </>
              }>
              <p>
                Se obtiene de la diferencia entre los <strong>Ingresos</strong> y{' '}
                <strong>Egresos</strong>.
              </p>
            </Card>
          </Col>
        </Row>
      </div>
    </>
  );
}
