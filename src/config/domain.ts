import Cookie from 'js-cookie';
export const PRODUCTION: string = 'http://api-laboratorio-juanito.herokuapp.com';
export const RESPONSE: string = 'https://api-response-farmacia-juanito.herokuapp.com';
export const DEVELOMENP: string = 'http://localhost:7000';
export const DOMAIN: string = RESPONSE;
//export const TOKEN: string = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZF91c2VyIjoibFhndUZZaHNQIiwidGlwb191c2VyIjoiQWRtaW5pc3RyYWRvciIsImlhdCI6MTU5NzcxMjY1OH0.vrTB-iPdgoWd8H7TFWfVJ-Q7u4AoXS72dTkrBZNTyJs";
export const TOKEN: string | undefined = Cookie.get('access-token');
