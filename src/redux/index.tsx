import { createStore, combineReducers, compose, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import { ResponseAxios } from '../interface';
import { TokenLife } from '../api/fetch/login';
import Cookie from 'js-cookie';
import { verificar_posibles_productos_caducados } from '../api/productos';
import ProductosReducer, { getProductos, getProductosCaducados } from './modulos/productos';
import PropiedadesReducer, {
  getLaboratorio,
  getPrincipioActive,
  getProductosName,
} from './modulos/propiedades-producto';
import NotificacionReducer from './modulos/notificaciones';
import CarritoReducer from './modulos/carrito';
import UsuarioReducer, { getUsuarios, getHistoryUser, getMyUser } from './modulos/usuario';
import ClienteReducer, { getClientes } from './modulos/clientes';
import VentasReduce, { getVentas } from './modulos/ventas';
import ProveedorReducer, { getProveedores } from './modulos/proveedores';
import PrestamosReducer, { getPrestamos } from './modulos/prestamos';
import { fecha_actual } from '../hooks/fechas';

const rootReducer = combineReducers({
  ProductosReducer,
  PropiedadesReducer,
  NotificacionReducer,
  CarritoReducer,
  UsuarioReducer,
  ClienteReducer,
  VentasReduce,
  ProveedorReducer,
  PrestamosReducer,
});

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}

const composeEnhancers =
  (window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] as typeof compose) || compose;

export default function generateStore() {
  const store = createStore(rootReducer, composeEnhancers(applyMiddleware(thunk)));

  getProductos()(store.dispatch);
  getProductosCaducados()(store.dispatch);
  getLaboratorio()(store.dispatch);
  getPrincipioActive()(store.dispatch);
  getProductosName()(store.dispatch);
  getUsuarios()(store.dispatch);
  getHistoryUser()(store.dispatch);
  getClientes()(store.dispatch);
  getVentas()(store.dispatch);
  getProveedores()(store.dispatch);
  getPrestamos()(store.dispatch);

  if (Cookie.get('access-token') !== undefined) {
    TokenLife(Cookie.get('access-token'))
      .then((user: ResponseAxios) => {
        getMyUser(user.axios.data.myUser)(store.dispatch);
      })
      .catch(err => console.error(err.message + ' error en getMyUser'));
  }

  if (Cookie.get('fecha-verificar-caducados') !== undefined) {
    if (Cookie.get('fecha-verificar-caducados') !== fecha_actual()) {
      try {
        verificar_posibles_productos_caducados().then(() => {
          console.log('SE REVISO LOS PRODUCTOS POSIBLES EN CADUCACION....');
          Cookie.set('fecha-verificar-caducados', fecha_actual());
        });
      } catch (error) {
        console.log(error.message + ' Verificacion de posibles productos caducados');
      }
    }
  } else {
    Cookie.set('fecha-verificar-caducados', fecha_actual());
  }

  return store;
}

const store = generateStore();
export type RootState = ReturnType<typeof rootReducer>;
export type Dispatch = typeof store.dispatch;
