import { obtenerVentas } from '../../api/ventas';
import { Factura_INT } from '../../interface';
import { Dispatch } from 'redux';
import { fecha_actual } from '../../hooks/fechas';

/// CONSTANTES

interface initialData {
  facturas: Array<Factura_INT>;
  search_facturas: Array<Factura_INT>;
  loading: boolean;
  error: string;
}

const initialData: initialData = {
  facturas: [],
  search_facturas: [],
  loading: true,
  error: '',
};

const GET_FACTURAS = 'GET_FACTURAS';
const SET_FACTURAS = 'SET_FACTIRAS';
const SEARCH_FACTURAS = 'SEARCH_FACTURAS';

/// REDUCER

export default function reducer(state = initialData, action: any) {
  switch (action.type) {
    case GET_FACTURAS:
      return { ...state, facturas: action.payload, loading: false };
    case SET_FACTURAS:
      return { ...state, facturas: action.payload };
    case SEARCH_FACTURAS:
      return { ...state, search_facturas: action.payload };
    default:
      return state;
  }
}

/// ACTIONS

export const getVentas = () => (dispatch: Dispatch) => {
  obtenerVentas(fecha_actual()).then(res => {
    dispatch({
      type: GET_FACTURAS,
      payload: res.data,
    });
  });
};

export const SetVentas = (data: Array<Factura_INT>) => (dispatch: Dispatch) => {
  dispatch({
    type: SET_FACTURAS,
    payload: data,
  });
};

export const SearchVentas = (data: Array<Factura_INT>) => (dispatch: Dispatch) => {
  dispatch({
    type: SEARCH_FACTURAS,
    payload: data,
  });
};
