import { obtenerProductoCaducados, obtenerProductoCompleto } from '../../api/productos';
import { Producto_INT } from '../../interface';
import { Dispatch } from 'redux';

// CONSTANTES
export interface initialData {
  productos: Array<Producto_INT>;
  searchProductos: Array<Producto_INT>;
  productosCaducados: Array<Producto_INT>;
  loading: boolean;
  loadingCaducado: boolean;
  error: string;
}

const initialData: initialData = {
  productos: [],
  searchProductos: [],
  productosCaducados: [],
  loading: true,
  loadingCaducado: true,
  error: '',
};

const GET_PRODUCTOS: string = 'GET_PRODUCTOS';
const GET_PRODUCTOS_CADUCADOS: string = 'GET_PRODUCTOS_CADUCADOS';
const SET_PRODUCTOS: string = 'SET_PRODUCTOS';
const SET_SEARCH_PRODUCTO: string = 'SET_SEARCH_PRODUCTO';

// REDUCER
export default function reducer(state = initialData, action: any) {
  switch (action.type) {
    case GET_PRODUCTOS:
      return { ...state, productos: action.payload, loading: false };
    case GET_PRODUCTOS_CADUCADOS:
      return { ...state, productosCaducados: action.payload, loadingCaducado: false };
    case SET_PRODUCTOS:
      return { ...state, productos: action.payload };
    case SET_SEARCH_PRODUCTO:
      return { ...state, searchProductos: action.payload };
    default:
      return state;
  }
}

// ACTIONS

export const getProductos = () => (dispatch: Dispatch) => {
  obtenerProductoCompleto().then(res => {
    dispatch({
      type: GET_PRODUCTOS,
      payload: res.data,
    });
  });
};

export const getProductosCaducados = () => (dispatch: Dispatch) => {
  obtenerProductoCaducados().then(res => {
    dispatch({
      type: GET_PRODUCTOS_CADUCADOS,
      payload: res.data,
    });
  });
};

export const SetProductos = (data: Array<any>) => (dispatch: Dispatch) => {
  dispatch({
    type: SET_PRODUCTOS,
    payload: data,
  });
};

export const SetSearchProduct = (data: Array<any>) => (dispatch: Dispatch) => {
  dispatch({
    type: SET_SEARCH_PRODUCTO,
    payload: data,
  });
};
