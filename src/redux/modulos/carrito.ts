import { Carrito_INT } from '../../interface';
import { Dispatch } from 'redux';

// CONSTANTES

interface initialData {
    carrito: Array<Carrito_INT>;
    loading: boolean;
    error: string;
}

const initialData: initialData = {
    carrito: [],
    loading: true,
    error: '',
};

const AGREGAR_AL_CARRITO: string = "AGREGAR_AL_CARRITO";
const LIMPIAR_CARRITO: string = "LIMPIAR_CARRITO";

// REDUCER

export default function reducer(state = initialData, action: any) {
    switch (action.type) {
        case AGREGAR_AL_CARRITO:
            return { ...state, carrito: action.payload };
        case LIMPIAR_CARRITO:
            return { ...state, carrito: [ ...action.payload ] }
        default:
            return state;
    }
}

export const SetCarrito = (producto: Array<any>)  => (dispatch: Dispatch) => {
    dispatch({
        type: AGREGAR_AL_CARRITO,
        payload: producto,
    });
};

export const ClearCarrito = () => (dispatch: Dispatch, getState: any) => {
    const carrito = getState().CarritoReducer.carrito;
    carrito.splice(0, carrito.length);

    dispatch({
        type: LIMPIAR_CARRITO,
        payload: carrito,
    });
}