import { Proveedor_INT } from '../../interface';
import { Dispatch } from 'redux';
import { obtenerProveedores } from '../../api/proveedores';

// CONSTANTES

interface initialData {
  proveedores: Array<Proveedor_INT>;
  searchProveedor: Array<Proveedor_INT>;
  loading: boolean;
  error: string;
}

const initialData: initialData = {
  proveedores: [],
  searchProveedor: [],
  loading: true,
  error: '',
};

const GET_PROVEEDOR = 'GET_PROVEEDOR';
const SET_PROVEEDOR = 'SET_PROVEEDOR';
const SEARCH_PROVEEDOR = 'SEARCH_PROVEEDOR';

export default function reducer(state = initialData, action: any) {
  switch (action.type) {
    case GET_PROVEEDOR:
      return { ...state, proveedores: action.payload, loading: false };
    case SET_PROVEEDOR:
      return { ...state, proveedores: action.payload };
    case SEARCH_PROVEEDOR:
      return { ...state, searchProveedor: action.payload };
    default:
      return state;
  }
}

export const getProveedores = () => (dispatch: Dispatch) => {
  obtenerProveedores().then(res => {
    dispatch({
      type: GET_PROVEEDOR,
      payload: res.data,
    });
  });
};

export const SetProveedores = (data: Array<Proveedor_INT>) => (dispatch: Dispatch) => {
  dispatch({
    type: SET_PROVEEDOR,
    payload: data,
  });
};

export const SetsearchProveedor = (data: Array<Proveedor_INT>) => (dispatch: Dispatch) => {
  dispatch({
    type: SEARCH_PROVEEDOR,
    payload: data,
  });
};
