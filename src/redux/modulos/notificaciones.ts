import { Notificacion } from '../../interface';
import { Dispatch } from 'redux';

// CONSTANTES

interface initialData {
    notificaciones: Array<Notificacion>;
    loading: boolean;
    error: string;
}

const initialData: initialData = {
    notificaciones: [],
    loading: true,
    error: '',
};

const AGREGAR_NOTIFICACION: string = "AGREGAR_NOTIFICACION";

// REDUCER

export default function reducer(state = initialData, action: any) {
    switch (action.type) {
        case AGREGAR_NOTIFICACION:
            return { ...state, notificaciones: [...state.notificaciones, ...action.payload] };
        default:
            return state;
    }
}


/// ACTIONS 

export const SetNotificacion = (notifica: Notificacion) => (dispatch: Dispatch) => {
    dispatch({
        type: AGREGAR_NOTIFICACION,
        payload: [notifica],
    });
};