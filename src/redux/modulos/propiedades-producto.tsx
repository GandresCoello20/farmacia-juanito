/* eslint-disable @typescript-eslint/no-unused-vars */
import {
  obtenerNameProduct,
  obtenerNameLaboratorio,
  obtenerPrincipioActive,
} from '../../api/propiedades-producto';
import { Dispatch } from 'redux';
import { Producto_name, Laboratorio, Principio_activo } from '../../interface';

// CONSTANTES
export interface initialData {
  nombre: Array<Producto_name>;
  loading_nombre: boolean;
  //////////////////
  laboratorio: Array<Laboratorio>;
  loading_laboratorio: boolean;
  //////////////////
  principio_activo: Array<Principio_activo>;
  loading_principio_activo: boolean;
  //////////////////
  error: string;
}

const initialData: initialData = {
  nombre: [],
  loading_nombre: true,
  laboratorio: [],
  loading_laboratorio: true,
  principio_activo: [],
  loading_principio_activo: true,
  error: '',
};

const GET_PRODUCTO_NAME: string = 'GET_PRODUCT_NAME';
const GET_LABORATORIO: string = 'GET_LABORATORIO';
const GET_PRINCIPIO_ACTIVE: string = 'GET_PRINCIPIO_ACTIVE';

const SET_PRODUCT_NAME: string = 'SET_PRODUCT_NAME';
const SET_LABORATORIO_NAME: string = 'SET_LABORATORIO_NAME';
const SET_PRINCIPIO_ACTIVO: string = 'SET_PRINCIPIO_ACTIVO';

// REDUCER
export default function reducer(state = initialData, action: any) {
  switch (action.type) {
    case GET_PRODUCTO_NAME:
      return { ...state, nombre: action.payload, loading_nombre: false };
    case GET_LABORATORIO:
      return { ...state, laboratorio: action.payload, loading_laboratorio: false };
    case GET_PRINCIPIO_ACTIVE:
      return { ...state, principio_activo: action.payload, loading_principio_activo: false };
    case SET_PRODUCT_NAME:
      return { ...state, nombre: action.payload };
    case SET_LABORATORIO_NAME:
      return { ...state, laboratorio: action.payload };
    case SET_PRINCIPIO_ACTIVO:
      return { ...state, principio_activo: action.payload };
    default:
      return state;
  }
}

// ACTIONS  GET

export const getProductosName = () => (dispatch: Dispatch) => {
  obtenerNameProduct().then(res => {
    dispatch({
      type: GET_PRODUCTO_NAME,
      payload: res.data,
    });
  });
};

export const getLaboratorio = () => (dispatch: Dispatch) => {
  obtenerNameLaboratorio().then(res => {
    dispatch({
      type: GET_LABORATORIO,
      payload: res.data,
    });
  });
};

export const getPrincipioActive = () => (dispatch: Dispatch) => {
  obtenerPrincipioActive().then(res => {
    dispatch({
      type: GET_PRINCIPIO_ACTIVE,
      payload: res.data,
    });
  });
};

// ACTIONS

export const SetNombreProductos = (data: Array<any>) => (dispatch: Dispatch) => {
  dispatch({
    type: SET_PRODUCT_NAME,
    payload: data,
  });
};

export const SetLaboratorioName = (data: Array<any>) => (dispatch: Dispatch) => {
  dispatch({
    type: SET_LABORATORIO_NAME,
    payload: data,
  });
};

export const SetPrincipioActivo = (data: Array<any>) => (dispatch: Dispatch) => {
  dispatch({
    type: SET_PRINCIPIO_ACTIVO,
    payload: data,
  });
};
