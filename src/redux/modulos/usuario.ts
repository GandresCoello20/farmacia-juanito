import { obtenerUsuarios, sessionHistory } from '../../api/usuarios';
import { Usuario_INT, History_session_INT } from '../../interface';
import { Dispatch } from 'redux';

/// CONSTANTES

export interface initialData {
    myUser: Array<Usuario_INT>;
    usuarios: Array<Usuario_INT>;
    historyUser: Array<Usuario_INT>
    loading: boolean;
    error: string;
}

const initialData: initialData = {
    myUser: [],
    usuarios: [],
    historyUser: [],
    loading: true,
    error: '',
};

const GET_USUARIOS = "GET_USUARIOS";
const GET_HISTORY_USER = "GET_HISTORY_USER";
const GET_MY_USER = "GET_MY_USER";
const CLEAR_HISTORY_USER = "CLEAR_HISTORY_USER";


/// REDUCER

export default function reducer(state = initialData, action: any) {
    switch (action.type) {
        case GET_USUARIOS:
            return { ...state, usuarios: action.payload, loading: false };
        case GET_HISTORY_USER:
            return { ...state, historyUser: action.payload };
        case CLEAR_HISTORY_USER:
            return { ...state, historyUser: [ ...action.payload ] };
        case GET_MY_USER:
            return { ...state, myUser: action.payload };
        default:
            return state;
    }
}


/// ACTIONS

export const getUsuarios = () => (dispatch: Dispatch) => {
    obtenerUsuarios().then(res => {
        dispatch({
            type: GET_USUARIOS,
            payload: res.data,
        });
    });
}

export const getHistoryUser = () => (dispatch: Dispatch) => {
    sessionHistory().then(res => {
        dispatch({
            type: GET_HISTORY_USER,
            payload: res.data,
        });
    });
}

export const getMyUser = (myUser: any) => (dispatch: Dispatch) => {
    dispatch({
        type: GET_MY_USER,
        payload: myUser,
    });
}

export const SetHistoryUser = (history: History_session_INT) => (dispatch: Dispatch) => {
    dispatch({
        type: CLEAR_HISTORY_USER,
        payload: [history],
    });
}
