import { Cliente_INT } from '../../interface';
import { Dispatch } from 'redux';
import { traerCLiente } from '../../api/clientes';

// CONSTANTES

interface initialData {
  clientes: Array<Cliente_INT>;
  searchClientes: Array<Cliente_INT>;
  loading: boolean;
  error: string;
}

const initialData: initialData = {
  clientes: [],
  searchClientes: [],
  loading: true,
  error: '',
};

const GET_CLIENTE = 'GET_CLIENTE';
const SET_CLIENTE = 'CREATE_NEW_CLIENT';
const SEARCH_CLIENTE = 'SEARCH_CLIENTE';

export default function reducer(state = initialData, action: any) {
  switch (action.type) {
    case GET_CLIENTE:
      return { ...state, clientes: action.payload, loading: false };
    case SET_CLIENTE:
      return { ...state, clientes: action.payload };
    case SEARCH_CLIENTE:
      return { ...state, searchClientes: action.payload };
    default:
      return state;
  }
}

export const getClientes = () => (dispatch: Dispatch) => {
  traerCLiente().then(res => {
    dispatch({
      type: GET_CLIENTE,
      payload: res.data,
    });
  });
};

export const SetCliente = (cliente: Array<Cliente_INT>) => (dispatch: Dispatch) => {
  dispatch({
    type: SET_CLIENTE,
    payload: cliente,
  });
};

export const SetSearchtCliente = (cliente: Array<Cliente_INT>) => (dispatch: Dispatch) => {
  dispatch({
    type: SEARCH_CLIENTE,
    payload: cliente,
  });
};
