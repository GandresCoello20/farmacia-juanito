import { obtenerPrestamos } from '../../api/prestamos';
import { Prestamo_INT } from '../../interface';
import { Dispatch } from 'redux';

/// CONSTANTES

export interface initialData {
  Prestamos: Array<Prestamo_INT>;
  loading: boolean;
  error: string;
}

const initialData: initialData = {
  Prestamos: [],
  loading: true,
  error: '',
};

const GET_PRESTAMOS = 'GET_PRESTAMOS';
const SET_PRESTAMOS = 'SET_PRESTAMOS';

/// REDUCER

export default function reducer(state = initialData, action: any) {
  switch (action.type) {
    case GET_PRESTAMOS:
      return { ...state, Prestamos: action.payload, loading: false };
    case SET_PRESTAMOS:
      return { ...state, Prestamos: action.payload };
    default:
      return state;
  }
}

/// ACTIONS

export const getPrestamos = () => (dispatch: Dispatch) => {
  obtenerPrestamos().then(res => {
    dispatch({
      type: GET_PRESTAMOS,
      payload: res.data,
    });
  });
};

export const SetPrestamos = (prestamos: Array<Prestamo_INT>) => (dispatch: Dispatch) => {
  dispatch({
    type: SET_PRESTAMOS,
    payload: prestamos,
  });
};
