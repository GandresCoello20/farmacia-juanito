import React, { Suspense } from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import { PublicRoute } from './public';
import { PrivateRoute } from './private';
import { LoginPage } from '../pages/login';
import { NotFound } from '../pages/not-found';
import { HomePage } from '../pages/home';
import { ProductosPage } from '../pages/productos';
import { StockPage } from '../pages/stock';
import { CarritoPage } from '../pages/carrito';
import { EmitirFact } from '../pages/emitir-factura';
import { VentasPage } from '../pages/ventas';

const UsuarioPage = React.lazy(() => import('../pages/usuarios'));
const ProveedorDetallePage = React.lazy(() => import('../pages/proveedor'));
const ClientePage = React.lazy(() => import('../pages/clientes'));
const ProveedorPage = React.lazy(() => import('../pages/proveedores'));
const GraficosPage = React.lazy(() => import('../pages/graficos'));
const DetallesProdutoPage = React.lazy(() => import('../pages/detalles-producto'));
const FlujoCaja = React.lazy(() => import('../pages/flujo-de-caja'));
const Prestamos = React.lazy(() => import('../pages/prestamo'));

export default function Routes() {
  return (
    <Suspense fallback={<div />}>
      <BrowserRouter>
        <Switch>
          <PublicRoute path='/login/:form' component={LoginPage} />
          <PrivateRoute path='/detalles-producto' component={DetallesProdutoPage} />
          <PrivateRoute path='/productos' component={ProductosPage} />
          <PrivateRoute path='/usuarios' component={UsuarioPage} />
          <PrivateRoute path='/clientes' component={ClientePage} />
          <PrivateRoute path='/emitir-factura' component={EmitirFact} />
          <PrivateRoute path='/stock' component={StockPage} />
          <PrivateRoute
            path='/proveedores/detalles/:id_proveedor/:proveedor'
            component={ProveedorDetallePage}
          />
          <PrivateRoute path='/proveedores' component={ProveedorPage} />
          <PrivateRoute path='/graficos' component={GraficosPage} />
          <PrivateRoute path='/ventas' component={VentasPage} />
          <PrivateRoute path='/carrito' component={CarritoPage} />
          <PrivateRoute path='/flujo-de-caja' component={FlujoCaja} />
          <PrivateRoute path='/prestamos' component={Prestamos} />
          <Route exact path='/404' component={NotFound} />
          <PrivateRoute path='/' component={HomePage} />
          <Route exact component={NotFound} />
        </Switch>
      </BrowserRouter>
    </Suspense>
  );
}
