import { AxiosResponse } from 'axios';

export interface CreateCount {
  nombres: string;
  apellidos: string;
  email: string;
  password: string;
  tipo: string;
}

export interface Usuario_INT {
  readonly id_user: string;
  nombres: string;
  apellidos: string;
  foto?: string;
  tipo_user: string;
  email: string;
  email_on: boolean | number;
  password?: string;
}

export interface Email_INT {
  id_user: string | any;
  hash: string;
}

export interface History_session_INT {
  id_user: string;
  fecha_session: any;
  nombres: string;
  apellidos: string;
  foto?: string;
  tipo_user: string;
  email: string;
}

export interface Producto_INT {
  readonly id_producto: string;
  id_name_product?: number;
  id_name_laboratorio?: number;
  product_name?: string;
  nombre_laboratorio?: string;
  cantidad: number;
  cantidad_disponible: number;
  presentacion: string;
  lote: string;
  registro_sanitario: string;
  medida?: number;
  tipo_medida?: string;
  dosis?: number;
  tipo_dosis?: string;
  fecha_elaboracion: string;
  fecha_caducidad: string;
  pvp: number;
  pvf: number;
  estado?: string | 'Disponible' | 'Aun disponible' | 'Vendido' | 'Caducado';
  principio_activo?: string;
  id_principio_activo?: number;
  formato?: string;
  unidades?: number;
  item_total?: number;
  iva?: number;
  veces_ingreso?: number;
}

export interface Principio_activo {
  readonly id_principio_activo: number;
  principio_activo: string;
}

export interface Laboratorio {
  readonly id_name_laboratorio: number;
  nombre_laboratorio: string;
}

export interface Producto_name {
  readonly id_product_name: number;
  product_name: string;
}

export interface Cliente_INT {
  readonly id_cliente: string;
  nombres: string;
  apellidos: string;
  identificacion: number;
  correo: string;
  direccion: string;
}

export interface Factura_INT {
  readonly id_factura: string;
  id_cliente: string;
  correo: string;
  identificacion: number;
  nombres: string;
  apellidos: string;
  fecha_factura?: string;
  descripcion: string;
  descuento: number;
  total: number;
  efectivo: number;
  cambio: number;
  carrito: Array<Carrito_INT>;
}

export interface Producto_Factura_INT {
  readonly id_producto_fac: string;
  id_producto: string;
  id_factura: string;
  formato: string;
  cantidad: number;
  item_total: number;
  iva: number;
}

export interface Proveedor_INT {
  readonly id_proveedores: string;
  nombres: string;
  id_laboratorio: number;
  correo: string;
  telefono: number;
  nombre_laboratorio: string;
}

export interface Producto_proveedor_INT {
  readonly id_product_proveedor: string;
  descripcion: string;
  fecha_pago: string;
  fecha_ingreso?: string;
  total: number;
  id_proveedor: string;
  estado_pp: string;
  abonado: number;
  nombre_laboratorio?: string;
}

export interface Prestamo_INT {
  readonly id_prestamo: string;
  descripcion_prestamo: string;
  fecha_prestamo?: string;
  cantidad_prestamo: number;
  estado_prestamo: string;
  abono_prestamo: number;
}

export interface Respuesta {
  feeback: string | undefined;
  type: string | undefined;
}

export interface ResponseAxios {
  axios: AxiosResponse;
  respuesta: Respuesta;
}

export interface Notificacion {
  type: string | undefined;
  content: string | any;
  date: Date;
}

export interface Carrito_INT {
  producto: Producto_INT;
  formato: string;
  precio_unidad: number;
  cantidad: number;
  iva: boolean;
  total: number;
}
