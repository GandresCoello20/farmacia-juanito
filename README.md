# Farmacia Juanito
Frontend desarrollado en [React](https://es.reactjs.org/) para consumir [api farmacia](https://github.com/GandresCoello18/api_farmacia)

## Comenzando 🚀

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Mira **Deployment** para conocer como desplegar el proyecto.


### Pre-requisitos 📋

_Que cosas necesitas para instalar el software y como instalarlas_

_Tener instalado Node Js en tu ordenador, puedes obtener desde el sitio oficial para diferentes SOS._
* [Node js](https://nodejs.org/es/) - Entorno de ejecucion javascript
* [Xampp](https://www.apachefriends.org/es/download.html) - Panel de control LAMPP para utilizar phpMyAdmin como gestor de base de datos.
* [TypeScript](https://www.typescriptlang.org/) - Tipado para javaScript

### Instalación 🔧

Abrir una terminal o linea de comandos apuntando a la raiz de la carpeta y ejecutar.

```
npm install
```
o
```
yarn install
```
_Despues que termine la instacion proceda a ejecutar el siguiente comando para correr la api en ambiente de desarrollo o local._

```
yarn dev
```
_Pàra corrar en production ejecutar
```
yarn start
```

## Ejecutando las pruebas ⚙️

_Para hacer debuguin del codido, puedes hacer uso del siguiente script._

_Si estas utilizando windows solo agregale un ( Set ) al comienzo._
*[Jest](https://jestjs.io/)- Mas info aqui


## Despliegue 📦

Configurado con ( integracion continua ) de [verecel](https://vercel.com) cada cambio a push en **Master** se crea el deploy

## Construido con 🛠️

_Para el desarrollo de esta api rest se utilizo las siguientes herramientas._

* [React Hooks](https://es.reactjs.org/docs/hooks-intro.html) - Crear componentes funcionales.
* [Redux](https://es.redux.js.org/) - Para manejar el estado de la app de forma global.
* [Ant Design](https://ant.design/components/grid/) - Para la interfaz grafica de usuario.

## Autores ✒️

* **Andrés Coello** - *Developer full stack* - [Andres Coello](https://www.instagram.com/coellogoyes/)

## Licencia 📄

Este proyecto está bajo la Licencia (MIT)

## Expresiones de Gratitud 🎁

* Pasate por mi perfil para ver algun otro proyecto 📢
* Desarrollemos alguna app juntos, puedes escribirme en mis redes. 
* Muchas gracias por pasarte por este proyecto 🤓.


---
⌨️ con ❤️ por [Andres Coello](https://www.instagram.com/coellogoyes/) 😊

